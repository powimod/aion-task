# Aion Task

## Description
Aion-Task is a utility that allows you to manage your repetitive tasks.

Aion-Task's main screen :

![Aion Task main screen](/assets/images/aion-task-main-screen.png)

Task filtering to hide task scheduled in the future :

![Task editor](/assets/images/aion-task-filtering.png)


Aion Task main editor : 

![Task editor](/assets/images/aion-task-editor.png)


Task validation dialog :

![Task validation dialog](/assets/images/aion-task-validation-dialog.png)


## License
GNU GPL V2 or later.

## Project status
This project is under development. It is not operational.
