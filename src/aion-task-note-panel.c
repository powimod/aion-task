/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-TaskNotePanel"
#include <glib/gi18n.h>

#include "aion-task-note-panel.h"
#include "aion-panelable.h"
#include "aion-panelable-private.h"

struct _AionTaskNotePanel 
{
	GtkBox parent;
};

typedef struct {
        AionTask* task;
	GtkWidget* note_text_view; // GtkTextView
	gboolean data_changed;
} AionTaskNotePanelPrivate;

// local prototypes
static void aion_task_note_panel_panelable_interface_init(AionPanelableInterface*);
static void _aion_task_note_panel_refresh(AionTaskNotePanel*);
static void _set_edit_mode(AionTaskNotePanel*, gboolean);
static void _text_buffer_changed(GtkTextBuffer*, gpointer);

G_DEFINE_TYPE_WITH_CODE (AionTaskNotePanel, aion_task_note_panel, GTK_TYPE_BOX,
	G_ADD_PRIVATE(AionTaskNotePanel)
	G_IMPLEMENT_INTERFACE(AION_TYPE_PANELABLE, aion_task_note_panel_panelable_interface_init)
);

static void
aion_task_note_panel_init (AionTaskNotePanel* task_note_panel)
{
	AionTaskNotePanelPrivate* priv = aion_task_note_panel_get_instance_private(task_note_panel);
	priv->task = NULL;

	GtkWidget* text_view = gtk_text_view_new();
	gtk_text_view_set_editable(GTK_TEXT_VIEW(text_view), FALSE);
	gtk_box_pack_start(GTK_BOX(task_note_panel), text_view, TRUE, TRUE, 0);
	priv->note_text_view = text_view;
	priv->data_changed = FALSE;

	GtkTextBuffer *text_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(priv->note_text_view));
	g_signal_connect(text_buffer, "changed", G_CALLBACK(_text_buffer_changed), task_note_panel);

	gtk_orientable_set_orientation(GTK_ORIENTABLE(task_note_panel), GTK_ORIENTATION_VERTICAL);
	gtk_widget_show_all(GTK_WIDGET(task_note_panel));
}



static void
aion_task_note_panel_dispose(GObject* object)
{
	AionTaskNotePanel* panel = AION_TASK_NOTE_PANEL(object);
	AionTaskNotePanelPrivate* priv = aion_task_note_panel_get_instance_private(panel);
	if (priv->task) {
		g_object_unref(priv->task);
		priv->task = NULL;
	}
	G_OBJECT_CLASS(aion_task_note_panel_parent_class)->dispose(object);
}
 
static void
aion_task_note_panel_finalize(GObject* object)
{
	//AionTaskNotePanel* task_note_panel = AION_TASK_NOTE_PANEL(object);
	G_OBJECT_CLASS(aion_task_note_panel_parent_class)->finalize(object);
}
 
static void
aion_task_note_panel_class_init(AionTaskNotePanelClass* task_note_panel_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(task_note_panel_class);
	object_class->finalize = aion_task_note_panel_finalize;
	object_class->dispose = aion_task_note_panel_dispose;
}


static const gchar*
_aion_panelable_interface_get_name(AionPanelable* panel)
{
	return "note-panel";
}


static const gchar*
_aion_panelable_interface_get_title(AionPanelable* panel)
{
	return _("Notes");
}

static const gchar*
_aion_panelable_interface_get_icon_name(AionPanelable* panel)
{
	return "accessories-text-editor-symbolic"; // TODO use specific icon
}

static void
_aion_panelable_interface_set_task(AionPanelable* panel, AionTask* task)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	g_return_if_fail(task == NULL || AION_IS_TASK(task));
	AionTaskNotePanel* task_note_panel = AION_TASK_NOTE_PANEL(panel);
	AionTaskNotePanelPrivate* priv = aion_task_note_panel_get_instance_private(task_note_panel);
	if (priv->task)
		g_object_unref(priv->task);
	priv->task = task;
	if (priv->task)
		g_object_ref(priv->task);
	_aion_task_note_panel_refresh(task_note_panel);
}

static void
_aion_panelable_interface_refresh(AionPanelable* panel)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	_aion_task_note_panel_refresh(AION_TASK_NOTE_PANEL(panel));
}

static gboolean
_aion_panelable_interface_is_editable(AionPanelable* panel)
{
	return TRUE;
}

static gboolean
_aion_panelable_interface_needs_save(AionPanelable* panel)
{
	g_assert(AION_IS_PANELABLE(panel));
	AionTaskNotePanel* task_note_panel = AION_TASK_NOTE_PANEL(panel);
	AionTaskNotePanelPrivate* priv = aion_task_note_panel_get_instance_private(task_note_panel);
	return priv->data_changed;
}

static void
_set_edit_mode(AionTaskNotePanel* task_note_panel, gboolean edit_mode)
{
	g_assert(AION_IS_TASK_NOTE_PANEL(task_note_panel));
	AionTaskNotePanelPrivate* priv = aion_task_note_panel_get_instance_private(task_note_panel);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(priv->note_text_view), edit_mode);
}

static void
_aion_panelable_interface_start_editing(AionPanelable* panel)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	AionTaskNotePanel* task_note_panel = AION_TASK_NOTE_PANEL(panel);
	AionTaskNotePanelPrivate* priv = aion_task_note_panel_get_instance_private(task_note_panel);
	_set_edit_mode(task_note_panel, TRUE);
	_aion_panelable_private_emit_editing_started_signal(panel, priv->task);
	gtk_widget_grab_focus(priv->note_text_view);
}

static void
_aion_panelable_interface_end_editing(AionPanelable* panel, gboolean modification)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	AionTaskNotePanel* task_note_panel = AION_TASK_NOTE_PANEL(panel);
	AionTaskNotePanelPrivate* priv = aion_task_note_panel_get_instance_private(task_note_panel);
	g_assert (priv->task != NULL);
	_set_edit_mode(task_note_panel, FALSE);

	GtkTextBuffer *text_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(priv->note_text_view));
	if (modification) {
		// save task note
		GtkTextIter start_iter;
		GtkTextIter end_iter;
		gtk_text_buffer_get_bounds (text_buffer, &start_iter, &end_iter);
		gchar *text = gtk_text_buffer_get_text(text_buffer, &start_iter, &end_iter, FALSE);
		if (! aion_task_set_note(priv->task, text))
			modification = FALSE; // task note has not be modified
		g_free(text);
	}
	else {
		// restore task note
		const gchar* note_text = aion_task_get_note(priv->task);
		gtk_text_buffer_set_text(text_buffer, note_text, -1);
	}
	priv->data_changed = FALSE;
	_aion_panelable_private_emit_editing_finished_signal(panel, priv->task, modification);
}


static void
aion_task_note_panel_panelable_interface_init(AionPanelableInterface* iface)
{
	iface->get_name = _aion_panelable_interface_get_name;
	iface->get_title = _aion_panelable_interface_get_title;
	iface->get_icon_name = _aion_panelable_interface_get_icon_name;
	iface->set_task = _aion_panelable_interface_set_task;
	iface->refresh = _aion_panelable_interface_refresh;
	iface->is_editable = _aion_panelable_interface_is_editable;
	iface->needs_save = _aion_panelable_interface_needs_save;
	iface->start_editing = _aion_panelable_interface_start_editing;
	iface->end_editing = _aion_panelable_interface_end_editing;
}

AionTaskNotePanel*
aion_task_note_panel_new()
{
	return g_object_new(AION_TYPE_TASK_NOTE_PANEL, NULL);
}

static void
_aion_task_note_panel_refresh(AionTaskNotePanel* task_note_panel)
{
	g_assert(AION_IS_TASK_NOTE_PANEL(task_note_panel));
	AionTaskNotePanelPrivate* priv = aion_task_note_panel_get_instance_private(task_note_panel);
	const gchar* task_note = NULL;
	if (priv->task != NULL)
		task_note = aion_task_get_note(priv->task);
	if (task_note == NULL)
		task_note = "";
	g_assert(AION_IS_PANELABLE(task_note_panel));
	GtkTextBuffer *text_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(priv->note_text_view));
	/* FIXME strang bug here !!!
		There is a bug if `task_note' is passed directly to `gtk_text_buffer_set_text' function, like this :
			gtk_text_buffer_set_text(text_buffer, task_note, -1);
		A warning is displayed :
			« gtk_text_buffer_emit_insert: assertion 'g_utf8_validate (text, len, NULL)' failed »
		A test of UTF-8 validity does not show an error :
			if (! g_utf8_validate (task_note, -1, NULL))
				g_error("Task note is not encoded in UTF-8 !");
		A workaround is to copy the note with `g_strdup', like this :
	*/
	gchar* note_copy = g_strdup(task_note);
	gtk_text_buffer_set_text(text_buffer, note_copy , -1);
	g_free(note_copy);

	_set_edit_mode(task_note_panel, FALSE);
	priv->data_changed = FALSE;
}

static void
_text_buffer_changed(GtkTextBuffer *text_buffer, gpointer data)
{
	AionTaskNotePanel* panel = AION_TASK_NOTE_PANEL(data);
	AionTaskNotePanelPrivate* priv = aion_task_note_panel_get_instance_private(panel);
	priv->data_changed = TRUE;
}

// TODO add instance_counter
