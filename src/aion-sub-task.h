/* AionSubTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_SUB_TASK__
#define __AION_SUB_TASK__

#include <glib-object.h>

G_BEGIN_DECLS // TODO add to other header

#define AION_TYPE_SUB_TASK aion_sub_task_get_type()

G_DECLARE_FINAL_TYPE (AionSubTask, aion_sub_task, AION, SUB_TASK, GObject);

AionSubTask* aion_sub_task_new(const gchar*);

gint aion_sub_task_get_position(AionSubTask*);
const gchar* aion_sub_task_get_label(AionSubTask*);
gboolean aion_sub_task_set_label(AionSubTask*, const gchar*);

gboolean aion_sub_task_is_done(AionSubTask*);
gboolean aion_sub_task_mark_as_done(AionSubTask*, gboolean);

gint aion_sub_task_get_instance_count();
gint aion_sub_task_check_zero_instance();

G_END_DECLS

#endif /*__AION_SUB_TASK__*/

