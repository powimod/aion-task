/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-TaskValidationDialog"
#include <glib/gi18n.h>

#include "aion-task-validation-dialog.h"

struct _AionTaskValidationDialog {
	GtkDialog parent;
};

typedef struct {
	AionTask* task;
	AionMainWindow* main_window;
	GtkWidget* message;
	GtkWidget* duration_box;
	GtkWidget* duration_field;
} AionTaskValidationDialogPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(AionTaskValidationDialog, aion_task_validation_dialog, GTK_TYPE_DIALOG);

static gint instance_counter = 0;

enum {
	SIGNAL_DIALOG_CLOSED,
	LAST_SIGNAL
};
static guint aion_task_validation_dialog_signals[LAST_SIGNAL] = { 0 };

// local prototypes
static void _dialog_response(GtkDialog*, gint, gpointer);
static void _refresh(AionTaskValidationDialog*);

static void
aion_task_validation_dialog_init(AionTaskValidationDialog* task_validation_dialog)
{
	g_return_if_fail(AION_IS_TASK_VALIDATION_DIALOG(task_validation_dialog));
	AionTaskValidationDialogPrivate* priv = aion_task_validation_dialog_get_instance_private(task_validation_dialog);
	priv->task = NULL;
	priv->main_window = NULL;

	gtk_window_set_modal(GTK_WINDOW(task_validation_dialog), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(task_validation_dialog), TRUE);

	gtk_dialog_add_buttons(GTK_DIALOG(task_validation_dialog),
				_("OK"), GTK_RESPONSE_ACCEPT,
				_("Cancel"), GTK_RESPONSE_REJECT,
				NULL);

	GtkWidget* content_area = gtk_dialog_get_content_area(GTK_DIALOG(task_validation_dialog));
	gtk_container_set_border_width(GTK_CONTAINER(content_area), 15); // FIXME hard coded spacing

	GtkBuilder* builder = gtk_builder_new_from_resource("/org/gnome/aion-task/ui/task-validation-dialog");
	g_assert(builder != NULL);
	GtkWidget* container = GTK_WIDGET(gtk_builder_get_object(builder, "task-validation-dialog-content"));
	gtk_box_pack_start(GTK_BOX(content_area), container, TRUE, TRUE, 5);

	priv->message = GTK_WIDGET(gtk_builder_get_object(builder, "message"));
	g_assert(priv->message);

	priv->duration_box = GTK_WIDGET(gtk_builder_get_object(builder, "duration-box"));
	g_assert(priv->duration_box);
	priv->duration_field = GTK_WIDGET(gtk_builder_get_object(builder, "duration-field"));
	g_assert(priv->duration_field);

	g_signal_connect(task_validation_dialog, "response", G_CALLBACK(_dialog_response), task_validation_dialog);
	gtk_widget_show_all(GTK_WIDGET(task_validation_dialog));

	instance_counter++;
}

static void
aion_task_validation_dialog_dispose(GObject* object)
{
	AionTaskValidationDialog* task_validation_dialog = AION_TASK_VALIDATION_DIALOG(object);
	AionTaskValidationDialogPrivate *priv = aion_task_validation_dialog_get_instance_private(task_validation_dialog);
	if (priv->task) {
		g_object_unref(priv->task);
		priv->task = NULL;
	}
	if (priv->main_window) {
		g_object_unref(priv->main_window);
		priv->main_window= NULL;
	}
	G_OBJECT_CLASS(aion_task_validation_dialog_parent_class)->dispose(object);
}


static void
aion_task_validation_dialog_finalize(GObject* object)
{
	//AionTaskValidationDialog* task_validation_dialog = AION_TASK_VALIDATION_DIALOG(object);
	//AionTaskValidationDialogPrivate *priv = aion_task_validation_dialog_get_instance_private(task_validation_dialog);
	G_OBJECT_CLASS(aion_task_validation_dialog_parent_class)->finalize(object);
	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_TASK_VALIDATION_DIALOG));
}

 
static void
aion_task_validation_dialog_class_init(AionTaskValidationDialogClass* task_validation_dialog_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(task_validation_dialog_class);
	object_class->dispose= aion_task_validation_dialog_dispose;
	object_class->finalize = aion_task_validation_dialog_finalize;

	aion_task_validation_dialog_signals[SIGNAL_DIALOG_CLOSED] = 
		g_signal_new ("closed",
			G_TYPE_FROM_CLASS(task_validation_dialog_class),
			G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
			0,                   // class offset
			NULL,                // accumulator 
			NULL,                // accumulator data
			NULL,                // C marshaller
			G_TYPE_NONE,         // return_type
			1,                   // n_params 
			G_TYPE_BOOLEAN);     // first parameter
}


AionTaskValidationDialog*
aion_task_validation_dialog_new(AionTask* task, AionMainWindow* main_window)
{
	g_return_val_if_fail(AION_IS_TASK(task), NULL);
	g_return_val_if_fail(AION_IS_MAIN_WINDOW(main_window), NULL);
	AionTaskValidationDialog* task_validation_dialog = 
		AION_TASK_VALIDATION_DIALOG(g_object_new(AION_TYPE_TASK_VALIDATION_DIALOG, 
			"use-header-bar", FALSE,
			NULL));
	AionTaskValidationDialogPrivate* priv = aion_task_validation_dialog_get_instance_private(task_validation_dialog);

	priv->task = task; g_object_ref(priv->task);
	priv->main_window = main_window; g_object_ref(priv->main_window);
	gtk_window_set_transient_for(GTK_WINDOW(task_validation_dialog), GTK_WINDOW(main_window));

	_refresh(task_validation_dialog);
	return task_validation_dialog;
}

static void
_refresh(AionTaskValidationDialog* task_validation_dialog)
{
	g_assert(AION_IS_TASK_VALIDATION_DIALOG(task_validation_dialog));
	AionTaskValidationDialogPrivate* priv = aion_task_validation_dialog_get_instance_private(task_validation_dialog);

	gchar* message = g_markup_printf_escaped("Task <i>« %s »</i> will be validated...",  // FIXME transation
		aion_task_get_label(priv->task));
	gtk_label_set_markup(GTK_LABEL(priv->message), message);
	g_free(message);

	gint default_duration = aion_task_get_default_duration(priv->task);
	if (default_duration > 0) {
		gtk_widget_show(priv->duration_box);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->duration_field), default_duration);
	}
	else {
		gtk_widget_hide(priv->duration_box);
	}
}

static void
_dialog_response(GtkDialog* dialog, gint response_id, gpointer data)
{
	AionTaskValidationDialog* task_validation_dialog = AION_TASK_VALIDATION_DIALOG(data);
	AionTaskValidationDialogPrivate* priv = aion_task_validation_dialog_get_instance_private(task_validation_dialog);
	gboolean validation = FALSE;
	if (response_id == GTK_RESPONSE_ACCEPT) {
		validation = TRUE;

		gint effective_duration = 0;
		if (aion_task_get_default_duration(priv->task) > 0) 
			effective_duration = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->duration_field));

		aion_task_validate_with_duration(priv->task, effective_duration);
	}
	g_debug("Validation : %s", validation ? "yes" : "no");
	g_signal_emit(task_validation_dialog, aion_task_validation_dialog_signals[SIGNAL_DIALOG_CLOSED], 0, validation);
	gtk_widget_destroy(GTK_WIDGET(dialog));
}

gint
aion_task_validation_dialog_get_instance_count()
{
	return instance_counter;
}

gint
aion_task_validation_dialog_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_TASK_VALIDATION_DIALOG));
	return instance_counter;
}


