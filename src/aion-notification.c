/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-Notification"
#include "aion-notification.h"

struct _AionNotification {
	GtkFrame parent;
};

typedef struct {
	GtkWidget* message; // GtkLabel
	GtkWidget* action_button;  // GtkButton
	GAction* action;
	guint close_timer_id;
} AionNotificationPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(AionNotification, aion_notification, GTK_TYPE_BOX);

static gint instance_counter = 0;

#define AION_NOTIFICATION_CSS_CLASS_NAME "aion_notification"
#define DEFAULT_KEEPALIVE 5

static gboolean _close_notification_timeout(gpointer);
static void _close_button_clicked(GtkButton*, gpointer);
static void _action_button_clicked(GtkButton*, gpointer);

static void
aion_notification_init(AionNotification* notification)
{
	GtkStyleContext* style_context = gtk_widget_get_style_context(GTK_WIDGET(notification));
	gtk_style_context_add_class(style_context, "app-notification");

	g_return_if_fail(AION_IS_NOTIFICATION(notification));
	AionNotificationPrivate* priv = aion_notification_get_instance_private(notification);

	GtkWidget* h_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5); // FIXME hard coded spacing
	gtk_container_add(GTK_CONTAINER(notification), h_box);
	gtk_container_set_border_width(GTK_CONTAINER(h_box), 5); // FIXME hard coded spacing

	// message
	priv->message = gtk_label_new(NULL);
	gtk_box_pack_start(GTK_BOX(h_box), priv->message, TRUE, TRUE, 5);
	gtk_label_set_line_wrap(GTK_LABEL(priv->message), TRUE);
	gtk_widget_set_halign(priv->message, GTK_ALIGN_START);

	// action button
	GtkWidget* button = gtk_button_new();
	gtk_box_pack_start(GTK_BOX(h_box), button, FALSE, FALSE, 5);
	priv->action_button = button;

	// close button
	button = gtk_button_new();
	GtkWidget* button_image = gtk_image_new_from_icon_name("window-close-symbolic", GTK_ICON_SIZE_SMALL_TOOLBAR);
	//gtk_button_set_relief(GTK_BUTTON(button), GTK_RELIEF_NONE);
	gtk_button_set_image(GTK_BUTTON(button), button_image);
	gtk_box_pack_start(GTK_BOX(h_box), button, FALSE, FALSE, 5);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(_close_button_clicked), notification);

	priv->close_timer_id = g_timeout_add(DEFAULT_KEEPALIVE * 1000, _close_notification_timeout, notification);

	gtk_widget_set_vexpand(GTK_WIDGET(notification), FALSE);
	gtk_widget_show_all(GTK_WIDGET(notification));
	gtk_widget_hide(priv->action_button);
	instance_counter++;
}

static void
aion_notification_dispose(GObject* object)
{
	G_OBJECT_CLASS(aion_notification_parent_class)->dispose(object);
	AionNotification* notification = AION_NOTIFICATION(object);
	AionNotificationPrivate *priv = aion_notification_get_instance_private(notification);
	if (priv->action) {
		g_object_ref(priv->action);
		priv->action = NULL;
	}
}


static void
aion_notification_finalize(GObject* object)
{
	AionNotification* notification = AION_NOTIFICATION(object);
	AionNotificationPrivate *priv = aion_notification_get_instance_private(notification);
	if (priv->close_timer_id > 0) {
		g_source_remove(priv->close_timer_id);
		priv->close_timer_id = 0;
	}
	G_OBJECT_CLASS(aion_notification_parent_class)->finalize(object);
	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_NOTIFICATION));
}

 
static void
aion_notification_class_init(AionNotificationClass* notification_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(notification_class);
	object_class->dispose= aion_notification_dispose;
	object_class->finalize = aion_notification_finalize;
}

AionNotification*
aion_notification_new_with_message(gchar* message)
{
	g_return_val_if_fail(message != NULL, NULL);
	AionNotification* notification = AION_NOTIFICATION(g_object_new(AION_TYPE_NOTIFICATION, NULL));
	aion_notification_set_message(notification, message);
	return notification;
}

void
aion_notification_set_action(AionNotification* notification, const gchar* action_label, GAction* action)
{
	g_return_if_fail(AION_IS_NOTIFICATION(notification));
	g_return_if_fail(action_label != NULL);
	g_return_if_fail(G_IS_ACTION(action));
	AionNotificationPrivate* priv = aion_notification_get_instance_private(notification);
	gtk_button_set_label(GTK_BUTTON(priv->action_button), action_label);

	if (priv->action)
		g_object_unref(priv->action);
	priv->action = action;
	g_object_ref(priv->action);

	g_signal_connect(G_OBJECT(priv->action_button), "clicked", G_CALLBACK(_action_button_clicked), notification);
	gtk_widget_show(priv->action_button);
}

void
aion_notification_set_message(AionNotification* notification, const gchar* message)
{
	g_return_if_fail(AION_IS_NOTIFICATION(notification));
	g_return_if_fail(message != NULL);
	AionNotificationPrivate* priv = aion_notification_get_instance_private(notification);
	gtk_label_set_text(GTK_LABEL(priv->message), message);
}

static void
_destroy_notification(AionNotification* notification)
{
	AionNotificationPrivate* priv = aion_notification_get_instance_private(notification);
	if (priv->close_timer_id > 0) 
		g_source_remove(priv->close_timer_id);
	priv->close_timer_id = 0;
	gtk_widget_destroy(GTK_WIDGET(notification));
}

static gboolean
_close_notification_timeout(gpointer data)
{
	AionNotification* notification = AION_NOTIFICATION(data);
	_destroy_notification(notification);
	return FALSE; // ask not to be called again
}

static void
_close_button_clicked (GtkButton* button, gpointer data)
{
	AionNotification* notification = AION_NOTIFICATION(data);
	_destroy_notification(notification);
}

static void
_action_button_clicked (GtkButton* button, gpointer data)
{
	AionNotification* notification = AION_NOTIFICATION(data);
	AionNotificationPrivate* priv = aion_notification_get_instance_private(notification);
	g_assert(G_IS_ACTION(priv->action));
	g_action_activate(priv->action, NULL);
	_destroy_notification(notification);
}

gint
aion_notification_get_instance_count()
{
	return instance_counter;
}

gint
aion_notification_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_NOTIFICATION));
	return instance_counter;
}
