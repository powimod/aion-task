/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_TASK__
#define __AION_TASK__

#include "aion-sub-task.h"
#include <glib-object.h>

G_BEGIN_DECLS // TODO add to other header

#define AION_TASK_MINIMUM_PRIORITY 1 // lower priority
#define AION_TASK_MAXIMUM_PRIORITY 5 // higher priority
#define AION_TASK_DEFAULT_PRIORITY 3

#define AION_TYPE_TASK aion_task_get_type()

G_DECLARE_FINAL_TYPE (AionTask, aion_task, AION, TASK, GObject);

typedef enum {
	AION_TASK_REPETITION_ONCE = 0,
	AION_TASK_REPETITION_EVERY_DAY,
	AION_TASK_REPETITION_EVERY_WEEK,
	AION_TASK_REPETITION_EVERY_MONTH,
	AION_TASK_REPETITION_EVERY_YEAR
} AionTaskRepetitionType;

typedef enum {
	AION_TASK_STATE_DISABLED = 0,
	AION_TASK_STATE_PENDING,
	AION_TASK_STATE_PREVIEW,
	AION_TASK_STATE_ON_TIME,
	AION_TASK_STATE_POSTPONED,
	AION_TASK_STATE_OVERDUE
} AionTaskState;

typedef enum {
	AION_TASK_POSTPONE_FROM_EXECUTION_DATE = 0,
	AION_TASK_POSTPONE_FROM_DUE_DATE
} AionTaskPostponeFromWhen;

typedef struct {
	gshort year;
	gchar month;
	gchar day;
	gint duration;
} AionTaskLogEntry;

AionTask* aion_task_new(const gchar*);

const gchar* aion_task_get_label(AionTask*);
gboolean aion_task_set_label(AionTask*, const gchar*);

const gchar* aion_task_get_note(AionTask*);
gboolean aion_task_set_note(AionTask*, const gchar*);

gboolean aion_task_is_enabled(AionTask*);
gboolean aion_task_set_enabled(AionTask*, gboolean);


const gchar* aion_task_repetition_type_string(AionTaskRepetitionType);
AionTaskRepetitionType aion_task_get_repetition_type(AionTask*);
const gchar* aion_task_get_repetition_type_to_string(AionTask*);
gboolean aion_task_set_repetition_type(AionTask*, AionTaskRepetitionType);
AionTaskRepetitionType aion_task_repetition_type_from_string(const gchar*);
gboolean aion_task_set_repetition_type_from_string(AionTask*, const gchar*);

gchar* aion_task_get_human_representation_of_repetition(AionTask*);

AionTaskPostponeFromWhen aion_task_get_postpone_from_when(AionTask*);
gboolean aion_task_set_postpone_from_when(AionTask*, AionTaskPostponeFromWhen);

gint aion_task_get_interval(AionTask*);
gboolean aion_task_set_interval(AionTask*, gint);

gint aion_task_get_total_duration(AionTask*);
gboolean aion_task_private_set_default_duration(AionTask*, gint);

gint aion_task_get_default_duration(AionTask*);
gboolean aion_task_set_default_duration(AionTask*, gint);

const GDate* aion_task_get_next_execution_date(AionTask*);
gboolean aion_task_set_next_execution_date(AionTask*, const GDate*);

gint aion_task_get_preview_delay(AionTask*);
gboolean aion_task_set_preview_delay(AionTask*, gint);

gint aion_task_get_postponement_delay(AionTask*);
gboolean aion_task_set_postponement_delay(AionTask*, gint);

gint aion_task_get_priority(AionTask*);
gboolean aion_task_set_priority(AionTask*, gint);

AionTaskState aion_task_get_state(AionTask*);
const gchar* aion_task_get_state_str(AionTask*);
AionTaskState aion_task_evaluate_state_on_date_of(AionTask*, GDate*);
gint aion_task_get_remaining_days(AionTask*);
gboolean aion_task_validate(AionTask*);
gboolean aion_task_validate_with_duration(AionTask*, gint);
gboolean aion_task_postpone(AionTask*, gint);

gint aion_task_get_execution_count(AionTask*);
gint aion_task_get_log_entries_count(AionTask*);
gint aion_task_get_log_entries_limit(AionTask*);
gboolean aion_task_set_log_entries_limit(AionTask*, gint);
GDate* aion_task_get_log_entry_date(AionTask*, gint);
gint aion_task_get_log_entry_duration(AionTask*, gint);
void aion_task_get_log_entry_date_dmy(AionTask*, gint, gint*, gint*, gint*);

const gchar** aion_task_repetition_type_array();

typedef void (*AionSubTaskCallback) (AionSubTask*, gpointer);
AionSubTask* aion_task_create_sub_task(AionTask*, const gchar*);
gboolean aion_task_delete_sub_task(AionTask*, AionSubTask*);
gint aion_task_sub_task_count(AionTask*);
void aion_task_for_each_sub_task(AionTask*, AionSubTaskCallback, gpointer);
GList* aion_task_get_sub_task_list(AionTask*);
gboolean aion_task_swap_sub_tasks(AionTask* task, AionSubTask*, AionSubTask*);
gboolean aion_task_set_sub_task_position(AionTask*, AionSubTask*, gint);

gint aion_task_get_instance_count();
gint aion_task_check_zero_instance();

G_END_DECLS

#endif /*__AION_TASK__*/

