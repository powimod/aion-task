/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-TaskLogPanel"
#include <glib/gi18n.h>

#include "aion-task-log-panel.h"
#include "aion-panelable.h"
#include "aion-panelable-private.h"
#include "util.h"

struct _AionTaskLogPanel 
{
	GtkBox parent;
};

typedef struct {
        AionTask* task;
	GtkWidget* log_list; // GtkTreeView
	GtkListStore* log_store;
} AionTaskLogPanelPrivate;

enum {
	DATE_COLUMN,
	DURATION_COLUMN,
	NUMBER_OF_COLUMNS
};

static void aion_task_log_panel_panelable_interface_init(AionPanelableInterface*);
static void _aion_task_log_panel_refresh(AionTaskLogPanel*);

G_DEFINE_TYPE_WITH_CODE (AionTaskLogPanel, aion_task_log_panel, GTK_TYPE_BOX,
	G_ADD_PRIVATE(AionTaskLogPanel)
	G_IMPLEMENT_INTERFACE(AION_TYPE_PANELABLE, aion_task_log_panel_panelable_interface_init)
);

static void
aion_task_log_panel_init (AionTaskLogPanel* task_log_panel)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;

	AionTaskLogPanelPrivate* priv = aion_task_log_panel_get_instance_private(task_log_panel);
	priv->task = NULL;

	priv->log_store = gtk_list_store_new(
		NUMBER_OF_COLUMNS,
		G_TYPE_STRING, // DATE_COLUMN
		G_TYPE_STRING  // DURATION_COLUMN
	);
	GtkWidget* tree_view = gtk_tree_view_new_with_model(GTK_TREE_MODEL(priv->log_store));

	// Date column
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes (
					_("Date"),
					renderer,
					"text", DATE_COLUMN,
					NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree_view), column);

	// Duration column
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes (
					_("Duration"),
					renderer,
					"text", DURATION_COLUMN,
					NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree_view), column);


	gtk_box_pack_start(GTK_BOX(task_log_panel), tree_view, TRUE, TRUE, 0);
	priv->log_list = tree_view;

	gtk_orientable_set_orientation(GTK_ORIENTABLE(task_log_panel), GTK_ORIENTATION_VERTICAL);
	gtk_widget_show_all(GTK_WIDGET(task_log_panel));
}

static void
aion_task_log_panel_dispose(GObject* object)
{
	AionTaskLogPanel* panel = AION_TASK_LOG_PANEL(object);
	AionTaskLogPanelPrivate* priv = aion_task_log_panel_get_instance_private(panel);
	if (priv->task) {
		g_object_unref(priv->task);
		priv->task = NULL;
	}
	G_OBJECT_CLASS(aion_task_log_panel_parent_class)->dispose(object);
}
 
static void
aion_task_log_panel_finalize(GObject* object)
{
	G_OBJECT_CLASS(aion_task_log_panel_parent_class)->finalize(object);
}
 
static void
aion_task_log_panel_class_init(AionTaskLogPanelClass* task_log_panel_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(task_log_panel_class);
	object_class->finalize = aion_task_log_panel_finalize;
	object_class->dispose = aion_task_log_panel_dispose;
}


static const gchar*
_aion_panelable_interface_get_name(AionPanelable* panel)
{
	return "log-panel";
}


static const gchar*
_aion_panelable_interface_get_title(AionPanelable* panel)
{
	return _("Task log");
}

static const gchar*
_aion_panelable_interface_get_icon_name(AionPanelable* panel)
{
	return "view-sort-ascending-symbolic"; // TODO use specific icon
}

static void
_aion_panelable_interface_set_task(AionPanelable* panel, AionTask* task)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	g_return_if_fail(task == NULL || AION_IS_TASK(task));
	AionTaskLogPanel* task_log_panel = AION_TASK_LOG_PANEL(panel);
	AionTaskLogPanelPrivate* priv = aion_task_log_panel_get_instance_private(task_log_panel);
	if (priv->task)
		g_object_unref(priv->task);
	priv->task = task;
	if (priv->task)
		g_object_ref(priv->task);
	_aion_task_log_panel_refresh(task_log_panel);
}

static void
_aion_panelable_interface_refresh(AionPanelable* panel)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	_aion_task_log_panel_refresh(AION_TASK_LOG_PANEL(panel));
}

static gboolean
_aion_panelable_interface_is_editable(AionPanelable* panel)
{
	return FALSE;
}

static gboolean
_aion_panelable_interface_needs_save(AionPanelable* panel)
{
	return FALSE;
}

static void
_aion_panelable_interface_start_editing(AionPanelable* panel)
{
	g_assert_not_reached(); // log panel is not editable
}

static void
_aion_panelable_interface_end_editing(AionPanelable* panel, gboolean modification)
{
	g_assert_not_reached(); // log panel is not editable
}


static void
aion_task_log_panel_panelable_interface_init(AionPanelableInterface* iface)
{
	iface->get_name = _aion_panelable_interface_get_name;
	iface->get_title = _aion_panelable_interface_get_title;
	iface->get_icon_name = _aion_panelable_interface_get_icon_name;
	iface->set_task = _aion_panelable_interface_set_task;
	iface->refresh = _aion_panelable_interface_refresh;
	iface->is_editable = _aion_panelable_interface_is_editable;
	iface->needs_save = _aion_panelable_interface_needs_save;
	iface->start_editing = _aion_panelable_interface_start_editing;
	iface->end_editing = _aion_panelable_interface_end_editing;
}

AionTaskLogPanel*
aion_task_log_panel_new()
{
	return g_object_new(AION_TYPE_TASK_LOG_PANEL, NULL);
}

static void
_aion_task_log_panel_refresh(AionTaskLogPanel* task_log_panel)
{
	g_assert(AION_IS_TASK_LOG_PANEL(task_log_panel));
	AionTaskLogPanelPrivate* priv = aion_task_log_panel_get_instance_private(task_log_panel);

	gtk_list_store_clear(priv->log_store);
	if (priv->task == NULL)
		return;

	gint day, month, year;
	for (gint entry_index = 0; entry_index < aion_task_get_log_entries_count(priv->task); entry_index++) {
		GtkTreeIter iter; 
		aion_task_get_log_entry_date_dmy(priv->task, entry_index, &day, &month, &year);
		gchar* date_str = g_strdup_printf("%02i/%02i/%04i", day, month, year); // TODO localisation
		gint duration = aion_task_get_log_entry_duration(priv->task, entry_index);
		gchar* duration_str = aion_util_get_duration_human_representation(duration);
		gtk_list_store_append (priv->log_store, &iter);
		gtk_list_store_set (priv->log_store, &iter,
			DATE_COLUMN, date_str,
			DURATION_COLUMN, duration_str,
			-1);
		g_free(date_str);
		g_free(duration_str);
	}
}

// TODO add instance_counter
