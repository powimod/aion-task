/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_APPLICATION__
#define __AION_APPLICATION__

#include "aion-task-manager.h"
#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define AION_TYPE_APPLICATION aion_application_get_type()

G_DECLARE_FINAL_TYPE(AionApplication, aion_application, AION, APPLICATION, GtkApplication)

AionApplication* aion_application_new(const gchar*);
AionTaskManager* aion_application_get_task_manager(AionApplication*);

gboolean aion_application_save_data(AionApplication*, GError**);
gboolean aion_application_load_data(AionApplication*, GError**);

gint aion_application_get_instance_count();
gint aion_application_check_zero_instance();

G_END_DECLS

#endif  /* __AION_APPLICATION__ */

