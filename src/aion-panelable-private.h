/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_PANELABLE_PRIVATE__
#define __AION_PANELABLE_PRIVATE__

G_BEGIN_DECLS

void _aion_panelable_private_emit_editing_started_signal(AionPanelable*, AionTask*);
void _aion_panelable_private_emit_editing_finished_signal(AionPanelable*, AionTask*, gboolean);

G_END_DECLS

#endif /*__AION_PANELABLE_PRIVATE__*/

