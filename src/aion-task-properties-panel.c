/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-TaskPropertiesPanel"
#include <glib/gi18n.h>

#include "aion-task-properties-panel.h"
#include "aion-panelable.h"
#include "aion-panelable-private.h"
#include "util.h"

struct _AionTaskPropertiesPanel 
{
	GtkBox parent;
};

typedef struct {
        AionTask* task;
	GtkWidget* field_task_label;      // GtkEntry
	GtkWidget* field_task_enabled;    // GtkSwitch
	GtkWidget* field_repetition_type; // GtkComboText
	GtkWidget* frame_postpone;        // GtkFrame
	GtkWidget* field_postpone_from_due_date;  // GtkCheckButton
	GtkWidget* field_postpone_from_exec_date; // GtkCheckButton
	GtkWidget* field_interval;        // GtkSpinButton
	GtkWidget* field_priority;        // GtkScale
	GtkWidget* field_next_execution;  // GtkButton
	GtkWidget* field_preview_delay;   // GtkSpinButton
	GtkWidget* field_postponement_delay; // GtkSpinButton
	GtkWidget* field_log_limit;       // GtkSpinButton
	GtkWidget* field_default_duration;// GtkSpinButton
	gboolean edit_mode; // FIXME really used ?
	gboolean data_changed;
} AionTaskPropertiesPanelPrivate;

// local prototypes
static void aion_task_properties_panel_panelable_interface_init(AionPanelableInterface*);
static void _widget_set_dmy_data(GtkWidget*, guint, guint, guint);
static void _widget_get_dmy_data(GtkWidget*, guint*, guint*, guint*);
static void _next_execution_field_clicked (GtkButton*, gpointer);
static void _repetition_type_changed(GtkComboBox*, gpointer);
static void _set_edit_mode(AionTaskPropertiesPanel*, gboolean);
static gboolean aion_task_properties_panel_validate_changes(AionTaskPropertiesPanel*);
void _aion_task_properties_panel_refresh(AionTaskPropertiesPanel*);
static void _entry_buffer_changed(GtkEntryBuffer*, guint , gchar*, guint, gpointer);
static void _spin_button_value_changed(GtkSpinButton*, gpointer);
static void _priority_scale_value_changed(GtkRange*, gpointer);
static void _toogle_button_changed(GtkToggleButton*, gpointer);
static void _switch_state_changed(GtkSwitch*, GParamSpec*, gpointer);

G_DEFINE_TYPE_WITH_CODE (AionTaskPropertiesPanel, aion_task_properties_panel, GTK_TYPE_BOX,
	G_ADD_PRIVATE(AionTaskPropertiesPanel)
	G_IMPLEMENT_INTERFACE(AION_TYPE_PANELABLE, aion_task_properties_panel_panelable_interface_init)
);

static void
aion_task_properties_panel_init (AionTaskPropertiesPanel* task_properties_panel)
{
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(task_properties_panel);
	priv->task = NULL;
	priv->data_changed = FALSE;

	GtkBuilder* builder = gtk_builder_new_from_resource("/org/gnome/aion-task/ui/task-properties-panel");
	g_assert(builder != NULL);

	// FIXME task_properties_panel is a box with only one child : another box...
	GtkWidget* container = GTK_WIDGET(gtk_builder_get_object(builder, "task-properties-panel"));
	g_assert(container);
	gtk_box_pack_start(GTK_BOX(task_properties_panel), container, TRUE, TRUE, 0); // FIXME hard coded spacing

	priv->field_task_label = GTK_WIDGET(gtk_builder_get_object(builder, "task-label"));
	g_assert(priv->field_task_label);
	GtkEntryBuffer* entry_buffer = gtk_entry_get_buffer(GTK_ENTRY(priv->field_task_label));
	g_signal_connect(entry_buffer, "inserted-text", G_CALLBACK(_entry_buffer_changed), task_properties_panel);

	priv->field_next_execution = GTK_WIDGET(gtk_builder_get_object(builder, "next-execution"));
	g_assert(priv->field_next_execution);
	g_signal_connect(priv->field_next_execution, "clicked", G_CALLBACK(_next_execution_field_clicked), task_properties_panel);

	priv->field_preview_delay = GTK_WIDGET(gtk_builder_get_object(builder, "preview-delay"));
	g_assert(priv->field_preview_delay);
	g_signal_connect(priv->field_preview_delay, "value-changed", G_CALLBACK(_spin_button_value_changed), task_properties_panel);

	priv->field_postponement_delay = GTK_WIDGET(gtk_builder_get_object(builder, "postponement-delay"));
	g_assert(priv->field_postponement_delay);
	g_signal_connect(priv->field_postponement_delay, "value-changed", G_CALLBACK(_spin_button_value_changed), task_properties_panel);

	priv->field_task_enabled = GTK_WIDGET(gtk_builder_get_object(builder, "task-enabled"));
	g_assert(priv->field_task_enabled); 
	g_signal_connect(G_OBJECT(priv->field_task_enabled), "notify::state", G_CALLBACK(_switch_state_changed), task_properties_panel);


	priv->field_repetition_type = GTK_WIDGET(gtk_builder_get_object(builder, "repetition-type"));
	const gchar** str_array = aion_task_repetition_type_array();
	for (int i = 0; str_array[i] != NULL; i++) 
		gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(priv->field_repetition_type), NULL, str_array[i]);
	g_signal_connect(priv->field_repetition_type, "changed", G_CALLBACK(_repetition_type_changed), task_properties_panel);

	priv->field_interval = GTK_WIDGET(gtk_builder_get_object(builder, "repetition-interval"));
	g_assert(priv->field_interval);
	g_signal_connect(priv->field_interval, "value-changed", G_CALLBACK(_spin_button_value_changed), task_properties_panel);

	priv->field_priority = GTK_WIDGET(gtk_builder_get_object(builder, "priority-value"));
	g_assert(priv->field_priority);
	g_signal_connect(priv->field_priority, "value-changed", G_CALLBACK(_priority_scale_value_changed), task_properties_panel);

	priv->frame_postpone = GTK_WIDGET(gtk_builder_get_object(builder, "postpone-frame"));
	g_assert(priv->frame_postpone);

	priv->field_postpone_from_exec_date = GTK_WIDGET(gtk_builder_get_object(builder, "postpone-from-execution"));
	g_assert(priv->field_postpone_from_exec_date);
	g_signal_connect(priv->field_postpone_from_exec_date , "toggled", G_CALLBACK(_toogle_button_changed), task_properties_panel);

	priv->field_postpone_from_due_date = GTK_WIDGET(gtk_builder_get_object(builder, "postpone-from-due"));
	g_assert(priv->field_postpone_from_due_date);
	g_signal_connect(priv->field_postpone_from_due_date , "toggled", G_CALLBACK(_toogle_button_changed), task_properties_panel);

	priv->field_log_limit = GTK_WIDGET(gtk_builder_get_object(builder, "log-limit"));
	g_assert(priv->field_log_limit);
	g_signal_connect(priv->field_log_limit, "value-changed", G_CALLBACK(_spin_button_value_changed), task_properties_panel);

	priv->field_default_duration = GTK_WIDGET(gtk_builder_get_object(builder, "default-duration"));
	g_assert(priv->field_default_duration);
	g_signal_connect(priv->field_default_duration, "value-changed", G_CALLBACK(_spin_button_value_changed), task_properties_panel);

	gtk_orientable_set_orientation(GTK_ORIENTABLE(task_properties_panel), GTK_ORIENTATION_VERTICAL);
	gtk_widget_show_all(GTK_WIDGET(task_properties_panel));
	_set_edit_mode(task_properties_panel, FALSE);
}

static void
aion_task_properties_panel_dispose(GObject* object)
{
	AionTaskPropertiesPanel* panel = AION_TASK_PROPERTIES_PANEL(object);
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(panel);
	if (priv->task) {
		g_object_unref(priv->task);
		priv->task = NULL;
	}
	G_OBJECT_CLASS(aion_task_properties_panel_parent_class)->dispose(object);
}
 
static void
aion_task_properties_panel_finalize(GObject* object)
{
	//AionTaskPropertiesPanel* no_task_panel = AION_TASK_PROPERTIES_PANEL(object);
	G_OBJECT_CLASS(aion_task_properties_panel_parent_class)->finalize(object);
}
 
static void
aion_task_properties_panel_class_init(AionTaskPropertiesPanelClass* task_properties_panel_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(task_properties_panel_class);
	object_class->finalize = aion_task_properties_panel_finalize;
	object_class->dispose = aion_task_properties_panel_dispose;
}


static const gchar*
_aion_panelable_interface_get_name(AionPanelable* panel)
{
	return "properties-panel";
}


static const gchar*
_aion_panelable_interface_get_title(AionPanelable* panel)
{
	return _("Properties");
}

static const gchar*
_aion_panelable_interface_get_icon_name(AionPanelable* panel)
{
	return "document-properties-symbolic"; // TODO use specific icon
}

static void
_aion_panelable_interface_set_task(AionPanelable* panel, AionTask* task)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	g_return_if_fail(task == NULL || AION_IS_TASK(task));
	AionTaskPropertiesPanel* task_properties_panel = AION_TASK_PROPERTIES_PANEL(panel);
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(task_properties_panel);
	if (priv->task)
		g_object_unref(priv->task);
	priv->task = task;
	if (priv->task)
		g_object_ref(priv->task);
	_aion_task_properties_panel_refresh(task_properties_panel);
}

static void
_aion_panelable_interface_refresh(AionPanelable* panel)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	_aion_task_properties_panel_refresh(AION_TASK_PROPERTIES_PANEL(panel));
}


static gboolean
_aion_panelable_interface_is_editable(AionPanelable* panel)
{
	return TRUE;
}

static gboolean
_aion_panelable_interface_needs_save(AionPanelable* panel)
{
	g_assert(AION_IS_PANELABLE(panel));
	AionTaskPropertiesPanel* task_properties_panel = AION_TASK_PROPERTIES_PANEL(panel);
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(task_properties_panel);
	return priv->data_changed;
}

static void
_set_edit_mode(AionTaskPropertiesPanel* task_properties_panel, gboolean edit_mode)
{
	g_assert(AION_IS_TASK_PROPERTIES_PANEL(task_properties_panel));
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(task_properties_panel);
	priv->edit_mode = edit_mode;
	gtk_widget_set_sensitive(GTK_WIDGET(task_properties_panel), edit_mode);
	// can not set_sensitive for individual fields because set_sensitive is already used
	// when task repetition type is «None»
	gtk_widget_grab_focus(priv->field_task_label);
}

static void
_aion_panelable_interface_start_editing(AionPanelable* panel)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	AionTaskPropertiesPanel* task_properties_panel = AION_TASK_PROPERTIES_PANEL(panel);
	_set_edit_mode(task_properties_panel, TRUE);
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(task_properties_panel);
	_aion_panelable_private_emit_editing_started_signal(panel, priv->task);
}

static void
_aion_panelable_interface_end_editing(AionPanelable* panel, gboolean modification) {
	g_return_if_fail(AION_IS_PANELABLE(panel));
	AionTaskPropertiesPanel* task_properties_panel = AION_TASK_PROPERTIES_PANEL(panel);
	_set_edit_mode(task_properties_panel, FALSE);
	if (modification) {
		// save task properties
		gboolean changed = aion_task_properties_panel_validate_changes(task_properties_panel);
		if (! changed)
			modification = FALSE; // no fields have been modified
	}
	else {
		// restore task properties
		_aion_task_properties_panel_refresh(task_properties_panel);
	}
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(task_properties_panel);
	priv->data_changed = FALSE;
	_aion_panelable_private_emit_editing_finished_signal(panel, priv->task, modification);
}

static void
aion_task_properties_panel_panelable_interface_init(AionPanelableInterface* iface)
{
	iface->get_name = _aion_panelable_interface_get_name;
	iface->get_title = _aion_panelable_interface_get_title;
	iface->get_icon_name = _aion_panelable_interface_get_icon_name;
	iface->is_editable = _aion_panelable_interface_is_editable;
	iface->needs_save = _aion_panelable_interface_needs_save;
	iface->set_task = _aion_panelable_interface_set_task;
	iface->refresh = _aion_panelable_interface_refresh;
	iface->start_editing = _aion_panelable_interface_start_editing;
	iface->end_editing = _aion_panelable_interface_end_editing;
}

AionTaskPropertiesPanel*
aion_task_properties_panel_new()
{
	return g_object_new(AION_TYPE_TASK_PROPERTIES_PANEL, NULL);
}

void
_aion_task_properties_panel_refresh(AionTaskPropertiesPanel* task_properties_panel)
{
	g_assert(AION_IS_TASK_PROPERTIES_PANEL(task_properties_panel));
	g_assert(AION_IS_PANELABLE(task_properties_panel));
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(task_properties_panel);
	if (priv->task ==  NULL){
		gtk_entry_set_text(GTK_ENTRY(priv->field_task_label), "");
		gtk_switch_set_active(GTK_SWITCH(priv->field_task_enabled), FALSE);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_interval), 0);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(priv->field_postpone_from_exec_date), FALSE);
		gtk_entry_set_text(GTK_ENTRY(priv->field_task_label), "");
		gtk_switch_set_active(GTK_SWITCH(priv->field_task_enabled), FALSE);
		gtk_combo_box_set_active(GTK_COMBO_BOX(priv->field_repetition_type), AION_TASK_REPETITION_ONCE);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_interval), 0);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(priv->field_postpone_from_exec_date), TRUE);
		gtk_button_set_label(GTK_BUTTON(priv->field_next_execution), "");
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_preview_delay), 0);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_postponement_delay), 0);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_log_limit), 0);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_default_duration), 0);
		gtk_range_set_value(GTK_RANGE(priv->field_priority), AION_TASK_MINIMUM_PRIORITY);
		return;
	}

	// Field [label]
	const gchar* task_label = aion_task_get_label(priv->task);
	gtk_entry_set_text(GTK_ENTRY(priv->field_task_label), task_label);

	// Field [enabled]
	gboolean is_enabled = aion_task_is_enabled(priv->task);
	gtk_switch_set_active(GTK_SWITCH(priv->field_task_enabled), is_enabled);

	// Field [repetition type]
	AionTaskRepetitionType repetition_type = aion_task_get_repetition_type(priv->task);
	gtk_combo_box_set_active(GTK_COMBO_BOX(priv->field_repetition_type), repetition_type);

	// Field [interval]
	if (repetition_type == AION_TASK_REPETITION_ONCE) {
		gtk_widget_set_sensitive(priv->field_interval, FALSE);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_interval), 0);
	}
	else {
		gtk_widget_set_sensitive(priv->field_interval, TRUE);
		gint interval = aion_task_get_interval(priv->task);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_interval), interval);
	}

	// Fields [postpone_from_xxx]
	if (repetition_type == AION_TASK_REPETITION_ONCE) {
		gtk_widget_set_sensitive(priv->frame_postpone, FALSE);
	}
	else {
		gtk_widget_set_sensitive(priv->frame_postpone, TRUE);
		switch (aion_task_get_postpone_from_when(priv->task)) {
			case AION_TASK_POSTPONE_FROM_EXECUTION_DATE:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(priv->field_postpone_from_exec_date), TRUE);
				break;
			case AION_TASK_POSTPONE_FROM_DUE_DATE:
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(priv->field_postpone_from_due_date), TRUE);
				break;
			default:
				g_assert_not_reached();
		}
	}

	// Field [next_execution]
	const GDate* next_execution_date = aion_task_get_next_execution_date(priv->task);
	g_assert(g_date_valid(next_execution_date));

	gchar* next_execution_value = aion_util_get_date_local_representation(next_execution_date);
	gtk_button_set_label(GTK_BUTTON(priv->field_next_execution), next_execution_value);
	g_free(next_execution_value);

	guint year, month, day;
	if ( g_date_valid(next_execution_date)){
		year =  (guint) g_date_get_year(next_execution_date);
		month = (guint) g_date_get_month(next_execution_date);
		day =   (guint) g_date_get_day(next_execution_date);
	}
	else {
		GDateTime* today = g_date_time_new_now_local();
		year  = g_date_time_get_year(today);
		month = g_date_time_get_month(today);
		day   = g_date_time_get_day_of_month(today);
		g_date_time_unref(today);
	}
	_widget_set_dmy_data(priv->field_next_execution, year, month, day);

	// Field [preview_delay]
	gint preview_delay = aion_task_get_preview_delay(priv->task);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_preview_delay), preview_delay);

	// Field [postponement_delay]
	gint postponement_delay = aion_task_get_postponement_delay(priv->task);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_postponement_delay), postponement_delay);

	// Field [log_limit]
	gint log_limit = aion_task_get_log_entries_limit(priv->task);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_log_limit), log_limit);

	// Field [default_duration]
	gint default_duration = aion_task_get_default_duration(priv->task);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_default_duration), default_duration);

	// Field [priority]
	gint priority = aion_task_get_priority(priv->task);
	gtk_range_set_value(GTK_RANGE(priv->field_priority), priority);

	_set_edit_mode(task_properties_panel, FALSE);
	priv->data_changed = FALSE;
}

static gboolean
aion_task_properties_panel_validate_changes(AionTaskPropertiesPanel* task_properties_panel)
{
	g_return_val_if_fail(AION_IS_TASK_PROPERTIES_PANEL(task_properties_panel), FALSE);

	gboolean task_changed = FALSE;
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(task_properties_panel);

	// update field [label]
	const gchar* label_value = gtk_entry_get_text(GTK_ENTRY(priv->field_task_label));
	if (aion_task_set_label(priv->task, label_value))
		task_changed = TRUE;

	// update field [enabled]
	gboolean is_enabled = gtk_switch_get_active(GTK_SWITCH(priv->field_task_enabled));
	if (aion_task_set_enabled(priv->task, is_enabled))
		task_changed = TRUE;

	// update field [repetition_type]
	gint repetition_type = gtk_combo_box_get_active(GTK_COMBO_BOX(priv->field_repetition_type));
	if (aion_task_set_repetition_type(priv->task, repetition_type))
		task_changed = TRUE;

	// fields [postpone_from]
	gboolean postpone_from_due_date = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->field_postpone_from_due_date));
	AionTaskPostponeFromWhen from_when = postpone_from_due_date ? AION_TASK_POSTPONE_FROM_DUE_DATE : AION_TASK_POSTPONE_FROM_EXECUTION_DATE;
	if (aion_task_set_postpone_from_when(priv->task, from_when))
		task_changed = TRUE;

	// update field [interval]
	if (repetition_type != AION_TASK_REPETITION_ONCE) {
		gint interval = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->field_interval));
		if (aion_task_set_interval(priv->task, interval))
			task_changed = TRUE;
	} 

	// next execution date
	const gchar* dt_str = gtk_button_get_label(GTK_BUTTON(priv->field_next_execution));
	GDate* dt = g_date_new();
	// TODO do not use g_date_set_parse
	g_date_set_parse(dt, dt_str);
	if (! g_date_valid(dt))
		g_error("Invalid next execution date [%s]", dt_str);
	if (aion_task_set_next_execution_date(priv->task, dt))
		task_changed = TRUE;
	g_date_free(dt);

	// preview_delay
	gint preview_delay = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->field_preview_delay));
	if (aion_task_set_preview_delay(priv->task, preview_delay))
		task_changed = TRUE;

	// postponement_delay
	gint postponement_delay = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->field_postponement_delay));
	if (aion_task_set_postponement_delay(priv->task, postponement_delay))
		task_changed = TRUE;

	// next execution date
	gint log_limit = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->field_log_limit));
	if (aion_task_set_log_entries_limit(priv->task, log_limit))
		task_changed = TRUE;

	// update field [default_duration]
	gint default_duration = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->field_default_duration));
	if (aion_task_set_default_duration(priv->task, default_duration))
		task_changed = TRUE;

	gint priority = gtk_range_get_value(GTK_RANGE(priv->field_priority));
	if (aion_task_set_priority(priv->task, priority))
		task_changed = TRUE;

	g_debug("Task changed : %s", (task_changed) ? "yes" : "no");
	return task_changed;
}

static void
_widget_set_dmy_data(GtkWidget* widget, guint year, guint month, guint day)
{
	g_assert(GTK_IS_WIDGET(widget));
	g_object_set_data(G_OBJECT(widget), "year",  GUINT_TO_POINTER(year)); 
	g_object_set_data(G_OBJECT(widget), "month", GUINT_TO_POINTER(month));
	g_object_set_data(G_OBJECT(widget), "day",   GUINT_TO_POINTER(day));
}


static void
_widget_get_dmy_data(GtkWidget* widget, guint* year, guint* month, guint* day)
{
	g_assert(GTK_IS_WIDGET(widget));
	*year  = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(widget), "year"));
	*month = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(widget), "month"));
	*day   = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(widget), "day"));
}

static void
_calendar_response_callback(GtkDialog* dialog, gint response_id, gpointer data)
{
	GtkCalendar* calendar = GTK_CALENDAR(g_object_get_data(G_OBJECT(dialog), "calendar"));
	AionTaskPropertiesPanel* task_properties_panel = AION_TASK_PROPERTIES_PANEL(data);
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(task_properties_panel);
        if (response_id == GTK_RESPONSE_ACCEPT) {
		GDate* dt = aion_util_calendar_get_date(calendar); 
		guint year = g_date_get_year(dt);
		guint month = g_date_get_month(dt);
		guint day = g_date_get_day(dt);
		_widget_set_dmy_data(priv->field_next_execution, year, month, day);
		gchar* dt_str = aion_util_get_date_local_representation(dt);
		gtk_button_set_label(GTK_BUTTON(priv->field_next_execution), dt_str);
		g_free(dt_str);
		g_date_free(dt);
		priv->data_changed = TRUE;
	}
        gtk_widget_destroy(GTK_WIDGET(dialog));
}

static void
_next_execution_field_clicked (GtkButton* self, gpointer data)
{
	AionTaskPropertiesPanel* task_properties_panel = AION_TASK_PROPERTIES_PANEL(data);
        GtkDialog* dialog = GTK_DIALOG(gtk_dialog_new_with_buttons (
                        _("Date selection"),
                        NULL, // FIXME (GtkWindow*) main_window,
                        GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                        "OK",     GTK_RESPONSE_ACCEPT,
                        "Cancel", GTK_RESPONSE_CANCEL,
                        NULL, NULL));
        g_object_ref(G_OBJECT(dialog));

        gtk_window_set_default_size (GTK_WINDOW(dialog), 320, 200);
        //TODO gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(main_window));
        gtk_dialog_set_default_response(dialog, GTK_RESPONSE_ACCEPT);
        GtkWidget* content_area = gtk_dialog_get_content_area(dialog);
	// on Librem5, day button is not easy to click without a border
	gtk_container_set_border_width(GTK_CONTAINER(content_area), 15);

	GtkWidget* calendar = gtk_calendar_new();
	gtk_box_pack_start(GTK_BOX(content_area), calendar, TRUE, TRUE, 5); // FIXME hard coded spacing
	gtk_widget_show_all(GTK_WIDGET(dialog));
	g_object_set_data(G_OBJECT(dialog), "calendar", calendar);
	// TODO disable validation button if selected date is in the paste

	guint year, month, day;
	_widget_get_dmy_data(GTK_WIDGET(self), &year, &month, &day);
	gtk_calendar_select_month(GTK_CALENDAR(calendar), month-1, year);
	gtk_calendar_select_day(GTK_CALENDAR(calendar), day);

	g_signal_connect(dialog, "response", G_CALLBACK(_calendar_response_callback), task_properties_panel);
	g_object_unref(dialog);
}

static void
_repetition_type_changed(GtkComboBox* self, gpointer data)
{
	AionTaskPropertiesPanel* task_properties_panel = AION_TASK_PROPERTIES_PANEL(data);
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(task_properties_panel);
	gint repetition_type = gtk_combo_box_get_active(self);
	// fields hidden if repetition_type is Once
	gboolean show = (repetition_type != AION_TASK_REPETITION_ONCE);
	gtk_widget_set_sensitive(priv->field_interval, show);
	gtk_widget_set_sensitive(priv->frame_postpone, show);
	// update Interval field because its value changes when type becomes Once
	gint interval = 0;
	if (repetition_type != AION_TASK_REPETITION_ONCE) {
		interval = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->field_interval));
		if (interval < 1)
			interval = 1;
	}
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->field_interval), interval);
	priv->data_changed = TRUE;
}

static void
_entry_buffer_changed(GtkEntryBuffer *buffer, guint position, gchar *chars, guint n_chars, gpointer data)
{
	AionTaskPropertiesPanel* panel = AION_TASK_PROPERTIES_PANEL(data);
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(panel);
	priv->data_changed = TRUE;
}

static void
_spin_button_value_changed(GtkSpinButton *spin_button, gpointer data)
{
	AionTaskPropertiesPanel* panel = AION_TASK_PROPERTIES_PANEL(data);
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(panel);
	priv->data_changed = TRUE;
}

static void
_toogle_button_changed(GtkToggleButton *togglebutton, gpointer data)
{
	AionTaskPropertiesPanel* panel = AION_TASK_PROPERTIES_PANEL(data);
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(panel);
	priv->data_changed = TRUE;
}

static void
_switch_state_changed (GtkSwitch* gtk_switch, GParamSpec *pspec, gpointer data)
{
	AionTaskPropertiesPanel* panel = AION_TASK_PROPERTIES_PANEL(data);
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(panel);
	priv->data_changed = TRUE;
}

static void
_priority_scale_value_changed(GtkRange* gtk_range, gpointer data)
{
	AionTaskPropertiesPanel* panel = AION_TASK_PROPERTIES_PANEL(data);
	AionTaskPropertiesPanelPrivate* priv = aion_task_properties_panel_get_instance_private(panel);
	priv->data_changed = TRUE;
}

// TODO add instance_counter
