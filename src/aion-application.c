/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#define G_LOG_DOMAIN "Aion-Application"
#include <glib/gi18n.h>
#include <libhandy-1/handy.h>

#include "aion-application.h"
#include "aion-main-window.h"

#define AION_APPLICATION_ID "org.gnome.gitlab.powimod.aion-task"
#define  XML_FILE_NAME "aion-task.xml"

struct _AionApplication {
	GtkApplication parentInstance;
};

typedef struct 
{
	AionMainWindow* main_window;
	AionTaskManager* task_manager;
	gchar* data_dir_path;
	gboolean load_success;
} AionApplicationPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(AionApplication, aion_application, GTK_TYPE_APPLICATION)

static gint instance_counter = 0;

// local prototype
static void aion_application_dispose(GObject* object);
static void aion_application_finalize(GObject* object);
static void _fatal_error(AionApplication*, const gchar*, GError*);

static void
aion_application_init(AionApplication* aion_application)
{
	AionApplicationPrivate* priv = aion_application_get_instance_private(aion_application);
	priv->main_window = NULL;
	priv->task_manager = aion_task_manager_new();
	priv->data_dir_path = NULL;
	priv->load_success = FALSE;
	instance_counter++;
}


static void
aion_application_dispose(GObject* object)
{
	g_return_if_fail(AION_IS_APPLICATION(object));
	AionApplication* aion_application = AION_APPLICATION(object);
	AionApplicationPrivate* priv = aion_application_get_instance_private(aion_application);
	g_assert(priv->main_window == NULL);
	// FIXME where to unref TaskManager (in dispose or in finalize function) ?
	g_object_unref(priv->task_manager);
	priv->task_manager = NULL;
	G_OBJECT_CLASS(aion_application_parent_class)->dispose(object);
}


static void
aion_application_finalize(GObject* object)
{
	g_return_if_fail(AION_IS_APPLICATION(object));
	AionApplication* aion_application = AION_APPLICATION(object);
	AionApplicationPrivate* priv = aion_application_get_instance_private(aion_application);
	if (priv->data_dir_path != NULL)
		g_free(priv->data_dir_path);
	G_OBJECT_CLASS(aion_application_parent_class)->finalize(object);
	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_APPLICATION));
}

static void  
_main_window_main_window_destroyed(gpointer data, GObject* where_the_object_was)
{
	AionApplication* aion_application = AION_APPLICATION(data);
	AionApplicationPrivate* priv = aion_application_get_instance_private(aion_application);
	priv->main_window = NULL;

	GError* error = NULL;
	if (! aion_application_save_data(aion_application, &error)) {
		_fatal_error(aion_application, "Can't save Aion-Task data file", error);
		return;
	}
}

static void
_fatal_dialog_response(GtkDialog* dialog, gint response_id, gpointer data)
{
	AionApplication* aion_application = AION_APPLICATION(data);
	gtk_widget_destroy(GTK_WIDGET(dialog));
	g_application_release(G_APPLICATION(aion_application));
}

static void
_fatal_error(AionApplication* aion_application, const gchar* message, GError* error)
{
	g_assert(AION_IS_APPLICATION(aion_application));
	g_assert(message != NULL);

	if (error) 
		g_warning("Fatal error : %s (%s) !", message, error->message);
	else 
		g_warning("Fatal error : %s !", message);

	GtkWidget* dialog = gtk_dialog_new();
	gtk_container_set_border_width(GTK_CONTAINER(dialog), 10);
	gtk_dialog_add_button (GTK_DIALOG(dialog), _("Close"), GTK_RESPONSE_CLOSE);
	GtkWidget* content_area =  gtk_dialog_get_content_area(GTK_DIALOG(dialog));
	gtk_box_set_homogeneous(GTK_BOX(content_area), FALSE);

	GtkWidget* h_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	gtk_widget_show(h_box);
	gtk_box_pack_start(GTK_BOX(content_area), h_box, TRUE, TRUE, 5);

	GtkWidget *icon = gtk_image_new_from_icon_name ("dialog-error", GTK_ICON_SIZE_DIALOG);
	gtk_widget_set_valign(icon, GTK_ALIGN_START);
	gtk_widget_show(icon);
	gtk_box_pack_start(GTK_BOX(h_box), icon, FALSE, FALSE, 5);

	GtkWidget* v_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
	gtk_widget_show(v_box);
	gtk_box_pack_start(GTK_BOX(h_box), v_box, FALSE, FALSE, 5);

	GtkWidget* label = gtk_label_new(NULL);
	gchar* markup = g_strdup_printf("<b>Fatal error : %s !</b>", message);
	gtk_label_set_markup(GTK_LABEL(label), markup);
	g_free(markup);
	gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
	gtk_label_set_line_wrap_mode(GTK_LABEL(label), PANGO_WRAP_CHAR);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(v_box), label, FALSE, FALSE, 5);

	if (error != NULL) {
		GtkWidget* frame = gtk_frame_new(NULL);
		gtk_widget_show(frame);
		gtk_box_pack_start(GTK_BOX(v_box), frame, TRUE, TRUE, 5);

		GtkWidget *scrolled_window = gtk_scrolled_window_new(NULL, NULL);
		gtk_widget_show(scrolled_window);
		gtk_container_set_border_width(GTK_CONTAINER(scrolled_window), 5);
		gtk_container_add(GTK_CONTAINER(frame), scrolled_window);

		GtkWidget *text_view = gtk_text_view_new();
		gtk_text_view_set_editable(GTK_TEXT_VIEW(text_view), FALSE);
		gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(text_view), GTK_WRAP_WORD);
		gtk_widget_show(text_view);
		gtk_container_add(GTK_CONTAINER(scrolled_window), text_view);

		GtkTextBuffer *text_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
		gtk_text_buffer_set_text(text_buffer, error->message, -1);
	}

	g_signal_connect(dialog, "response", G_CALLBACK(_fatal_dialog_response), aion_application);
	g_application_hold(G_APPLICATION(aion_application));
	g_assert(dialog);
	gtk_widget_show(dialog);
}


static void
aion_application_real_startup (GApplication *g_application)
{
	G_APPLICATION_CLASS (aion_application_parent_class)->startup(g_application);
	hdy_init();

	// load XML data file
	AionApplication* aion_application = AION_APPLICATION(g_application);
	AionApplicationPrivate* priv = aion_application_get_instance_private(aion_application);
	GError* error = NULL;
	if (! aion_application_load_data(aion_application, &error)) {
		_fatal_error(aion_application, "Can't load Aion-Task data file", error);
		return;
	}
	priv->load_success = TRUE;
}


static void
aion_application_real_activate (GApplication *g_application)
{
	AionApplication* aion_application = AION_APPLICATION(g_application);
	AionApplicationPrivate* priv = aion_application_get_instance_private(aion_application);
	if (priv->load_success) {
		priv->main_window = aion_main_window_new(aion_application);
		g_object_weak_ref(G_OBJECT(priv->main_window), _main_window_main_window_destroyed, aion_application);
		aion_main_window_declare_actions(priv->main_window);
		aion_main_window_display_data(priv->main_window);
	}
}

static void
aion_application_real_shutdown (GApplication *g_application)
{
	G_APPLICATION_CLASS (aion_application_parent_class)->shutdown(g_application);
}


static void
aion_application_class_init(AionApplicationClass* aion_application_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(aion_application_class);
	object_class->dispose= aion_application_dispose;
	object_class->finalize = aion_application_finalize;
	GApplicationClass *g_application_class = G_APPLICATION_CLASS (aion_application_class);
	g_application_class->startup = aion_application_real_startup;
	g_application_class->activate = aion_application_real_activate;
	g_application_class->shutdown = aion_application_real_shutdown;
}


AionApplication* 
aion_application_new(const gchar* data_dir_path)
{
	g_assert(data_dir_path != NULL);
	AionApplication* self = AION_APPLICATION(g_object_new(AION_TYPE_APPLICATION, 
		"application-id", AION_APPLICATION_ID,
		// FIXME pass data_dir_path as a property
		NULL));

	// data directory
	if (! g_file_test(data_dir_path, G_FILE_TEST_EXISTS))  {
		g_info("Creating data dir [%s]...",  data_dir_path);
		gint result = g_mkdir_with_parents(data_dir_path, 0755);
		if (result != 0)
			g_error("Can not create [%s] directory",  data_dir_path);
	}
	else {
		if (! g_file_test(data_dir_path, G_FILE_TEST_IS_DIR))
			g_error("Path [%s] exists but is not a directory",  data_dir_path);
	}
	AionApplicationPrivate* priv = aion_application_get_instance_private(self);
	priv->data_dir_path = g_strdup(data_dir_path);

	// data file
	gchar* data_file_path = g_build_filename(data_dir_path, XML_FILE_NAME, NULL);
	g_info("XML data file : %s", data_file_path);
	aion_task_manager_initialize_task_manager(priv->task_manager, (const gchar*)data_file_path);
	g_free(data_file_path);

	return self;
}

AionTaskManager*
aion_application_get_task_manager(AionApplication* aion_application)
{
	g_return_val_if_fail(AION_IS_APPLICATION(aion_application), NULL);
	AionApplicationPrivate* priv = aion_application_get_instance_private(aion_application);
	return priv->task_manager;
}


gboolean
aion_application_save_data(AionApplication* aion_application, GError** p_error)
{
	g_return_val_if_fail(AION_IS_APPLICATION(aion_application), FALSE);
	AionApplicationPrivate* priv = aion_application_get_instance_private(aion_application);
	return aion_task_manager_save_data(priv->task_manager, p_error);
}

gboolean
aion_application_load_data(AionApplication* aion_application, GError** p_error)
{
	g_return_val_if_fail(AION_IS_APPLICATION(aion_application), FALSE);
	AionApplicationPrivate* priv = aion_application_get_instance_private(aion_application);
	return aion_task_manager_load_data(priv->task_manager, p_error);
}

gint
aion_application_get_instance_count()
{
	return instance_counter;
}

gint
aion_application_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_APPLICATION));
	return instance_counter;
	
}
