/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-TaskListRow"
#include <glib/gi18n.h>

#include "aion-task-list-row.h"
#include "util.h"

// TODO cleanup
// #define AION_TASK_LIST_ROW_MIN_WIDTH 300

struct _AionTaskListRow {
	GtkFrame parent;
};

typedef struct {
	AionTask* aion_task;
	GtkWidget* task_state_field;
	GtkWidget* task_label_field;
	GtkWidget* task_status_field;
} AionTaskListRowPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(AionTaskListRow, aion_task_list_row, GTK_TYPE_FRAME);

static gint instance_counter = 0;

static void
aion_task_list_row_init(AionTaskListRow* task_list_row)
{
	g_return_if_fail(AION_IS_TASK_LIST_ROW(task_list_row));
	AionTaskListRowPrivate* priv = aion_task_list_row_get_instance_private(task_list_row);
	priv->aion_task = NULL;

	// TODO cleanup
	//gtk_widget_set_size_request(GTK_WIDGET(task_list_row), AION_TASK_LIST_ROW_MIN_WIDTH, -1);

	GtkWidget* v_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5); // FIXME hard coded spacing
	gtk_container_add(GTK_CONTAINER(task_list_row), v_box);
	gtk_container_set_border_width(GTK_CONTAINER(v_box), 10); // FIXME hard coded spacing

	priv->task_label_field = gtk_label_new(NULL);
	gtk_box_pack_start(GTK_BOX(v_box), priv->task_label_field, FALSE, FALSE, 5);
	//gtk_container_add(GTK_CONTAINER(v_box), priv->task_label_field);
	gtk_widget_set_halign(priv->task_label_field, GTK_ALIGN_START);

	// horizontal box with task state icon and remaining days
	GtkWidget* h_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5); // FIXME hard coded spacing
	gtk_container_add(GTK_CONTAINER(v_box), h_box);

	priv->task_state_field = gtk_image_new();
	gtk_image_set_from_icon_name(GTK_IMAGE(priv->task_state_field), "emoji-recent-symbolic", GTK_ICON_SIZE_BUTTON);
	gtk_container_add(GTK_CONTAINER(h_box), priv->task_state_field);

	priv->task_status_field = gtk_label_new(NULL);
	gtk_container_add(GTK_CONTAINER(h_box), priv->task_status_field);
	gtk_widget_set_halign(priv->task_status_field, GTK_ALIGN_START);


	gtk_widget_show_all(GTK_WIDGET(task_list_row));
	aion_task_list_row_refresh(task_list_row);
	instance_counter++;
}

static void
aion_task_list_row_dispose(GObject* object)
{
	AionTaskListRow* task_list_row = AION_TASK_LIST_ROW(object);
	AionTaskListRowPrivate *priv = aion_task_list_row_get_instance_private(task_list_row);
	if (priv->aion_task != NULL) {
		g_object_unref(priv->aion_task);
		priv->aion_task = NULL;
	}
	G_OBJECT_CLASS(aion_task_list_row_parent_class)->dispose(object);
}


static void
aion_task_list_row_finalize(GObject* object)
{
	//AionTaskListRow* task_list_row = AION_TASK_LIST_ROW(object);
	//AionTaskListRowPrivate *priv = aion_task_list_row_get_instance_private(task_list_row);
	G_OBJECT_CLASS(aion_task_list_row_parent_class)->finalize(object);
	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_TASK_LIST_ROW));
}

 
static void
aion_task_list_row_class_init(AionTaskListRowClass* task_list_row_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(task_list_row_class);
	object_class->dispose= aion_task_list_row_dispose;
	object_class->finalize = aion_task_list_row_finalize;
}


AionTaskListRow*
aion_task_list_row_new(AionTask* aion_task)
{
	g_return_val_if_fail(aion_task == NULL || AION_IS_TASK(aion_task), NULL);
	AionTaskListRow* task_list_row = AION_TASK_LIST_ROW(g_object_new(AION_TYPE_TASK_LIST_ROW, NULL));
	if (aion_task)
		aion_task_list_row_set_task(task_list_row, aion_task);
	return task_list_row;
}

 
void
aion_task_list_row_set_task(AionTaskListRow* aion_task_list_row, AionTask* aion_task)
{
	g_return_if_fail(AION_IS_TASK_LIST_ROW(aion_task_list_row));
	g_return_if_fail(AION_IS_TASK(aion_task));
	AionTaskListRowPrivate *priv = aion_task_list_row_get_instance_private(aion_task_list_row);
	if (priv->aion_task == aion_task)
		return;
	if (priv->aion_task != NULL)
		g_object_unref(priv->aion_task);
	g_object_ref(aion_task);
	priv->aion_task = aion_task;
	aion_task_list_row_refresh(aion_task_list_row);
}

const AionTask*
aion_task_list_row_get_task(AionTaskListRow* aion_task_list_row)
{
	g_return_val_if_fail(AION_IS_TASK_LIST_ROW(aion_task_list_row), NULL);
	AionTaskListRowPrivate *priv = aion_task_list_row_get_instance_private(aion_task_list_row);
	return priv->aion_task;
}

void
aion_task_list_row_refresh(AionTaskListRow* task_list_row)
{
	const gchar* state_icon_name;
	gchar* task_label_field_value = NULL;
	gchar* task_status_value = NULL;

	g_return_if_fail(AION_IS_TASK_LIST_ROW(task_list_row));
	AionTaskListRowPrivate* priv = aion_task_list_row_get_instance_private(task_list_row);
	AionTask* task = priv->aion_task;

	if (task ==  NULL){
		state_icon_name = "dialog-error-symbolic";
		task_label_field_value = g_strdup(_("no task to display"));
		// TODO make field not visible when no task is selected
		task_status_value = g_strdup("");
	} 
	else {
		// TODO use <span> pango markup
		task_label_field_value = g_markup_printf_escaped("<i><b>%s</b></i>", aion_task_get_label(task));
				
		if (! aion_task_is_enabled(task)) {
			task_status_value = g_strdup("Disabled");
			state_icon_name = "action-unavailable-symbolic";
		}
		else {
			const GDate* next_execution_date = aion_task_get_next_execution_date(task);
			g_assert(g_date_valid(next_execution_date));
			gchar* date_str = aion_util_get_date_local_representation(next_execution_date);
			g_assert(date_str != NULL);

			gint remaining_days = aion_task_get_remaining_days(task);
			gint postponement_delay = aion_task_get_postponement_delay(task);
			if (remaining_days == 0) 
				task_status_value = g_strdup(_("Today"));
			else if (remaining_days < 0) 
				task_status_value = g_strdup_printf(_("%s - %i days(s) late"), date_str, -remaining_days);
			else if (postponement_delay > 0)
				task_status_value = g_strdup_printf(_("%s - postponed for %i days(s)"), date_str, postponement_delay);
			else
				task_status_value = g_strdup_printf(_("%s - %i days(s) remaining"), date_str, remaining_days);
			
			g_free(date_str);

			switch (aion_task_get_state(task)) {
				case AION_TASK_STATE_DISABLED:
					state_icon_name = "action-unavailable-symbolic";
					break;
				case AION_TASK_STATE_PREVIEW:
				case AION_TASK_STATE_PENDING:
					state_icon_name = "alarm-symbolic";
					break;
				case AION_TASK_STATE_ON_TIME:
					state_icon_name = "dialog-information-symbolic";
					break;
				case AION_TASK_STATE_POSTPONED:
				case AION_TASK_STATE_OVERDUE:
					state_icon_name = "dialog-warning-symbolic";
					break;
				default:
					g_warning("Invalid task state");
					state_icon_name = "process-stop-symbolic";
					break;
			}
		}
	}

	gtk_image_set_from_icon_name(GTK_IMAGE(priv->task_state_field), state_icon_name, GTK_ICON_SIZE_BUTTON);
	g_assert(task_label_field_value != NULL);
	gtk_label_set_markup(GTK_LABEL(priv->task_label_field), task_label_field_value);
	gtk_label_set_text(GTK_LABEL(priv->task_status_field), task_status_value);

	g_free(task_label_field_value);
	g_free(task_status_value);
}

gint
aion_task_list_row_get_instance_count()
{
	return instance_counter;
}

gint
aion_task_list_row_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_TASK_LIST_ROW));
	return instance_counter;
}
