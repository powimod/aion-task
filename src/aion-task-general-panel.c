/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-TaskGeneralPanel"
#include <glib/gi18n.h>

#include "aion-task-general-panel.h"
#include "aion-panelable.h"
#include "aion-panelable-private.h"
#include "util.h"

struct _AionTaskGeneralPanel 
{
	GtkBox parent;
};

typedef struct {
        AionTask* task;
	GtkWidget* task_label_field;
	GtkWidget* task_state_image_field;
	GtkWidget* task_state_text_field;
	GtkWidget* task_repetition_field;
	GtkWidget* task_execution_field;
} AionTaskGeneralPanelPrivate;

static void aion_task_general_panel_panelable_interface_init(AionPanelableInterface*);
static void _aion_task_general_panel_refresh(AionTaskGeneralPanel*);

G_DEFINE_TYPE_WITH_CODE (AionTaskGeneralPanel, aion_task_general_panel, GTK_TYPE_BOX,
	G_ADD_PRIVATE(AionTaskGeneralPanel)
	G_IMPLEMENT_INTERFACE(AION_TYPE_PANELABLE, aion_task_general_panel_panelable_interface_init)
);

static void
aion_task_general_panel_init (AionTaskGeneralPanel* task_general_panel)
{
	GtkWidget* button;
	GtkWidget* button_image;

	AionTaskGeneralPanelPrivate* priv = aion_task_general_panel_get_instance_private(task_general_panel);
	priv->task = NULL;

	// field [task_label]

	GtkWidget* h_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5); // FIXME hard coded spacing
	gtk_box_pack_start(GTK_BOX(task_general_panel), h_box, FALSE, FALSE, 5);

	GtkWidget* image = gtk_image_new();
	gtk_image_set_from_icon_name(GTK_IMAGE(image), "object-select-symbolic", GTK_ICON_SIZE_BUTTON); // TODO specific icon
	gtk_box_pack_start(GTK_BOX(h_box), image, FALSE, FALSE, 0);

	GtkWidget* label = gtk_label_new(NULL);
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_label_set_ellipsize(GTK_LABEL(label), PANGO_ELLIPSIZE_END);
	gtk_box_pack_start(GTK_BOX(h_box), label, FALSE, FALSE, 0); // FIXME hard coded spacing
	priv->task_label_field = label;


	// field [task_repetition]

	h_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5); // FIXME hard coded spacing
	gtk_box_pack_start(GTK_BOX(task_general_panel), h_box, FALSE, FALSE, 5);

	image = gtk_image_new();
	gtk_image_set_from_icon_name(GTK_IMAGE(image), "view-refresh-symbolic", GTK_ICON_SIZE_BUTTON); // TODO specific icon
	gtk_box_pack_start(GTK_BOX(h_box), image, FALSE, FALSE, 0);

	label  = gtk_label_new(NULL);
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_box_pack_start(GTK_BOX(h_box), label, FALSE, FALSE, 0);
	priv->task_repetition_field  = label;

	// horizontal box with task state icon and text

	h_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5); // FIXME hard coded spacing
	gtk_box_pack_start(GTK_BOX(task_general_panel), h_box, FALSE, FALSE, 5);

	priv->task_state_image_field = gtk_image_new();
	gtk_box_pack_start(GTK_BOX(h_box), priv->task_state_image_field, FALSE, FALSE, 0);

	priv->task_state_text_field  = gtk_label_new(NULL);
	gtk_box_pack_start(GTK_BOX(h_box), priv->task_state_text_field , TRUE, TRUE, 0);
	gtk_widget_set_halign(priv->task_state_text_field , GTK_ALIGN_START);

	// field [execution]
	h_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_box_pack_start(GTK_BOX(task_general_panel), h_box, FALSE, FALSE, 5);


	image = gtk_image_new();
	gtk_image_set_from_icon_name(GTK_IMAGE(image), "view-refresh-symbolic", GTK_ICON_SIZE_BUTTON); // TODO specific icon
	gtk_box_pack_start(GTK_BOX(h_box), image, FALSE, FALSE, 0);

	label  = gtk_label_new(NULL);
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_box_pack_start(GTK_BOX(h_box), label, FALSE, FALSE, 0);
	priv->task_execution_field = label;


	// toolbar
	GtkWidget* toolbar_box = gtk_flow_box_new();
	gtk_flow_box_set_homogeneous(GTK_FLOW_BOX(toolbar_box), TRUE);
	gtk_widget_set_margin_start(toolbar_box, 10);
	gtk_widget_set_margin_end(toolbar_box, 10);
	gtk_widget_show(toolbar_box);
	gtk_box_pack_end(GTK_BOX(task_general_panel), toolbar_box, FALSE, FALSE, 5); // FIXME hard coded spacing

	button = gtk_button_new_with_label(_("Validate"));
	gtk_widget_show(button);
	gtk_button_set_always_show_image(GTK_BUTTON(button), TRUE);
	button_image = gtk_image_new_from_icon_name("gtk-apply", GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_widget_show(button_image);
	gtk_button_set_image(GTK_BUTTON(button), button_image);
	gtk_actionable_set_action_name (GTK_ACTIONABLE(button), "win.validate-current-task");
	gtk_flow_box_insert(GTK_FLOW_BOX(toolbar_box), button, -1); // FIXME hard coded spacing

	button = gtk_button_new_with_label(_("Postpone"));
	gtk_widget_show(button);
	gtk_button_set_always_show_image(GTK_BUTTON(button), TRUE);
	button_image = gtk_image_new_from_icon_name("go-jump-symbolic", GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_widget_show(button_image);
	gtk_button_set_image(GTK_BUTTON(button), button_image);
	gtk_actionable_set_action_name (GTK_ACTIONABLE(button), "win.postpone-current-task");
	gtk_flow_box_insert(GTK_FLOW_BOX(toolbar_box), button, -1); // FIXME hard coded spacing

	button = gtk_button_new_with_label(_("Delete"));
	gtk_widget_show(button);
	gtk_button_set_always_show_image(GTK_BUTTON(button), TRUE);
	button_image = gtk_image_new_from_icon_name("edit-delete-symbolic", GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_widget_show(button_image);
	gtk_button_set_image(GTK_BUTTON(button), button_image);
	gtk_actionable_set_action_name (GTK_ACTIONABLE(button), "win.delete-current-task");
	gtk_flow_box_insert(GTK_FLOW_BOX(toolbar_box), button, -1); // FIXME hard coded spacing

	gtk_orientable_set_orientation(GTK_ORIENTABLE(task_general_panel), GTK_ORIENTATION_VERTICAL);
	gtk_widget_show_all(GTK_WIDGET(task_general_panel));
}

static void
aion_task_general_panel_dispose(GObject* object)
{
	AionTaskGeneralPanel* panel = AION_TASK_GENERAL_PANEL(object);
	AionTaskGeneralPanelPrivate* priv = aion_task_general_panel_get_instance_private(panel);
	if (priv->task) {
		g_object_unref(priv->task);
		priv->task = NULL;
	}
	G_OBJECT_CLASS(aion_task_general_panel_parent_class)->dispose(object);
}
 
static void
aion_task_general_panel_finalize(GObject* object)
{
	//AionTaskGeneralPanel* task_general_panel = AION_TASK_GENERAL_PANEL(object);
	G_OBJECT_CLASS(aion_task_general_panel_parent_class)->finalize(object);
}
 
static void
aion_task_general_panel_class_init(AionTaskGeneralPanelClass* task_general_panel_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(task_general_panel_class);
	object_class->finalize = aion_task_general_panel_finalize;
	object_class->dispose = aion_task_general_panel_dispose;
}


static const gchar*
_aion_panelable_interface_get_name(AionPanelable* panel)
{
	return "general-panel";
}


static const gchar*
_aion_panelable_interface_get_title(AionPanelable* panel)
{
	return _("Generalities");
}

static const gchar*
_aion_panelable_interface_get_icon_name(AionPanelable* panel)
{
	return "go-home-symbolic"; // TODO use specific icon
}

static void
_aion_panelable_interface_set_task(AionPanelable* panel, AionTask* task)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	g_return_if_fail(task == NULL || AION_IS_TASK(task));
	AionTaskGeneralPanel* task_general_panel = AION_TASK_GENERAL_PANEL(panel);
	AionTaskGeneralPanelPrivate* priv = aion_task_general_panel_get_instance_private(task_general_panel);
	if (priv->task)
		g_object_unref(priv->task);
	priv->task = task;
	if (priv->task)
		g_object_ref(priv->task);
	_aion_task_general_panel_refresh(task_general_panel);
}

static void
_aion_panelable_interface_refresh(AionPanelable* panel)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	_aion_task_general_panel_refresh(AION_TASK_GENERAL_PANEL(panel));
}



static gboolean
_aion_panelable_interface_is_editable(AionPanelable* panel)
{
	return FALSE;
}

static gboolean
_aion_panelable_interface_needs_save(AionPanelable* panel)
{
	return FALSE;
}

static void
_aion_panelable_interface_start_editing(AionPanelable* panel)
{
	g_assert_not_reached(); // editing is not implemented by this panel
}

static void
_aion_panelable_interface_end_editing(AionPanelable* panel, gboolean modification)
{
	g_assert_not_reached(); // editing is not implemented by this panel
}


static void
aion_task_general_panel_panelable_interface_init(AionPanelableInterface* iface)
{
	iface->get_name = _aion_panelable_interface_get_name;
	iface->get_title = _aion_panelable_interface_get_title;
	iface->get_icon_name = _aion_panelable_interface_get_icon_name;
	iface->set_task = _aion_panelable_interface_set_task;
	iface->refresh = _aion_panelable_interface_refresh;
	iface->is_editable = _aion_panelable_interface_is_editable;
	iface->needs_save = _aion_panelable_interface_needs_save;
	iface->start_editing = _aion_panelable_interface_start_editing;
	iface->end_editing = _aion_panelable_interface_end_editing;
}

AionTaskGeneralPanel*
aion_task_general_panel_new()
{
	return g_object_new(AION_TYPE_TASK_GENERAL_PANEL, NULL);
}

static void
_aion_task_general_panel_refresh(AionTaskGeneralPanel* panel)
{
	g_assert(AION_IS_TASK_GENERAL_PANEL(panel));
	g_assert(AION_IS_PANELABLE(panel));
	AionTaskGeneralPanelPrivate* priv = aion_task_general_panel_get_instance_private(panel);
	if (priv->task == NULL) {
		gtk_label_set_markup(GTK_LABEL(priv->task_label_field), "");
		gtk_image_set_from_icon_name(GTK_IMAGE(priv->task_state_image_field), "action-unavailable-symbolic", GTK_ICON_SIZE_BUTTON);
		gtk_label_set_text(GTK_LABEL(priv->task_state_text_field ), "");
		gtk_label_set_text(GTK_LABEL(priv->task_repetition_field), "");
		gtk_label_set_markup(GTK_LABEL(priv->task_execution_field), "");
		return;
	}

	// refresh task label
	gchar* label_text;
	if (priv->task)
		label_text =  g_markup_printf_escaped(_("Task : <b><i>%s</i></b>"), aion_task_get_label(priv->task));
	else
		label_text = g_strdup("");
	gtk_label_set_markup(GTK_LABEL(priv->task_label_field), label_text);
	g_free(label_text);

	// refresh task state (icon and text)
	gchar* state_str = NULL;
	const gchar* state_icon_name = NULL;
	gint remaining_days = aion_task_get_remaining_days(priv->task);
	switch (aion_task_get_state(priv->task)) {
		case AION_TASK_STATE_DISABLED:
			state_icon_name = "action-unavailable-symbolic";
			state_str = g_strdup(_("Task is disabled"));
			break;
		case AION_TASK_STATE_PREVIEW:
		case AION_TASK_STATE_PENDING:
			state_icon_name = "alarm-symbolic";
			if (remaining_days == 1)
				state_str = g_strdup(_("Tomorrow"));
			else
				state_str = g_strdup_printf(_("%i days remaining"), remaining_days);
			break;
		case AION_TASK_STATE_ON_TIME:
			state_icon_name = "dialog-information-symbolic";
			state_str = g_strdup(_("Today"));
			break;
		case AION_TASK_STATE_POSTPONED:
		case AION_TASK_STATE_OVERDUE:
			state_icon_name = "dialog-warning-symbolic";
			if (remaining_days == -1)
				state_str = g_strdup(_("Yesterday"));
			else
				state_str = g_strdup_printf(_("%i days late"), -remaining_days);
			break;
		default: 
			g_error("Invalid task state");
	}
	gint postponement_delay = aion_task_get_postponement_delay(priv->task);
	if (postponement_delay > 0){
		const GDate* next_execution_date = aion_task_get_next_execution_date(priv->task);
		gchar* date_str = aion_util_get_date_local_representation(next_execution_date);
		gchar* state_str_bis = g_strdup_printf("%s : %s + %i day(s)",  // TODO translation
						state_str,
						date_str,
						postponement_delay
					);
		g_free(date_str);
		g_free(state_str);
		state_str = state_str_bis;
	}

	g_assert(state_str != NULL);
	gtk_label_set_text(GTK_LABEL(priv->task_state_text_field ), state_str);
	g_free(state_str);

	g_assert(state_icon_name != NULL);
	gtk_image_set_from_icon_name(GTK_IMAGE(priv->task_state_image_field), state_icon_name, GTK_ICON_SIZE_BUTTON);

	gchar* human_repetition_str = aion_task_get_human_representation_of_repetition(priv->task);
	gchar* repetition_field_value = g_strdup_printf(_("Repetition : %s"), human_repetition_str);
	g_free(human_repetition_str);
	gtk_label_set_text(GTK_LABEL(priv->task_repetition_field), repetition_field_value);
	g_free(repetition_field_value);

	// refresh task_execution_field
	gchar* str;
	if (priv->task) {
		str =  g_strdup_printf(_("Execution count : %i"), 
				aion_task_get_execution_count(priv->task));
		gint total_duration = aion_task_get_total_duration(priv->task);
		if (total_duration > 0){
			gchar* duration_str = aion_util_get_duration_human_representation(total_duration);
			gchar* str_ext = g_strdup_printf("%s (%s)", str, duration_str);
			g_free(duration_str);
			g_free(str);
			str = str_ext;
		}
	}
	else
		str = g_strdup("");
	gtk_label_set_markup(GTK_LABEL(priv->task_execution_field), str);
	g_free(str);

}


// TODO add instance_counter
