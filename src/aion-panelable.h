/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_PANELABLE__
#define __AION_PANELABLE__

#include "aion-task.h"
#include <gtk/gtk.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define AION_TYPE_PANELABLE aion_panelable_get_type()

G_DECLARE_INTERFACE(AionPanelable, aion_panelable, AION, PANELABLE, GObject)

struct _AionPanelableInterface
{
	GTypeInterface parent_iface;
	const gchar* (*get_name) (AionPanelable*);
	const gchar* (*get_title) (AionPanelable*);
	const gchar* (*get_icon_name) (AionPanelable*);
	void (*set_task) (AionPanelable*, AionTask*);
	void (*refresh) (AionPanelable*);
	gboolean (*is_editable) (AionPanelable*);
	gboolean (*needs_save) (AionPanelable*);
	void (*start_editing) (AionPanelable*);
	void (*end_editing) (AionPanelable*, gboolean);
};

const gchar* aion_panelable_get_name(AionPanelable*);
const gchar* aion_panelable_get_title(AionPanelable*);
const gchar* aion_panelable_get_icon_name(AionPanelable*);
void aion_panelable_set_task(AionPanelable*, AionTask*);
void aion_panelable_refresh(AionPanelable*);
gboolean aion_panelable_is_editable(AionPanelable*);
gboolean aion_panelable_needs_save(AionPanelable*);
void aion_panelable_start_editing(AionPanelable*);
void aion_panelable_end_editing(AionPanelable*, gboolean);

G_END_DECLS

#endif /*__AION_PANELABLE__*/
