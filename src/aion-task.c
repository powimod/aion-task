/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "AionTask"
#include <glib/gi18n.h>

#include "aion-task.h"
#include "aion-task-private.h"
#include "aion-sub-task-private.h"
#include "util.h"

#define DEFAULT_LOG_ENTRIES_LIMIT 5

struct _AionTask {
	GObject parent;
};

typedef struct {
	gchar* label;
	gchar* note;
	gboolean enabled;
	gint interval;
	gint priority;
	gint default_duration;
	gint total_duration;
	AionTaskRepetitionType repetition_type;
	AionTaskPostponeFromWhen postpone_from_when;
	GDate* next_execution_date;
	gint preview_delay;
	gint postponement_delay;
	AionTaskState state;
	gint remaining_days;
	gint execution_count;
	gint log_entries_limit;
	GArray* log_entries;
	GList* sub_task_list;
} AionTaskPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(AionTask, aion_task, G_TYPE_OBJECT);

static gint instance_counter = 0;

// local prototypes
void _aion_task_evaluate_state(AionTask*);
AionTaskState _aion_task_evaluate_state_on_date_of(AionTask*, GDate*, gint*);
void _reindex_sub_task_list(AionTask* task);

static void
aion_task_init(AionTask* task)
{
	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	priv->label = NULL;
	priv->note = NULL;
	priv->enabled = TRUE;
	priv->postpone_from_when = AION_TASK_POSTPONE_FROM_EXECUTION_DATE;
	priv->repetition_type = AION_TASK_REPETITION_ONCE; 
	priv->next_execution_date = aion_util_get_today_date();
	priv->preview_delay = 0;
	priv->postponement_delay = 0;
	priv->interval = -1;
	priv->priority = AION_TASK_DEFAULT_PRIORITY;
	priv->default_duration = 0;
	priv->total_duration = 0;
	priv->execution_count = 0;
	priv->sub_task_list = NULL;

	priv->log_entries = g_array_sized_new (TRUE, //zero_terminated
                   TRUE, // clear
                   sizeof(AionTaskLogEntry),
                   DEFAULT_LOG_ENTRIES_LIMIT);
	priv->log_entries_limit = DEFAULT_LOG_ENTRIES_LIMIT;

	priv->state = AION_TASK_STATE_ON_TIME;
	priv->remaining_days = 0;
	instance_counter++;
}


static void
aion_task_dispose(GObject* object)
{
	//AionTask* task = AION_TASK(object);
	//AionTaskPrivate *priv = aion_task_get_instance_private(task);
	G_OBJECT_CLASS(aion_task_parent_class)->dispose(object);
}

static void
_free_sub_task(gpointer data)
{
	AionSubTask* sub_task = (AionSubTask*) data;
	g_object_unref(sub_task);
}


static void
aion_task_finalize(GObject* object)
{
	AionTask* task = AION_TASK(object);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if (priv->label != NULL)
		g_free(priv->label);
	g_date_free(priv->next_execution_date);
	g_array_free(priv->log_entries, TRUE);
	g_list_free_full(priv->sub_task_list, _free_sub_task);
	G_OBJECT_CLASS(aion_task_parent_class)->finalize(object);
	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_TASK));
}

 
static void
aion_task_class_init(AionTaskClass* task_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(task_class);
	object_class->dispose= aion_task_dispose;
	object_class->finalize = aion_task_finalize;
}


AionTask*
aion_task_new(const gchar* label)
{
	g_return_val_if_fail(label != NULL, NULL);
	AionTask* task = AION_TASK(g_object_new(AION_TYPE_TASK, NULL));
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	priv->label = g_strdup(label);
	return task;
}

const gchar*
aion_task_get_label(AionTask* aion_task)
{
	g_return_val_if_fail(AION_IS_TASK(aion_task), NULL);
	AionTaskPrivate *priv = aion_task_get_instance_private(aion_task);
	return priv->label;
}

gboolean
aion_task_set_label(AionTask* aion_task, const gchar* new_label)
{
	g_return_val_if_fail(AION_IS_TASK(aion_task), FALSE);
	g_return_val_if_fail(new_label != NULL, FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(aion_task);
	gboolean changed = FALSE;

	// label length can't be zero
	gchar* stripped_label = g_strdup(new_label);
	g_strstrip((gchar*)stripped_label);
	if (stripped_label[0] == '\0')
		goto done;

	// do nothing if new label is the same
	if (priv->label != NULL && g_strcmp0(priv->label, stripped_label) == 0) 
		goto done;

	if (priv->label)
		g_free(priv->label);
	priv->label = g_strdup(stripped_label);
	changed = TRUE;
done:
	g_free(stripped_label);
	return changed;
}


const gchar*
aion_task_get_note(AionTask* aion_task)
{
	g_return_val_if_fail(AION_IS_TASK(aion_task), NULL);
	AionTaskPrivate *priv = aion_task_get_instance_private(aion_task);
	return priv->note;
}

gboolean
aion_task_set_note(AionTask* aion_task, const gchar* new_note)
{
	g_return_val_if_fail(AION_IS_TASK(aion_task), FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(aion_task);
	gboolean changed = FALSE;

	gchar* note = NULL;
	if (new_note != NULL) {
		note = g_strdup(new_note);
		// FIXME is g_strstrip UTF-8 compliant ?
		g_strstrip(note);
	}
	if (note[0] == '\0') {
		g_free(note);
		note = NULL;
	}

	if (priv->note == NULL && note == NULL) 
		goto done;

	// do nothing if new note is the same
	if (priv->note != NULL && note != NULL &&  g_strcmp0(priv->note, note) == 0) 
		goto done;

	if (priv->note)
		g_free(priv->note);
	priv->note = (note) ? g_strdup(note): NULL;
	changed = TRUE;
done:
	if (note)
		g_free(note);
	return changed;
}


gboolean 
aion_task_is_enabled(AionTask* aion_task)
{
	g_return_val_if_fail(AION_IS_TASK(aion_task), FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(aion_task);
	return priv->enabled;
}

gboolean 
aion_task_set_enabled(AionTask* aion_task, gboolean enabled)
{
	g_return_val_if_fail(AION_IS_TASK(aion_task), FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(aion_task);
	if (priv->enabled == enabled)
		return FALSE;
	priv->enabled = enabled;
	_aion_task_evaluate_state(aion_task);
	return TRUE;
}



AionTaskRepetitionType
aion_task_get_repetition_type(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	return priv->repetition_type;
}

gboolean
aion_task_set_repetition_type(AionTask* task, AionTaskRepetitionType repetition_type)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if (priv->repetition_type == repetition_type)
		return FALSE; // no change
	priv->repetition_type = repetition_type;
	if ( priv->repetition_type == AION_TASK_REPETITION_ONCE) {
		priv->interval = -1;
	}
	else {
		if (priv->interval < 1) // 0 or -1
			priv->interval = 1;
	}
	return TRUE;
}


AionTaskPostponeFromWhen 
aion_task_get_postpone_from_when(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if ( priv->repetition_type == AION_TASK_REPETITION_ONCE) 
		g_warning("Getting Postpone does not make sense when repetition type is Once");
	return priv->postpone_from_when;
}

gboolean
aion_task_set_postpone_from_when(AionTask* task, AionTaskPostponeFromWhen from_when)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if (priv->postpone_from_when == from_when)
		return FALSE; // property is not changed
	priv->postpone_from_when = from_when;
	return TRUE;
}

// repetition type without translation (used to save in XML storage)
const gchar* repetition_type_str_array[] = {
	"once",
	"day",
	"week",
	"month",
	"year",
	NULL
};



// FIXME translation
const gchar* repetition_type_str_singular_array[] = {
	"once",
	"day",
	"week",
	"month",
	"year",
	NULL
};

// FIXME translation
const gchar* repetition_type_str_plural_array[] = {
	"once",
	"days",
	"weeks",
	"months",
	"years",
	NULL
};

const gchar** aion_task_repetition_type_array()
{
	return repetition_type_str_array;
}

const gchar*
aion_task_get_repetition_type_to_string(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), NULL);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if (priv->repetition_type == -1) {
		g_warning("Task interval not initialized");
		return "?";
	}
	return repetition_type_str_array[priv->repetition_type];
}


const gchar*
aion_task_repetition_type_string(AionTaskRepetitionType repetition_type)
{
	return repetition_type_str_array[repetition_type];
}


AionTaskRepetitionType
aion_task_repetition_type_from_string(const gchar* str)
{
	g_return_val_if_fail(str != NULL, FALSE);
	gint repetition_type = 0;
	while (repetition_type_str_array[repetition_type] != NULL){
		if (g_strcmp0(str, repetition_type_str_array[repetition_type]) == 0) 
			return repetition_type;
		repetition_type++;
	}
	g_warning("Invalid repetition type [%s]", str);
	return -1;
}

gboolean
aion_task_set_repetition_type_from_string(AionTask* task, const gchar* repetition_type_str)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(repetition_type_str != NULL, FALSE);
	gint repetition_type = aion_task_repetition_type_from_string(repetition_type_str);
	return aion_task_set_repetition_type(task, repetition_type);
}


gint
aion_task_get_interval(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if ( priv->repetition_type == AION_TASK_REPETITION_ONCE) 
		g_warning("Task interval is not defined when repetition type is Once");
	else if (priv->interval == -1)
		g_warning("Task interval not initialized");
	return priv->interval;
}

gboolean
aion_task_set_interval(AionTask* task, gint interval)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	if (interval < 1) {
		g_warning("Task interval should be greater than zero (not %i)", interval);
		return FALSE;
	}
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if (priv->interval == interval)
		return FALSE; // no change
	priv->interval = interval;
	return TRUE;
}


gint
aion_task_get_total_duration(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	return priv->total_duration;
}

void
aion_task_private_set_total_duration(AionTask* task, gint total_duration)
{
	g_return_if_fail(AION_IS_TASK(task));
	g_return_if_fail(AION_IS_TASK(task));
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	priv->total_duration = total_duration;
}

gint
aion_task_get_default_duration(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	return priv->default_duration;
}

gboolean
aion_task_set_default_duration(AionTask* task, gint default_duration)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(AION_IS_TASK(task), default_duration > 0);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if (priv->default_duration == default_duration)
		return FALSE; // no change
	priv->default_duration = default_duration;
	return TRUE;
}

gchar*
aion_task_get_human_representation_of_repetition(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), NULL);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if (priv->repetition_type == -1) {
		g_warning("Task interval not initialized");
		return g_strdup("?");
	}
	if ( priv->repetition_type == AION_TASK_REPETITION_ONCE) 
		return g_strdup(repetition_type_str_singular_array[AION_TASK_REPETITION_ONCE]);

	const gchar* unit_str = NULL;
	if (priv->interval == 1)
		unit_str = repetition_type_str_singular_array[priv->repetition_type];
	else
		unit_str = repetition_type_str_plural_array[priv->repetition_type];

	return g_strdup_printf("%i %s", priv->interval, unit_str);
}

const GDate*
aion_task_get_next_execution_date(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), NULL);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	g_assert(g_date_valid(priv->next_execution_date)); // TODO cleanup
	return priv->next_execution_date;
}


gboolean
aion_task_set_next_execution_date(AionTask* task, const GDate* next_execution_date)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(next_execution_date != NULL, FALSE);
	g_return_val_if_fail(g_date_valid(next_execution_date), FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if (g_date_compare(priv->next_execution_date, next_execution_date) == 0)
		return FALSE; // no change
	priv->next_execution_date = g_date_copy(next_execution_date);
	_aion_task_evaluate_state(task);
	return TRUE;
}


gint
aion_task_get_preview_delay(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	return priv->preview_delay;
}

gboolean
aion_task_set_preview_delay(AionTask* task, gint delay)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(delay >= 0, FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	priv->preview_delay = delay;
	_aion_task_evaluate_state(task);
	return TRUE;
}


gint
aion_task_get_postponement_delay(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	return priv->postponement_delay;
}

gboolean
aion_task_set_postponement_delay(AionTask* task, gint delay)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(delay >= 0, FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	priv->postponement_delay = delay;
	_aion_task_evaluate_state(task);
	return TRUE;
}


gint
aion_task_get_priority(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	return priv->priority;
}


gboolean
aion_task_set_priority(AionTask* task, gint priority)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(priority >= AION_TASK_MINIMUM_PRIORITY && priority <= AION_TASK_MAXIMUM_PRIORITY, FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	priv->priority = priority;
	return TRUE;
}


AionTaskState
_aion_task_evaluate_state_on_date_of(AionTask* task, GDate* date, gint* p_remaining_days)
{
	g_assert(AION_IS_TASK(task));
	g_assert(g_date_valid(date));
	AionTaskPrivate *priv = aion_task_get_instance_private(task);

	if (! priv->enabled) 
		return AION_TASK_STATE_DISABLED;

	gint remaining_days = g_date_days_between(date, priv->next_execution_date);
	if (p_remaining_days != NULL)
		*p_remaining_days = remaining_days;

	if (remaining_days < -(priv->postponement_delay))
		return AION_TASK_STATE_OVERDUE;

	if (remaining_days < 0)
		return AION_TASK_STATE_POSTPONED;

	if (remaining_days == 0)
		return AION_TASK_STATE_ON_TIME;

	if (remaining_days <= priv->preview_delay)
		return AION_TASK_STATE_PREVIEW;

	return AION_TASK_STATE_PENDING;
}

void
_aion_task_evaluate_state(AionTask* task)
{
	g_assert(AION_IS_TASK(task));
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	GDate* today_date = aion_util_get_today_date();
	priv->state =_aion_task_evaluate_state_on_date_of(task, today_date, &priv->remaining_days);
	g_date_free(today_date);
}

AionTaskState
aion_task_get_state(AionTask* aion_task)
{
	g_return_val_if_fail(AION_IS_TASK(aion_task), -1);
	AionTaskPrivate *priv = aion_task_get_instance_private(aion_task);
	return priv->state;
}


const gchar*
aion_task_get_state_str(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), NULL);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	const gchar* state_str = NULL;
	switch (priv->state) {
		case AION_TASK_STATE_OVERDUE :
			state_str = _("overdue");
			break;
		case AION_TASK_STATE_PENDING :
			state_str = _("pending");
			break;
		case AION_TASK_STATE_ON_TIME :
			state_str = _("on time");
			break;
		case AION_TASK_STATE_POSTPONED :
			state_str = _("postponed");
			break;
		case AION_TASK_STATE_PREVIEW :
			state_str = _("preview");
			break;
		case AION_TASK_STATE_DISABLED :
			state_str = _("disabled");
			break;
		default:
			g_assert_not_reached();
	}
	return state_str;
}
 
AionTaskState
aion_task_evaluate_state_on_date_of(AionTask* task, GDate* limit_date)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	g_return_val_if_fail(g_date_valid(limit_date), -1);
	AionTaskState state = _aion_task_evaluate_state_on_date_of(task, limit_date, NULL);
	return state;
}

gint
aion_task_get_remaining_days(AionTask* aion_task)
{
	g_return_val_if_fail(AION_IS_TASK(aion_task), 0);
	AionTaskPrivate *priv = aion_task_get_instance_private(aion_task);
	return priv->remaining_days;
}


/**
 * _aion_task_shift_date
 *
 * Cette fonction décale la date en argument soit vers l'avenir (si l'argument delta vaut 1) soit vers le passé (s'il vaut -1).
 * Si la tâche a un type de répétition «une seule fois», la date passée en argument est fixée à aujourd'hui.
 * La fonction est réellement utile dans le cas où la répétition est fixée à la date d'échéance.
 * Dans ce cas, la date passée en argument est décalée dans le futur ou dans le passé (en fonction de l'argument delta)
 * autant de fois qu'il est nécessaire.
 * Si l'argument delta est négatif, la date est décalée en arrière autant de fois que nécessaire pour arriver dans le passé.
 * Si l'argument delta est positif, la date est décalée en avant   autant de fois que nécessaire pour arriver dans l'avenir.
 *
 */
static gboolean
_aion_task_shift_date(AionTask* task, GDate* date_to_shift, gint delta)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(g_date_valid(date_to_shift), FALSE);
	g_return_val_if_fail(delta == 1 || delta == -1, FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	GDate* today_date = aion_util_get_today_date();

	g_debug("Shifting date %02i/%02i/%04i in the %s ...", 
			g_date_get_day(date_to_shift),
			g_date_get_month(date_to_shift),
			g_date_get_year(date_to_shift),
			(delta > 0) ? "future" : "paste");

	if (priv->repetition_type == AION_TASK_REPETITION_ONCE) {
		g_debug("Shift to today because task must be executed once");
		*date_to_shift = *today_date;
		goto done;
	}
	if (priv->postpone_from_when == AION_TASK_POSTPONE_FROM_EXECUTION_DATE) {
		g_debug("Shift to today because task must be shifted to the execution date");
		*date_to_shift = *today_date;
	}

	while (TRUE) {
		if (delta < 0 && g_date_compare(today_date, date_to_shift) >= 0) // shift to the paste
			break;
		if (delta > 0 && g_date_compare(today_date, date_to_shift) < 0) // shift to the future
			break;
		switch (priv->repetition_type) {
			case AION_TASK_REPETITION_ONCE: 
				g_assert_not_reached(); // case already treated above
				break;
			case AION_TASK_REPETITION_EVERY_DAY:
				g_debug("%s %i day(s)", (delta > 0) ? "Add" : "Remove", priv->interval);
				if (delta < 0)
					g_date_subtract_days(date_to_shift, priv->interval);
				else
					g_date_add_days(date_to_shift, priv->interval);
				break;
			case AION_TASK_REPETITION_EVERY_WEEK:
				g_debug("%s %i week(s)", (delta > 0) ? "Add" : "Remove", priv->interval);
				if (delta < 0)
					g_date_subtract_days(date_to_shift, priv->interval * 7);
				else
					g_date_add_days(date_to_shift, priv->interval * 7);
				break;
			case AION_TASK_REPETITION_EVERY_MONTH:
				g_debug("%s %i month(s)", (delta > 0) ? "Add" : "Remove", priv->interval);
				if (delta < 0)
					g_date_subtract_months(date_to_shift, priv->interval);
				else
					g_date_add_months(date_to_shift, priv->interval);
				break;
			case AION_TASK_REPETITION_EVERY_YEAR:
				g_debug("%s %i year(s)", (delta > 0) ? "Add" : "Remove", priv->interval);
				if (delta < 0)
					g_date_subtract_years(date_to_shift, priv->interval);
				else
					g_date_add_years(date_to_shift, priv->interval);
				break;
			default:
				g_error("Invalid repetition type");
		}
		g_debug("date : %02i/%02i/%04i", 
				g_date_get_day(date_to_shift),
				g_date_get_month(date_to_shift),
				g_date_get_year(date_to_shift));
	}

done:
	g_date_free(today_date);
	g_debug("Shifted date : %02i/%02i/%04i", 
			g_date_get_day(date_to_shift),
			g_date_get_month(date_to_shift),
			g_date_get_year(date_to_shift));
	return TRUE;
}


gboolean
aion_task_validate_with_duration(AionTask* aion_task, gint effective_duration)
{
	g_return_val_if_fail(AION_IS_TASK(aion_task), FALSE);
	g_return_val_if_fail(effective_duration >= 0, FALSE);

	AionTaskPrivate *priv = aion_task_get_instance_private(aion_task);
	if (priv->state == AION_TASK_STATE_DISABLED) {
		g_warning("Can't validate a task when it's state is disabled");
		return FALSE;
	}

	GDate* postponement_date = priv->next_execution_date;
	_aion_task_shift_date(aion_task, postponement_date, -1); // come back to the paste
	_aion_task_shift_date(aion_task, postponement_date, 1);  // go into the future
	g_assert(g_date_valid(postponement_date));
	
	g_debug("Next execution date : %02i/%02i/%04i", 
			g_date_get_day(priv->next_execution_date),
			g_date_get_month(priv->next_execution_date),
			g_date_get_year(priv->next_execution_date));

	priv->execution_count++;
	priv->total_duration += effective_duration;
	priv->postponement_delay = 0;
	GDate* today_date = aion_util_get_today_date();
	aion_task_private_add_log_entry(aion_task, today_date, effective_duration);
	g_date_free(today_date);

	_aion_task_evaluate_state(aion_task);

	// reinitialize sub-tasks status
	GList* sub_task_list_item = priv->sub_task_list;
	while (sub_task_list_item) {
		AionSubTask* sub_task = AION_SUB_TASK(sub_task_list_item->data);
		aion_sub_task_mark_as_done(sub_task, FALSE);
		sub_task_list_item = sub_task_list_item->next;
	}

	return TRUE;
}

gboolean
aion_task_validate(AionTask* aion_task)
{
	g_return_val_if_fail(AION_IS_TASK(aion_task), FALSE);
	return aion_task_validate_with_duration(aion_task, 0);
}



gboolean
aion_task_postpone(AionTask* task, gint days)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(days > 0, FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if (priv->state == AION_TASK_STATE_DISABLED) {
		g_warning("Can't postpone a task when it's state is disabled");
		return FALSE;
	}
	GDate* postponement_date = aion_util_get_today_date();
	g_date_add_days(postponement_date, days); 

	gint postponement_delay = g_date_days_between(priv->next_execution_date, postponement_date);
	g_date_free(postponement_date);
	if (postponement_delay < 0) {
		g_warning("Invalid postponement delay");
		return FALSE;
	}
	priv->postponement_delay = postponement_delay;
	_aion_task_evaluate_state(task);
	return TRUE;
}


void
aion_task_private_resize_log_entries(AionTask* task, gint new_size)
{
	g_return_if_fail(AION_IS_TASK(task));
	g_return_if_fail(new_size >= 0);
	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	if (new_size < priv->log_entries->len)
		g_warning("New size of log entries is smaller than the actual size");
	priv->log_entries = g_array_set_size(priv->log_entries, new_size);
}

AionTaskLogEntry*
aion_task_private_get_log_entry(AionTask* task, gint index)
{
	g_return_val_if_fail(AION_IS_TASK(task), NULL);
	g_return_val_if_fail(index >= 0, NULL);
	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	if (index > priv->log_entries->len - 1)
		g_error("Access outside of boundaries in log entries array");
	return &g_array_index(priv->log_entries, AionTaskLogEntry, index);
}

void
aion_task_private_add_log_entry(AionTask* task, const GDate* new_date, gint duration)
{
	g_return_if_fail(AION_IS_TASK(task));
	g_return_if_fail(duration >= 0);

	GDate* date = NULL;
	if (new_date == NULL)
		date = aion_util_get_today_date(); 
	else
		date = g_date_copy(new_date);

	AionTaskLogEntry* log_entry = (AionTaskLogEntry*) g_malloc0(sizeof(AionTaskLogEntry));
	log_entry->year  = g_date_get_year(date);
	log_entry->month = g_date_get_month(date);
	log_entry->day   = g_date_get_day(date);
	log_entry->duration = duration;
	g_date_free(date);

	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	// TODO insert it in the right position to keep array sorted by date
	priv->log_entries =  g_array_append_val(priv->log_entries, *log_entry);
	g_free(log_entry);

	if (priv->log_entries->len > priv->log_entries_limit)
		priv->log_entries = g_array_remove_index(priv->log_entries, 0);
}

gint
aion_task_get_execution_count(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	return priv->execution_count;
}

void aion_task_private_set_execution_count(AionTask* task, gint count)
{
	g_return_if_fail(AION_IS_TASK(task));
	g_return_if_fail(count >= 0);
	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	priv->execution_count = count;
}


gint
aion_task_get_log_entries_count(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	return priv->log_entries->len;
}

gint
aion_task_get_log_entries_limit(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	return priv->log_entries_limit;
}

GDate*
aion_task_get_log_entry_date(AionTask* task, gint log_entry_index)
{
	g_return_val_if_fail(AION_IS_TASK(task), NULL);
	g_return_val_if_fail(log_entry_index >= 0, NULL);
	AionTaskLogEntry* log_entry = aion_task_private_get_log_entry(task, log_entry_index);
	return g_date_new_dmy(
		log_entry->day,
		log_entry->month,
		log_entry->year
	);
}

void 
aion_task_get_log_entry_date_dmy(AionTask* task, gint log_entry_index, gint* day, gint* month, gint* year)
{
	g_return_if_fail(AION_IS_TASK(task));
	g_return_if_fail(log_entry_index >= 0);
	AionTaskLogEntry* log_entry = aion_task_private_get_log_entry(task, log_entry_index);
	*day   = log_entry->day;
	*month = log_entry->month;
	*year  = log_entry->year;
}

gint
aion_task_get_log_entry_duration(AionTask* task, gint log_entry_index)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	g_return_val_if_fail(log_entry_index >= 0, -1);
	AionTaskLogEntry* log_entry = aion_task_private_get_log_entry(task, log_entry_index);
	return log_entry->duration;
}

void
aion_task_private_set_log_entry_duration(AionTask* task, gint log_entry_index, gint duration)
{
	g_return_if_fail(AION_IS_TASK(task));
	g_return_if_fail(log_entry_index >= 0);
	AionTaskLogEntry* log_entry = aion_task_private_get_log_entry(task, log_entry_index);
	log_entry->duration = duration;
}

gboolean
aion_task_set_log_entries_limit(AionTask* task, gint new_limit)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(new_limit > 0, FALSE);
	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	if (priv->log_entries_limit == new_limit)
		return FALSE;
	priv->log_entries_limit = new_limit;
	while (priv->log_entries->len > new_limit)
		priv->log_entries = g_array_remove_index(priv->log_entries, 0);
	return TRUE;
}


AionSubTask*
aion_task_create_sub_task(AionTask* task, const gchar* label)
{
	g_return_val_if_fail(AION_IS_TASK(task), NULL);
	g_return_val_if_fail(label != NULL, NULL);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	AionSubTask* sub_task = aion_sub_task_new(label);
	priv->sub_task_list = g_list_append(priv->sub_task_list, sub_task);
	aion_sub_task_private_set_position(sub_task, g_list_length(priv->sub_task_list) - 1);
	return sub_task;
}


void
aion_task_private_append_sub_task(AionTask* task, AionSubTask* sub_task, gint index)
{
	g_return_if_fail(AION_IS_TASK(task));
	g_return_if_fail(AION_IS_SUB_TASK(sub_task));
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	if (index == -1)
		priv->sub_task_list = g_list_append(priv->sub_task_list, sub_task);
	else
		priv->sub_task_list = g_list_insert(priv->sub_task_list, sub_task, index);
	_reindex_sub_task_list(task);
	g_object_ref(sub_task);
}

gboolean
aion_task_delete_sub_task(AionTask* task, AionSubTask* sub_task)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(AION_IS_SUB_TASK(sub_task), FALSE);
	AionTaskPrivate *priv = aion_task_get_instance_private(task);
	GList* sub_task_item = g_list_find(priv->sub_task_list, sub_task);
	if (! sub_task_item)
		return FALSE;
	priv->sub_task_list = g_list_delete_link(priv->sub_task_list, sub_task_item);
	g_object_unref(sub_task);
	return TRUE;
}

gint
aion_task_sub_task_count(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), -1);
	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	return g_list_length(priv->sub_task_list);
}


void
aion_task_for_each_sub_task(AionTask* task, AionSubTaskCallback callback, gpointer data)
{
	g_return_if_fail(AION_IS_TASK(task));
	g_return_if_fail(callback != NULL);
	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	g_list_foreach(priv->sub_task_list, (GFunc)callback, data);
}


void
_aion_sub_task_list_append(AionSubTask* sub_task, gpointer data)
{
	GList **sub_task_list_pointer = (GList**) data;
	*sub_task_list_pointer = g_list_prepend (*sub_task_list_pointer, sub_task);
}

GList*
aion_task_get_sub_task_list(AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK(task), NULL);
	GList* sub_task_list = NULL;
	aion_task_for_each_sub_task(task, _aion_sub_task_list_append, &sub_task_list);
	return g_list_reverse (sub_task_list);
}

gboolean
aion_task_swap_sub_tasks(AionTask* task, AionSubTask* sub_task_1, AionSubTask* sub_task_2)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(AION_IS_SUB_TASK(sub_task_1), FALSE);
	g_return_val_if_fail(AION_IS_SUB_TASK(sub_task_2), FALSE);
	g_return_val_if_fail(sub_task_1 == sub_task_2, FALSE);

	AionTaskPrivate* priv = aion_task_get_instance_private(task);

	GList* sub_task_1_item = g_list_find (priv->sub_task_list, sub_task_1);
	g_return_val_if_fail(! sub_task_1_item, FALSE);
	
	GList* sub_task_2_item = g_list_find (priv->sub_task_list, sub_task_2);
	g_return_val_if_fail(! sub_task_1_item, FALSE);

	g_error("changing position not yet implemented in sub-task swaping");

	sub_task_1_item->data = sub_task_2;
	sub_task_2_item->data = sub_task_1;

	return TRUE;
}

gboolean
aion_task_set_sub_task_position(AionTask* task, AionSubTask* sub_task, gint wanted_position)
{
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	g_return_val_if_fail(AION_IS_SUB_TASK(sub_task), FALSE);
	g_return_val_if_fail(wanted_position >= 0, FALSE);
	AionTaskPrivate* priv = aion_task_get_instance_private(task);

	// TODO cleanup : g_print("\nmoving in position %i sub-task [%s] ...\n", wanted_position, aion_sub_task_get_label(sub_task));

	gint sub_task_count = g_list_length(priv->sub_task_list);
	g_return_val_if_fail(wanted_position < sub_task_count, FALSE); // index out of range

	gint actual_position = g_list_index (priv->sub_task_list, sub_task);
	g_return_val_if_fail(actual_position != -1, FALSE);
	if (actual_position == wanted_position) 
		return FALSE; // already positioned
	GList* sub_task_item = g_list_nth (priv->sub_task_list, actual_position);
	g_assert(sub_task_item != NULL);

/* TODO cleanup
	g_print("Subtask list before change : \n");
	GList* list_item_bis = priv->sub_task_list;
	while (list_item_bis) {
		AionSubTask* sub_task_bis = AION_SUB_TASK(list_item_bis->data);
		g_print("\t- %i : %s\n", aion_sub_task_get_position(sub_task_bis), aion_sub_task_get_label(sub_task_bis));
		list_item_bis = list_item_bis->next;
	}
*/

	priv->sub_task_list = g_list_remove_link(priv->sub_task_list, sub_task_item);
	if (wanted_position > actual_position)
		wanted_position--;

	GList* list_item = g_list_nth (priv->sub_task_list, wanted_position);
	priv->sub_task_list = g_list_insert_before_link (priv->sub_task_list, list_item, sub_task_item);

/* TODO cleanup
	g_print("SOm subtask list after change : \n");
	list_item_bis = priv->sub_task_list;
	while (list_item_bis) {
		AionSubTask* sub_task_bis = AION_SUB_TASK(list_item_bis->data);
		g_print("\t- %i : %s\n", aion_sub_task_get_position(sub_task_bis), aion_sub_task_get_label(sub_task_bis));
		list_item_bis = list_item_bis->next;
	}
*/

	return TRUE;
}

void
_reindex_sub_task_list(AionTask* task)
{
	g_return_if_fail(AION_IS_TASK(task));
	AionTaskPrivate* priv = aion_task_get_instance_private(task);
	gint index = 0; //wanted_position; TODO optimization
	GList* list_item = priv->sub_task_list; //sub_task_item;
	while (list_item) {
		AionSubTask* sub_task = AION_SUB_TASK(list_item->data);
		aion_sub_task_private_set_position(sub_task, index++);
		list_item = list_item->next;
	}
}


gint
aion_task_get_instance_count()
{
	return instance_counter;
}

gint
aion_task_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_TASK));
	return instance_counter;;
}
