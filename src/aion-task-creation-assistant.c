/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-TaskCreationAssistant"
#include <glib/gi18n.h>

#include "aion-task-creation-assistant.h"
#include "util.h"

struct _AionTaskCreationAssistant {
	GtkAssistant parent;
};

typedef struct {
	AionTask* task;
	AionMainWindow* main_window;

	GtkWidget* task_label_field;
	GtkWidget* task_next_execution_date_field;
	GtkWidget* preview_delay_enable_option;
	GtkWidget* preview_delay_frame;
	GtkWidget* preview_delay_field;
	GtkWidget* single_repetition_option;
	GtkWidget* task_frequency_field;
	GtkWidget* task_interval_field;
	GtkWidget* resume_message;

	gint task_label_page_num;
	gint task_next_execution_page_num;
	gint task_preview_delay_page_num;
	gint task_repetition_page_num;
	gint task_frequency_page_num;
	gint confirmation_page_num;
} AionTaskCreationAssistantPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(AionTaskCreationAssistant, aion_task_creation_assistant, GTK_TYPE_ASSISTANT);

static gint instance_counter = 0;

enum {
	SIGNAL_TASK_CREATION_ASSISTANT_CLOSE,
	LAST_SIGNAL
};
static guint aion_task_creation_assistant_signals[LAST_SIGNAL] = { 0 };

// local prototypes
static gint _assistant_define_next_page(gint, gpointer);
static void _assistant_close(GtkAssistant*, gpointer);
static void _assistant_apply(GtkAssistant*, gpointer);
static void _assistant_cancel(GtkAssistant*, gpointer);
static void _assistant_page_prepare(GtkDialog*, GtkWidget*, gpointer);
static void _label_field_changed(GtkEntry*, GParamSpec*, gpointer);
static void _repetition_type_changed(GtkListBox*, GtkListBoxRow*, gpointer);
static void _select_today_button_clicked(GtkButton*, gpointer);
static void _calendar_select_today(GtkCalendar*);
static void _calendar_day_selected(GtkCalendar*, gpointer);
static void _preview_delay_enable_option_toggled(GtkToggleButton*, gpointer);
static void _aion_task_create_task(AionTaskCreationAssistant*);

static void
aion_task_creation_assistant_init(AionTaskCreationAssistant* task_creation_assistant)
{
	g_return_if_fail(AION_IS_TASK_CREATION_ASSISTANT(task_creation_assistant));
	AionTaskCreationAssistantPrivate* priv = aion_task_creation_assistant_get_instance_private(task_creation_assistant);
	priv->task = NULL;
	priv->main_window = NULL;

	// TODO : gtk_window_set_title(GTK_WINDOW(task_creation_assistant), "Task creation");
	gtk_window_set_modal(GTK_WINDOW(task_creation_assistant), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(task_creation_assistant), TRUE);


	GtkBuilder* builder = gtk_builder_new_from_resource("/org/gnome/aion-task/ui/task-creation-assistant");
	g_assert(builder != NULL);

	// Task label page

	GtkWidget* page = GTK_WIDGET(gtk_builder_get_object(builder, "task-label-page"));
	g_assert(page != NULL);
	priv->task_label_page_num = gtk_assistant_append_page(GTK_ASSISTANT(task_creation_assistant), page);
	// TODO cleanup : gtk_assistant_set_page_title(GTK_ASSISTANT(task_creation_assistant), page, _("Task label"));

	priv->task_label_field = GTK_WIDGET(gtk_builder_get_object(builder, "task-label-field"));
	g_assert(priv->task_label_field != NULL);
	g_signal_connect(priv->task_label_field, "notify::text", G_CALLBACK(_label_field_changed), task_creation_assistant);


	// Task next execution date page

	page = GTK_WIDGET(gtk_builder_get_object(builder, "task-next-execution-page"));
	g_assert(page != NULL);
	priv->task_next_execution_page_num = gtk_assistant_append_page(GTK_ASSISTANT(task_creation_assistant), page);
	// TODO cleanup : gtk_assistant_set_page_title(GTK_ASSISTANT(task_creation_assistant), page, _("Next execution date"));

	priv->task_next_execution_date_field = GTK_WIDGET(gtk_builder_get_object(builder, "next-execution-field"));
	g_assert(priv->task_next_execution_date_field != NULL);
	_calendar_select_today(GTK_CALENDAR(priv->task_next_execution_date_field));
	g_signal_connect(priv->task_next_execution_date_field, "day-selected", G_CALLBACK(_calendar_day_selected), task_creation_assistant);

	GtkWidget* button = GTK_WIDGET(gtk_builder_get_object(builder, "select-today-button"));
	g_assert(button != NULL);
	g_signal_connect(button, "clicked", G_CALLBACK(_select_today_button_clicked), priv->task_next_execution_date_field );

	gtk_assistant_set_page_complete (GTK_ASSISTANT(task_creation_assistant), page, TRUE);


	// Task preview delay

	page = GTK_WIDGET(gtk_builder_get_object(builder, "task-preview-delay-page"));
	g_assert(page != NULL);
	priv->task_preview_delay_page_num = gtk_assistant_append_page(GTK_ASSISTANT(task_creation_assistant), page);

	priv->preview_delay_enable_option = GTK_WIDGET(gtk_builder_get_object(builder, "preview-delay-enable-option"));
	g_assert(priv->preview_delay_enable_option);
	g_signal_connect(priv->preview_delay_enable_option, "toggled", G_CALLBACK(_preview_delay_enable_option_toggled), task_creation_assistant);

	priv->preview_delay_frame = GTK_WIDGET(gtk_builder_get_object(builder, "preview-delay-frame"));
	g_assert(priv->preview_delay_enable_option);
	gtk_widget_set_visible(priv->preview_delay_frame, FALSE);

	priv->preview_delay_field = GTK_WIDGET(gtk_builder_get_object(builder, "preview-delay-field"));
	g_assert(priv->preview_delay_field);


	gtk_assistant_set_page_complete (GTK_ASSISTANT(task_creation_assistant), page, TRUE);


	// Task next execution date page

	page = GTK_WIDGET(gtk_builder_get_object(builder, "task-repetition-page"));
	g_assert(page != NULL);
	priv->task_repetition_page_num = gtk_assistant_append_page(GTK_ASSISTANT(task_creation_assistant), page);
	// TODO cleanup : gtk_assistant_set_page_title(GTK_ASSISTANT(task_creation_assistant), page, _("Task repetition"));

	priv->single_repetition_option = GTK_WIDGET(gtk_builder_get_object(builder, "single-repetition-option"));
	g_assert(priv->task_next_execution_date_field != NULL);

	gtk_assistant_set_page_complete (GTK_ASSISTANT(task_creation_assistant), page, TRUE);


	// Task frequency page

	page = GTK_WIDGET(gtk_builder_get_object(builder, "task-frequency-page"));
	g_assert(page != NULL);
	priv->task_frequency_page_num = gtk_assistant_append_page(GTK_ASSISTANT(task_creation_assistant), page);
	// TODO cleanup : gtk_assistant_set_page_title(GTK_ASSISTANT(task_creation_assistant), page, _("Task frequency"));

	priv->task_frequency_field = GTK_WIDGET(gtk_builder_get_object(builder, "task-frequency-field"));
	g_assert(priv->task_frequency_field != NULL);
	const gchar** str_array = aion_task_repetition_type_array();
	// start for loop at position 1 to bypass Once entry
	for (int i = 1; str_array[i] != NULL; i++) {
		GtkWidget* row = gtk_list_box_row_new();
		gtk_widget_show(row);
		g_object_set_data(G_OBJECT(row), "repetition-type",  GUINT_TO_POINTER(i)); 
		gtk_container_add(GTK_CONTAINER(priv->task_frequency_field), row);

		GtkWidget* label = gtk_label_new(str_array[i]);
		gtk_widget_show(label);
		gtk_label_set_xalign(GTK_LABEL(label), 0);
		gtk_container_add(GTK_CONTAINER(row), label);
	}
	g_signal_connect(priv->task_frequency_field, "row-selected", G_CALLBACK(_repetition_type_changed), task_creation_assistant);

	priv->task_interval_field = GTK_WIDGET(gtk_builder_get_object(builder, "task-interval-field"));
	g_assert(priv->task_interval_field != NULL);


	// Confirmation page

	page = GTK_WIDGET(gtk_builder_get_object(builder, "confirmation-page"));
	g_assert(page != NULL);
	priv->confirmation_page_num = gtk_assistant_append_page(GTK_ASSISTANT(task_creation_assistant), page);
	// TODO cleanup : gtk_assistant_set_page_title(GTK_ASSISTANT(task_creation_assistant), page, _("Confirmation"));
	gtk_assistant_set_page_type(GTK_ASSISTANT(task_creation_assistant), page, GTK_ASSISTANT_PAGE_CONFIRM);
	gtk_assistant_set_page_complete (GTK_ASSISTANT(task_creation_assistant), page, TRUE);

	priv->resume_message = GTK_WIDGET(gtk_builder_get_object(builder, "resume-message"));
	g_assert(priv->resume_message);
	
	g_object_unref(builder);

	gtk_assistant_set_forward_page_func(GTK_ASSISTANT(task_creation_assistant), _assistant_define_next_page, task_creation_assistant, NULL);
	g_signal_connect(task_creation_assistant, "close", G_CALLBACK(_assistant_close), task_creation_assistant);
	g_signal_connect(task_creation_assistant, "apply", G_CALLBACK(_assistant_apply), task_creation_assistant);
	g_signal_connect(task_creation_assistant, "cancel", G_CALLBACK(_assistant_cancel), task_creation_assistant);
	g_signal_connect(task_creation_assistant, "prepare", G_CALLBACK(_assistant_page_prepare), task_creation_assistant);
	gtk_widget_show(GTK_WIDGET(task_creation_assistant));

	instance_counter++;
}

static void
aion_task_creation_assistant_dispose(GObject* object)
{
	AionTaskCreationAssistant* task_creation_assistant = AION_TASK_CREATION_ASSISTANT(object);
	AionTaskCreationAssistantPrivate *priv = aion_task_creation_assistant_get_instance_private(task_creation_assistant);
	if (priv->task) {
		g_object_unref(priv->task);
		priv->task = NULL;
	}
	if (priv->main_window) {
		g_object_unref(priv->main_window);
		priv->main_window= NULL;
	}
	G_OBJECT_CLASS(aion_task_creation_assistant_parent_class)->dispose(object);
}


static void
aion_task_creation_assistant_finalize(GObject* object)
{
	//AionTaskCreationAssistant* task_creation_assistant = AION_TASK_CREATION_ASSISTANT(object);
	//AionTaskCreationAssistantPrivate *priv = aion_task_creation_assistant_get_instance_private(task_creation_assistant);
	G_OBJECT_CLASS(aion_task_creation_assistant_parent_class)->finalize(object);
	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_TASK_CREATION_ASSISTANT));
}

 
static void
aion_task_creation_assistant_class_init(AionTaskCreationAssistantClass* task_creation_assistant_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(task_creation_assistant_class);
	object_class->dispose= aion_task_creation_assistant_dispose;
	object_class->finalize = aion_task_creation_assistant_finalize;

	aion_task_creation_assistant_signals[SIGNAL_TASK_CREATION_ASSISTANT_CLOSE] = 
		g_signal_new ("closed",
			G_TYPE_FROM_CLASS(task_creation_assistant_class),
			G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
			0,                   // class offset
			NULL,                // accumulator 
			NULL,                // accumulator data
			NULL,                // C marshaller
			G_TYPE_NONE,         // return_type
			1,                   // n_params 
			AION_TYPE_TASK);     // first parameter (new task or NULL if canceled)
}


AionTaskCreationAssistant*
aion_task_creation_assistant_new(AionMainWindow* main_window)
{
	g_return_val_if_fail(AION_IS_MAIN_WINDOW(main_window), NULL);
	AionTaskCreationAssistant* task_creation_assistant = 
		AION_TASK_CREATION_ASSISTANT(g_object_new(AION_TYPE_TASK_CREATION_ASSISTANT, 
			"use-header-bar", TRUE, // FIXME use-header-bar ?
			NULL));
	AionTaskCreationAssistantPrivate* priv = aion_task_creation_assistant_get_instance_private(task_creation_assistant);
	priv->main_window = main_window; g_object_ref(priv->main_window);
	gtk_window_set_transient_for(GTK_WINDOW(task_creation_assistant), GTK_WINDOW(main_window));
	return task_creation_assistant;
}

static gint
_assistant_define_next_page (gint current_page_num, gpointer data)
{
	AionTaskCreationAssistant* task_creation_assistant = AION_TASK_CREATION_ASSISTANT(data);
	AionTaskCreationAssistantPrivate* priv = aion_task_creation_assistant_get_instance_private(task_creation_assistant);

	if (current_page_num == priv->task_label_page_num)
		return priv->task_next_execution_page_num;

	if (current_page_num == priv->task_next_execution_page_num)
		return priv->task_preview_delay_page_num;

	if (current_page_num == priv->task_preview_delay_page_num)
		return priv->task_repetition_page_num;

	if (current_page_num == priv->task_repetition_page_num) {
		gboolean single_execution = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->single_repetition_option));
		if (single_execution)
			return priv->confirmation_page_num; // skip frequency page
		else
			return priv->task_frequency_page_num;
	}

	if (current_page_num == priv->task_frequency_page_num)
		return priv->confirmation_page_num;

	if (current_page_num == priv->confirmation_page_num)
		return -1;

	g_assert_not_reached();
	return -1;
}

static void
_assistant_apply(GtkAssistant* assistant, gpointer data)
{
	AionTaskCreationAssistant* task_creation_assistant = AION_TASK_CREATION_ASSISTANT(data);
	AionTaskCreationAssistantPrivate* priv = aion_task_creation_assistant_get_instance_private(task_creation_assistant);
	g_assert(priv->task != NULL); // task was previously created by the confirmation page
	g_signal_emit(task_creation_assistant, aion_task_creation_assistant_signals[SIGNAL_TASK_CREATION_ASSISTANT_CLOSE], 0, priv->task);
}

static void
_assistant_cancel(GtkAssistant* assistant, gpointer data)
{
	AionTaskCreationAssistant* task_creation_assistant = AION_TASK_CREATION_ASSISTANT(data);
	g_signal_emit(task_creation_assistant, aion_task_creation_assistant_signals[SIGNAL_TASK_CREATION_ASSISTANT_CLOSE], 0, NULL);
	gtk_widget_destroy(GTK_WIDGET(assistant));
}

static void
_assistant_close(GtkAssistant* assistant, gpointer data)
{
	gtk_widget_destroy(GTK_WIDGET(assistant));
}


static void
_assistant_page_prepare(GtkDialog* dialog, GtkWidget* page, gpointer data)
{
	AionTaskCreationAssistant* task_creation_assistant = AION_TASK_CREATION_ASSISTANT(data);
	AionTaskCreationAssistantPrivate* priv = aion_task_creation_assistant_get_instance_private(task_creation_assistant);


	if (page == gtk_assistant_get_nth_page(GTK_ASSISTANT(task_creation_assistant), priv->task_label_page_num)) {
		aion_util_grab_focus(priv->task_label_field);
		return;
	}
	if (page == gtk_assistant_get_nth_page(GTK_ASSISTANT(task_creation_assistant), priv->task_next_execution_page_num )) {
		aion_util_grab_focus(priv->task_next_execution_date_field);
		return;
	}
	if (page == gtk_assistant_get_nth_page(GTK_ASSISTANT(task_creation_assistant), priv->task_frequency_page_num)) {
		aion_util_grab_focus(priv->task_frequency_field);
		return;
	}
	if (page == gtk_assistant_get_nth_page(GTK_ASSISTANT(task_creation_assistant), priv->confirmation_page_num)) {
		_aion_task_create_task(task_creation_assistant);

		gchar* str = aion_task_get_human_representation_of_repetition(priv->task);
		gchar* message = g_strdup_printf(_("Ready to create the new task «<b>%s</b>»\n(repetition %s)"),
			gtk_entry_get_text(GTK_ENTRY(priv->task_label_field)),
			str);
		g_free(str);
		gtk_label_set_markup(GTK_LABEL(priv->resume_message), message);
		g_free(message);
		return;
	}
}


static void
_label_field_changed (GtkEntry* label_field_entry, GParamSpec* pspec, gpointer data)
{
	AionTaskCreationAssistant* task_creation_assistant = AION_TASK_CREATION_ASSISTANT(data);
	AionTaskCreationAssistantPrivate* priv = aion_task_creation_assistant_get_instance_private(task_creation_assistant);
	GtkWidget *label_page = gtk_assistant_get_nth_page(GTK_ASSISTANT(task_creation_assistant), priv->task_label_page_num); 
	gboolean complete = gtk_entry_get_text_length(label_field_entry) > 0 ? TRUE : FALSE;
	gtk_assistant_set_page_complete (GTK_ASSISTANT(task_creation_assistant), label_page, complete); 
}

static void
_repetition_type_changed (GtkListBox* task_list_box, GtkListBoxRow* selected_row, gpointer data)
{
	AionTaskCreationAssistant* task_creation_assistant = AION_TASK_CREATION_ASSISTANT(data);
	AionTaskCreationAssistantPrivate* priv = aion_task_creation_assistant_get_instance_private(task_creation_assistant);
	GtkWidget *frequency_page = gtk_assistant_get_nth_page(GTK_ASSISTANT(task_creation_assistant), priv->task_frequency_page_num); 
	gboolean complete = TRUE; 
	gtk_assistant_set_page_complete (GTK_ASSISTANT(task_creation_assistant), frequency_page, complete); 
}

static void
_calendar_select_today(GtkCalendar* calendar)
{
	GDate* date = aion_util_get_today_date();
	guint year  = g_date_get_year(date);
	guint month = g_date_get_month(date);
	guint day   = g_date_get_day(date);
	g_date_free(date);
	gtk_calendar_select_month(GTK_CALENDAR(calendar), month-1, year);
	gtk_calendar_select_day(GTK_CALENDAR(calendar), day);
}

static void
_select_today_button_clicked(GtkButton* button, gpointer data)
{
	GtkCalendar* calendar = GTK_CALENDAR(data);
	_calendar_select_today(calendar);
}

static void
_calendar_day_selected(GtkCalendar *calendar, gpointer data)
{
	AionTaskCreationAssistant* task_creation_assistant = AION_TASK_CREATION_ASSISTANT(data);
	AionTaskCreationAssistantPrivate* priv = aion_task_creation_assistant_get_instance_private(task_creation_assistant);
	GtkWidget *assistant_page = gtk_assistant_get_nth_page(GTK_ASSISTANT(task_creation_assistant), priv->task_next_execution_page_num); 
	GDate* selected_date= aion_util_calendar_get_date(GTK_CALENDAR(priv->task_next_execution_date_field));
	GDate* today_date = aion_util_get_today_date();
	gint days = g_date_days_between(today_date, selected_date);
	g_date_free(today_date);
	g_date_free(selected_date);
	gboolean complete = (days < 0) ? FALSE : TRUE; 
	/*  TODO display a notification message
	if (days <= 0) {
		AionNotification* notification = aion_notification_new_with_message("Postpone date must be in the future");
		gtk_widget_show(GTK_WIDGET(notification));
		gtk_overlay_add_overlay(GTK_OVERLAY(priv->notification_overlay), GTK_WIDGET(notification));
	}
	*/
	gtk_assistant_set_page_complete (GTK_ASSISTANT(task_creation_assistant), assistant_page, complete); 
}

static void
_preview_delay_enable_option_toggled(GtkToggleButton* preview_delay_enable_option, gpointer data)
{
	gboolean preview_enabled = gtk_toggle_button_get_active(preview_delay_enable_option);
	AionTaskCreationAssistant* task_creation_assistant = AION_TASK_CREATION_ASSISTANT(data);
	AionTaskCreationAssistantPrivate* priv = aion_task_creation_assistant_get_instance_private(task_creation_assistant);
	gtk_widget_set_visible(priv->preview_delay_frame, preview_enabled);
}

static void
_aion_task_create_task(AionTaskCreationAssistant* assistant)
{
	g_return_if_fail(AION_IS_TASK_CREATION_ASSISTANT(assistant));
	AionTaskCreationAssistantPrivate* priv = aion_task_creation_assistant_get_instance_private(assistant);

	// a task may be created multiple times with previous/next buttons
	if (priv->task != NULL)
		g_object_unref(priv->task);

	const gchar* label = gtk_entry_get_text(GTK_ENTRY(priv->task_label_field));
	g_assert(label != NULL);
	priv->task = aion_task_new(label); 

	GDate* date = aion_util_calendar_get_date(GTK_CALENDAR(priv->task_next_execution_date_field));
	g_assert(g_date_valid(date));
	aion_task_set_next_execution_date(priv->task, date);
	g_date_free(date);


	gboolean preview_enabled = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->preview_delay_enable_option));
	if (preview_enabled) {
		gint preview_delay = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->preview_delay_field));
		aion_task_set_preview_delay(priv->task, preview_delay);
	}

	gboolean single_execution = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->single_repetition_option));
	if (! single_execution) {
		GtkListBoxRow* selected_row =  gtk_list_box_get_selected_row (GTK_LIST_BOX(priv->task_frequency_field));
		g_assert(selected_row != NULL);
		AionTaskRepetitionType repetition_type = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(selected_row), "repetition-type"));
		aion_task_set_repetition_type(priv->task, repetition_type);

		gint interval = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->task_interval_field));
		aion_task_set_interval(priv->task, interval);
	}
}

gint
aion_task_creation_assistant_get_instance_count()
{
	return instance_counter;
}

gint
aion_task_creation_assistant_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_TASK_CREATION_ASSISTANT));
	return instance_counter;
}

