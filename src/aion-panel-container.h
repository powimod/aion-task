/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_PANEL_CONTAINER__
#define __AION_PANEL_CONTAINER__

#include "aion-panelable.h"
#include "aion-task.h"
#include <glib-object.h>
#include <gtk/gtk.h>

#define AION_TYPE_PANEL_CONTAINER aion_panel_container_get_type()

G_DECLARE_FINAL_TYPE (AionPanelContainer, aion_panel_container, AION, PANEL_CONTAINER, GtkBox);

AionPanelContainer* aion_panel_container_new();

gint aion_panel_container_get_instance_count();
gint aion_panel_container_check_zero_instance();


#define AION_TYPE_NO_TASK_PANEL aion_no_task_panel_get_type()

G_DECLARE_FINAL_TYPE (AionNoTaskPanel, aion_no_task_panel, AION, NO_TASK_PANEL, GtkLabel);

AionNoTaskPanel* aion_no_task_panel_new();
void aion_panel_container_add_panel(AionPanelContainer*, AionPanelable*, gboolean, gboolean);
void aion_panel_container_set_task(AionPanelContainer*, AionTask*);
void aion_panel_container_refresh(AionPanelContainer*);
void aion_panel_container_activate_panel(AionPanelContainer*, AionPanelable*, gboolean);

#endif /*__AION_PANEL_CONTAINER__*/

