/* AionTaskStorage
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_TASK_STORAGE__
#define __AION_TASK_STORAGE__

#include "aion-task.h"
#include <glib-object.h>
#include <gio/gio.h>

#define AION_TYPE_TASK_STORAGE aion_task_storage_get_type()

G_DECLARE_FINAL_TYPE (AionTaskStorage, aion_task_storage, AION, TASK_STORAGE, GObject);


typedef enum {
        AION_STORAGE_ERROR_FILE_NOT_FOUND,
	AION_STORAGE_ERROR_FILE_READ_ERROR,
	AION_STORAGE_ERROR_XML_ERROR
} AionStorageError;
#define AION_TYPE_STORAGE_ERROR aion_storage_error_quark()
GQuark aion_storage_error_quark(void);


AionTaskStorage* aion_task_storage_new(GFile* xml_file);

gboolean aion_task_storage_save(AionTaskStorage*, GSList*, GError**);
gboolean aion_task_storage_load(AionTaskStorage*, GSList**, GError**);

gint aion_task_storage_get_instance_count();
gint aion_task_storage_check_zero_instance();

#endif /*__AION_TASK_STORAGE__*/

