/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_MAIN_WINDOW__
#define __AION_MAIN_WINDOW__

#include "aion-application.h"
#include "aion-notification.h"
#include <glib-object.h>
#include <gtk/gtk.h>
#include <libhandy-1/handy.h>

G_BEGIN_DECLS

#define AION_TYPE_MAIN_WINDOW aion_main_window_get_type()

G_DECLARE_FINAL_TYPE(AionMainWindow, aion_main_window, AION, MAIN_WINDOW, HdyApplicationWindow)

AionMainWindow* aion_main_window_new(AionApplication*);

void aion_main_window_declare_actions(AionMainWindow*);
void aion_main_window_display_data(AionMainWindow*);

AionApplication* aion_main_window_get_application(AionMainWindow*);
AionNotification* aion_main_window_display_notification(AionMainWindow*, gchar*);

gint aion_main_window_get_instance_count();
gint aion_main_window_check_zero_instance();

G_END_DECLS

#endif  /* __AION_MAIN_WINDOW__ */

