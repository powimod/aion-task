/* AionTaskManager
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-TaskManager"
#include "aion-task-manager.h"
#include "aion-task-storage.h"

struct _AionTaskManager {
	GObject parent;
};

typedef struct {
	GSList* task_list;
	AionTaskStorage* task_storage;
} AionTaskManagerPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(AionTaskManager, aion_task_manager, G_TYPE_OBJECT);

static gint instance_counter = 0;

static void
aion_task_manager_init(AionTaskManager* task_manager)
{
	AionTaskManagerPrivate* priv = aion_task_manager_get_instance_private(task_manager);
	priv->task_list = NULL;
	priv->task_storage = NULL;
	instance_counter++;
}


static void
aion_task_manager_dispose(GObject* object)
{
	//AionTaskManager* task_manager = AION_TASK_MANAGER(object);
	//AionTaskManagerPrivate *priv = aion_task_manager_get_instance_private(task_manager);
	G_OBJECT_CLASS(aion_task_manager_parent_class)->dispose(object);
}

static void
_free_task(gpointer data)
{
	AionTask* task = (AionTask*) data;
	g_object_unref(task);
}


static void
aion_task_manager_finalize(GObject* object)
{
	AionTaskManager* task_manager = AION_TASK_MANAGER(object);
	AionTaskManagerPrivate *priv = aion_task_manager_get_instance_private(task_manager);
	g_slist_free_full(priv->task_list, _free_task);
	if (priv->task_storage) {
		// FIXME where to unref TaskStorage (in dispose or in finalize function) ?
		g_object_unref(priv->task_storage);
		priv->task_storage = NULL;
	}
	G_OBJECT_CLASS(aion_task_manager_parent_class)->finalize(object);
	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_TASK_MANAGER));
}

 
static void
aion_task_manager_class_init(AionTaskManagerClass* task_manager_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(task_manager_class);
	object_class->dispose= aion_task_manager_dispose;
	object_class->finalize = aion_task_manager_finalize;
}


AionTaskManager*
aion_task_manager_new()
{
	AionTaskManager* task_manager = AION_TASK_MANAGER(g_object_new(AION_TYPE_TASK_MANAGER, NULL));
	//AionTaskManagerPrivate *priv = aion_task_manager_get_instance_private(task_manager);
	return task_manager;
}


AionTask*
aion_task_manager_create_task(AionTaskManager* task_manager, gchar* label)
{
	g_return_val_if_fail(AION_IS_TASK_MANAGER(task_manager), NULL);
	g_return_val_if_fail(label != NULL, NULL);
	AionTaskManagerPrivate *priv = aion_task_manager_get_instance_private(task_manager);
	AionTask* task = aion_task_new(label);
	priv->task_list = g_slist_append(priv->task_list, task);
	return task;
}

gint
aion_task_manager_task_count(AionTaskManager* task_manager)
{
	g_return_val_if_fail(AION_IS_TASK_MANAGER(task_manager), -1);
	AionTaskManagerPrivate* priv = aion_task_manager_get_instance_private(task_manager);
	return g_slist_length(priv->task_list);
}

void
aion_task_manager_for_each_task(AionTaskManager* task_manager, AionTaskCallback callback, gpointer data)
{
	g_return_if_fail(AION_IS_TASK_MANAGER(task_manager));
	g_return_if_fail(callback != NULL);
	AionTaskManagerPrivate* priv = aion_task_manager_get_instance_private(task_manager);
	g_slist_foreach(priv->task_list, (GFunc)callback, data);
}


gboolean
aion_task_manager_contains_task(AionTaskManager* task_manager, AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK_MANAGER(task_manager), FALSE);
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	AionTaskManagerPrivate* priv = aion_task_manager_get_instance_private(task_manager);
	return (g_slist_find(priv->task_list, task) == NULL) ? FALSE : TRUE;
}

gboolean
aion_task_manager_append_task(AionTaskManager* task_manager, AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK_MANAGER(task_manager), FALSE);
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	AionTaskManagerPrivate* priv = aion_task_manager_get_instance_private(task_manager);
	if (g_slist_find(priv->task_list, task) != NULL)
		return FALSE; // task is already in task list
	priv->task_list = g_slist_append(priv->task_list, task);
	g_object_ref(G_OBJECT(task));
	return TRUE;
}

gboolean
aion_task_manager_remove_task(AionTaskManager* task_manager, AionTask* task)
{
	g_return_val_if_fail(AION_IS_TASK_MANAGER(task_manager), FALSE);
	g_return_val_if_fail(AION_IS_TASK(task), FALSE);
	AionTaskManagerPrivate* priv = aion_task_manager_get_instance_private(task_manager);
	if (g_slist_find(priv->task_list, task) == NULL)
		return FALSE; // task is not list
	priv->task_list = g_slist_remove(priv->task_list, task); 
	g_object_unref(G_OBJECT(task));
	return TRUE;
}

void
aion_task_manager_initialize_task_manager(AionTaskManager* aion_task_manager, const gchar* data_file_path)
{
	g_return_if_fail(AION_IS_TASK_MANAGER(aion_task_manager));
	AionTaskManagerPrivate* priv = aion_task_manager_get_instance_private(aion_task_manager);
	g_assert(priv->task_storage == NULL);
	GFile* g_data_file = g_file_new_for_path(data_file_path);
	priv->task_storage = aion_task_storage_new(g_data_file);
	g_object_unref(g_data_file);
}

gboolean
aion_task_manager_save_data(AionTaskManager* aion_task_manager, GError** p_error)
{
	g_return_val_if_fail(AION_IS_TASK_MANAGER(aion_task_manager), FALSE);
	AionTaskManagerPrivate* priv = aion_task_manager_get_instance_private(aion_task_manager);
	g_assert(priv->task_storage != NULL);
	g_assert(AION_IS_TASK_STORAGE(priv->task_storage));
	return aion_task_storage_save(priv->task_storage, priv->task_list, p_error);
}

gboolean
aion_task_manager_load_data(AionTaskManager* aion_task_manager, GError** p_error)
{
	g_return_val_if_fail(AION_IS_TASK_MANAGER(aion_task_manager), FALSE);
	AionTaskManagerPrivate* priv = aion_task_manager_get_instance_private(aion_task_manager);
	g_assert(priv->task_storage != NULL); // can be call once during startup
	g_assert(AION_IS_TASK_STORAGE(priv->task_storage));
	return aion_task_storage_load(priv->task_storage, &(priv->task_list), p_error);
}

gint
aion_task_manager_get_instance_count()
{
	return instance_counter;
}

gint
aion_task_manager_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_TASK_MANAGER));
	return instance_counter;
}
