/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-PanelContainer"
#include <glib/gi18n.h>

#include "aion-panel-container.h"

struct _AionPanelContainer {
	GtkFrame parent;
};

typedef struct {
	GtkWidget* panel_stack;
	AionNoTaskPanel* no_task_panel;
	AionTask* task;
	GtkWidget* start_edit_button;
	GtkWidget* editing_validate_button;
	GtkWidget* editing_cancel_button;
	AionPanelable* current_panel;
} AionPanelContainerPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(AionPanelContainer, aion_panel_container, GTK_TYPE_BOX);

enum {
	SIGNAL_EDITING_STARTED,
	SIGNAL_EDITING_FINISHED,
	SIGNAL_NEEDS_SAVE,
	LAST_SIGNAL
};
static guint aion_panel_container_signals[LAST_SIGNAL] = { 0 };


// local prototypes
static void _aion_panel_container_set_task_in_panel(AionPanelContainer*, AionPanelable*, AionTask*);
static GtkWidget* _get_panel_parent(AionPanelable*);
static AionPanelable* _get_panel_child(GtkWidget*);
static void _panel_stack_visible_child_changed(GtkWidget*, GParamSpec*, AionPanelContainer*);
static void _start_editing_button_clicked(GtkButton*, gpointer);
static void _validate_editing_button_clicked(GtkButton*, gpointer);
static void _cancel_editing_button_clicked(GtkButton*, gpointer);
static void _define_edit_button_visibility(AionPanelContainer*);
static void _panel_editing_started(AionPanelable*, AionTask*, gpointer);
static void _panel_editing_finished(AionPanelable*, AionTask*, gboolean, gpointer);
static void _detect_panel_changes(AionPanelContainer*);

static gint instance_counter = 0;

static void
aion_panel_container_init(AionPanelContainer* panel_container)
{
	g_return_if_fail(AION_IS_PANEL_CONTAINER(panel_container));
	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);
	priv->task = NULL;

	gtk_orientable_set_orientation(GTK_ORIENTABLE(panel_container), GTK_ORIENTATION_VERTICAL);

	// horizontal box with panel switcher and edit buttons
	GtkWidget* h_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_show(h_box);
	gtk_box_pack_start(GTK_BOX(panel_container), h_box, FALSE, FALSE, 0);

	// Panel switcher
	GtkWidget* panel_switcher = gtk_stack_switcher_new();
	gtk_box_pack_start(GTK_BOX(h_box), panel_switcher, TRUE, TRUE, 0);
	gtk_widget_show(GTK_WIDGET(panel_switcher));

	// edit button
	GtkWidget* button = gtk_button_new_from_icon_name("document-edit-symbolic", GTK_ICON_SIZE_SMALL_TOOLBAR); 
	gtk_box_pack_start(GTK_BOX(h_box), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(_start_editing_button_clicked), panel_container);
	priv->start_edit_button = button;

	// editing cancel button
	button = gtk_button_new_from_icon_name("gtk-cancel", GTK_ICON_SIZE_SMALL_TOOLBAR); 
	gtk_box_pack_start(GTK_BOX(h_box), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(_cancel_editing_button_clicked), panel_container);
	priv->editing_cancel_button = button;

	// editing validation button
	button = gtk_button_new_from_icon_name("gtk-ok", GTK_ICON_SIZE_SMALL_TOOLBAR); 
	gtk_box_pack_start(GTK_BOX(h_box), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(_validate_editing_button_clicked), panel_container);
	priv->editing_validate_button = button;

	// Panel stack
	GtkWidget* panel_stack = gtk_stack_new();
	gtk_box_pack_start(GTK_BOX(panel_container), panel_stack, TRUE, TRUE, 0);
	gtk_stack_switcher_set_stack(GTK_STACK_SWITCHER(panel_switcher), GTK_STACK(panel_stack));
	gtk_widget_show(GTK_WIDGET(panel_stack));
	g_signal_connect(G_OBJECT(panel_stack), "notify::visible-child", G_CALLBACK(_panel_stack_visible_child_changed), panel_container);
	priv->panel_stack = panel_stack;

	// NoTask panel
	AionNoTaskPanel* no_task_panel = aion_no_task_panel_new();
	aion_panel_container_add_panel(panel_container, AION_PANELABLE(no_task_panel), FALSE, FALSE);
	priv->no_task_panel = no_task_panel;

	instance_counter++;
}

static void
aion_panel_container_dispose(GObject* object)
{
	AionPanelContainer* panel_container = AION_PANEL_CONTAINER(object);
	AionPanelContainerPrivate *priv = aion_panel_container_get_instance_private(panel_container);
	if (priv->task) {
		g_object_unref(priv->task);
		priv->task = NULL;
	}
	G_OBJECT_CLASS(aion_panel_container_parent_class)->dispose(object);
}


static void
aion_panel_container_finalize(GObject* object)
{
	//AionPanelContainer* panel_container = AION_PANEL_CONTAINER(object);
	//AionPanelContainerPrivate *priv = aion_panel_container_get_instance_private(panel_container);
	G_OBJECT_CLASS(aion_panel_container_parent_class)->finalize(object);
	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_PANEL_CONTAINER));
}

 
static void
aion_panel_container_class_init(AionPanelContainerClass* panel_container_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(panel_container_class);
	object_class->dispose= aion_panel_container_dispose;
	object_class->finalize = aion_panel_container_finalize;

	aion_panel_container_signals[SIGNAL_EDITING_STARTED] = 
		g_signal_new ("panel-editing-started",
			G_TYPE_FROM_CLASS(panel_container_class),
			G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
			0,                   // class offset
			NULL,                // accumulator 
			NULL,                // accumulator data
			NULL,                // C marshaller
			G_TYPE_NONE,         // return_type
			2,                   // n_params 
			AION_TYPE_PANELABLE, // first parameter
			AION_TYPE_TASK);     // second parameter

	aion_panel_container_signals[SIGNAL_EDITING_FINISHED] = 
		g_signal_new ("panel-editing-finished",
			G_TYPE_FROM_CLASS(panel_container_class),
			G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
			0,                   // class offset
			NULL,                // accumulator 
			NULL,                // accumulator data
			NULL,                // C marshaller
			G_TYPE_NONE,         // return_type
			3,                   // n_params 
			AION_TYPE_PANELABLE, // first parameter
			AION_TYPE_TASK,      // second parameter
			G_TYPE_BOOLEAN);     // third parameter

	aion_panel_container_signals[SIGNAL_NEEDS_SAVE] = 
		g_signal_new ("save-needed",
			G_TYPE_FROM_CLASS(panel_container_class),
			G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
			0,                   // class offset
			NULL,                // accumulator 
			NULL,                // accumulator data
			NULL,                // C marshaller
			G_TYPE_BOOLEAN,         // return_type
			2,                   // n_params 
			AION_TYPE_PANELABLE, // first parameter
			AION_TYPE_TASK);     // second parameter

}


AionPanelContainer*
aion_panel_container_new()
{
	AionPanelContainer* panel_container = AION_PANEL_CONTAINER(g_object_new(AION_TYPE_PANEL_CONTAINER, NULL));
	return panel_container;
}


gint
aion_panel_container_get_instance_count()
{
	return instance_counter;
}

gint
aion_panel_container_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_PANEL_CONTAINER));
	return instance_counter;
}


// Class AionNoTaskPanel

struct _AionNoTaskPanel 
{
	GtkLabel parent;
};

static void aion_no_task_panel_panelable_interface_init(AionPanelableInterface*);

G_DEFINE_TYPE_WITH_CODE (AionNoTaskPanel, aion_no_task_panel, GTK_TYPE_LABEL,
	G_IMPLEMENT_INTERFACE(AION_TYPE_PANELABLE, aion_no_task_panel_panelable_interface_init)
);


static void
aion_no_task_panel_init (AionNoTaskPanel* panel)
{
	gtk_widget_set_valign(GTK_WIDGET(panel), GTK_ALIGN_START);
	gtk_widget_set_halign(GTK_WIDGET(panel), GTK_ALIGN_START);
	gtk_widget_set_margin_start(GTK_WIDGET(panel), 15);
	gtk_widget_set_margin_top(GTK_WIDGET(panel), 10);
	gtk_widget_show(GTK_WIDGET(panel));
	gtk_label_set_text(GTK_LABEL(panel), _("No task selected"));
}

// TODO cleanup
//static void
//aion_no_task_panel_finalize(GObject* object)
//{
//	//AionNoTaskPanel* no_task_panel = AION_NO_TASK_PANEL(object);
//	G_OBJECT_CLASS(aion_no_task_panel_parent_class)->finalize(object);
//}
 
static void
aion_no_task_panel_class_init(AionNoTaskPanelClass* no_task_panel_class)
{
	//GObjectClass* object_class = G_OBJECT_CLASS(no_task_panel_class);
	//object_class->finalize = aion_no_task_panel_finalize;
}


static const gchar*
_aion_panelable_interface_get_name(AionPanelable* panel)
{
	return "no-task-panel";
}


static const gchar*
_aion_panelable_interface_get_title(AionPanelable* panel)
{
	return _("No task selected");
}

static const gchar*
_aion_panelable_interface_get_icon_name(AionPanelable* panel)
{
	return "dialog-error-symbolic"; // TODO use specific icon
}

static void
_aion_panelable_interface_set_task(AionPanelable* panel, AionTask* task)
{
	// NoTask panel does not use current task
}

static void
_aion_panelable_interface_refresh(AionPanelable* panel)
{
	// NoTask panel has nothing to refresh 
}

static gboolean
_aion_panelable_interface_is_editable(AionPanelable* panel)
{
	return FALSE;
}

static gboolean
_aion_panelable_interface_needs_save(AionPanelable* panel)
{
	return FALSE;
}

static void
_aion_panelable_interface_start_editing(AionPanelable* panel)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	g_assert_not_reached(); // editing is not supported by no-task panel
}

static void
_aion_panelable_interface_end_editing(AionPanelable* panel, gboolean validation)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	g_assert_not_reached(); // editing is not supported by no-task panel
}

static void
aion_no_task_panel_panelable_interface_init(AionPanelableInterface* iface)
{
	iface->get_name = _aion_panelable_interface_get_name;
	iface->get_title = _aion_panelable_interface_get_title;
	iface->get_icon_name = _aion_panelable_interface_get_icon_name;
	iface->set_task = _aion_panelable_interface_set_task;
	iface->refresh = _aion_panelable_interface_refresh;
	iface->is_editable = _aion_panelable_interface_is_editable;
	iface->needs_save = _aion_panelable_interface_needs_save;
	iface->start_editing = _aion_panelable_interface_start_editing;
	iface->end_editing = _aion_panelable_interface_end_editing;
}

AionNoTaskPanel*
aion_no_task_panel_new()
{
	return g_object_new(AION_TYPE_NO_TASK_PANEL, NULL);
}

void
aion_panel_container_add_panel(AionPanelContainer* panel_container, AionPanelable* panel, gboolean with_frame, gboolean with_scroll)
{
	g_return_if_fail(AION_IS_PANEL_CONTAINER(panel_container));
	g_return_if_fail(AION_IS_PANELABLE(panel));
	if (! GTK_IS_WIDGET(panel))
		g_error("Panel to add is not a GtkWidget");

	const gchar* panel_name = aion_panelable_get_name(AION_PANELABLE(panel));
	g_return_if_fail(panel_name != NULL);
	const gchar* panel_icon_name = aion_panelable_get_icon_name(AION_PANELABLE(panel));
	g_return_if_fail(panel_icon_name != NULL);
	const gchar* panel_title = aion_panelable_get_title(AION_PANELABLE(panel));
	g_return_if_fail(panel_title != NULL);

	gtk_widget_show(GTK_WIDGET(panel));
	GtkWidget* widget_to_add = GTK_WIDGET(panel);

	if (with_scroll) {
		GtkWidget *scrolled_window = gtk_scrolled_window_new(NULL, NULL);
		gtk_widget_show(scrolled_window);
		gtk_container_set_border_width(GTK_CONTAINER(scrolled_window), 5);
		gtk_container_add (GTK_CONTAINER(scrolled_window), GTK_WIDGET(panel));
		widget_to_add = scrolled_window;
	}

	if (with_frame) {
		GtkWidget* panel_frame = gtk_frame_new(panel_title);
		gtk_widget_show(panel_frame);
		gtk_frame_set_label_align(GTK_FRAME(panel_frame), 0.05, 0.5);
		gtk_container_set_border_width(GTK_CONTAINER(panel_frame), 5); // FIXME hard coded spacing
		gtk_container_add(GTK_CONTAINER(panel_frame), widget_to_add);
		widget_to_add = panel_frame;

		GtkWidget* label = gtk_frame_get_label_widget(GTK_FRAME(panel_frame));
		gtk_widget_set_margin_start(label, 5);
		gtk_widget_set_margin_end(label, 5);
	}

	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);
	gtk_stack_add_named(GTK_STACK(priv->panel_stack), widget_to_add, panel_name);
	gtk_container_child_set(GTK_CONTAINER(priv->panel_stack), GTK_WIDGET(widget_to_add), 
		"icon-name", panel_icon_name,
		"title",     panel_title,
		NULL);

	if (panel != AION_PANELABLE(priv->no_task_panel)) {	
		g_signal_connect(G_OBJECT(panel), "editing-started",  G_CALLBACK(_panel_editing_started),  panel_container);
		g_signal_connect(G_OBJECT(panel), "editing-finished", G_CALLBACK(_panel_editing_finished), panel_container);
	}

	_aion_panel_container_set_task_in_panel(panel_container, panel, priv->task);

}

static AionPanelable*
_get_panel_child(GtkWidget* widget)
{
	//g_print("widget class [%s]\n", g_type_name(G_OBJECT_TYPE(widget)));
	g_assert(GTK_IS_BIN(widget) || AION_IS_PANELABLE(widget));
	GtkWidget* child = GTK_WIDGET(widget);
	while (! AION_IS_PANELABLE(child)) {
		g_assert(GTK_IS_BIN(child));
		child = gtk_bin_get_child(GTK_BIN(child));
		// g_print("widget class [%s]\n", g_type_name(G_OBJECT_TYPE(child)));
		g_assert(child != NULL);
	}
	return AION_PANELABLE(child);
}

static GtkWidget*
_get_panel_parent(AionPanelable* panel)
{
 	// Ignore GtkFrame / GtkScrolledWindow / GtkViewport between panel and it's GtkStack container
	g_assert(AION_IS_PANELABLE(panel));
	GtkWidget* child = GTK_WIDGET(panel);
	g_assert(child != NULL);
	GtkWidget* parent = gtk_widget_get_parent(child);
	g_assert(parent != NULL);
	while (! GTK_IS_STACK(parent)) {
		child = parent;
		parent = gtk_widget_get_parent(child);
		g_assert(parent != NULL);
	}
	g_assert(child != NULL);
	return child;
}

void
aion_panel_container_set_task(AionPanelContainer* panel_container, AionTask* task)
{
	g_return_if_fail(AION_IS_PANEL_CONTAINER(panel_container));
	g_return_if_fail(task == NULL || AION_IS_TASK(task));
	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);

	_detect_panel_changes(panel_container);

	if (priv->task)
		g_object_unref(priv->task);
	priv->task = task;
	if (priv->task)
		g_object_ref(priv->task);
	//g_print("Setting task in panel container : [%s]\n", task ? aion_task_get_label(task) : "no task");

	GList* panel_list = gtk_container_get_children(GTK_CONTAINER(priv->panel_stack));
	GList* panel_item = g_list_first(panel_list);
	while (panel_item) {
		GtkWidget* panel_widget = GTK_WIDGET(panel_item->data);
		AionPanelable* panel = _get_panel_child(panel_widget);
		_aion_panel_container_set_task_in_panel(panel_container, panel, task);
		panel_item = g_list_next(panel_item);
	}
	g_list_free(panel_list);

	_define_edit_button_visibility(panel_container);
}

void
aion_panel_container_refresh(AionPanelContainer* panel_container)
{
	g_return_if_fail(AION_IS_PANEL_CONTAINER(panel_container));
	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);

	GList* panel_list = gtk_container_get_children(GTK_CONTAINER(priv->panel_stack));
	GList* panel_item = g_list_first(panel_list);
	while (panel_item) {
		GtkWidget* panel_widget = GTK_WIDGET(panel_item->data);
		AionPanelable* panel = _get_panel_child(panel_widget);
		aion_panelable_refresh(panel);
		panel_item = g_list_next(panel_item);
	}
	g_list_free(panel_list);
}
static void
_aion_panel_container_set_task_in_panel (AionPanelContainer* panel_container, AionPanelable *panel, AionTask* task)
{
	g_assert(AION_IS_PANEL_CONTAINER(panel_container));
	g_assert(AION_IS_PANELABLE(panel));
	g_return_if_fail(task == NULL || AION_IS_TASK(task));

	aion_panelable_set_task(panel, task);

	GtkWidget* panel_parent = _get_panel_parent(panel);
	if (AION_IS_NO_TASK_PANEL(panel)) {
		// display no-task panel when no task is selected
		gtk_widget_set_visible(GTK_WIDGET(panel_parent), (task == NULL));
	} else {
		// display other panels when a task is selected
		gtk_widget_set_visible(GTK_WIDGET(panel_parent), (task != NULL));
	}
	//g_print("panel [%s] visible=%s\n", panel_name, gtk_widget_is_visible(GTK_WIDGET(panel_parent)) ? "yes" : "no");
}

static void
_set_edit_mode(AionPanelContainer* panel_container, gboolean edit_mode)
{
	g_assert(AION_IS_PANEL_CONTAINER(panel_container));
	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);
	gtk_widget_set_visible(priv->start_edit_button, ! edit_mode);
	gtk_widget_set_visible(priv->editing_validate_button, edit_mode);
	gtk_widget_set_visible(priv->editing_cancel_button, edit_mode);
}

static void
_define_edit_button_visibility(AionPanelContainer* panel_container)
{
	g_assert(AION_IS_PANEL_CONTAINER(panel_container));
	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);
	g_assert(priv->current_panel != NULL);
	gboolean visible = aion_panelable_is_editable(priv->current_panel);
	//g_print("Panel [%s] edit button visible : %s\n", aion_panelable_get_name(priv->current_panel), visible ? "yes" : "no");
	gtk_widget_set_visible(priv->start_edit_button, visible);
	gtk_widget_set_visible(priv->editing_validate_button, FALSE);
	gtk_widget_set_visible(priv->editing_cancel_button, FALSE);
}

static void
_panel_stack_visible_child_changed(GtkWidget* gtk_stack, GParamSpec *pspec, AionPanelContainer* panel_container)
{
	g_assert(GTK_IS_STACK(gtk_stack));
	g_assert(AION_IS_PANEL_CONTAINER(panel_container));
	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);

	 GtkWidget* panel_widget = gtk_stack_get_visible_child(GTK_STACK(priv->panel_stack));
	// FIXME why is panel null when a task is selected in task for the first time
	// (maybe because all panels are hidden at the same time)
	if (panel_widget == NULL)  
		return;

	_detect_panel_changes(panel_container);

	AionPanelable* panel = _get_panel_child(panel_widget);
	//g_print("Selected panel is changed : %s\n", aion_panelable_get_name(panel));
	priv->current_panel = panel;
	_define_edit_button_visibility(panel_container);
}

static void
_start_editing_button_clicked(GtkButton* button, gpointer data)
{
	AionPanelContainer* panel_container = AION_PANEL_CONTAINER(data);
	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);
	_set_edit_mode(panel_container, TRUE);
	g_assert(priv->current_panel != NULL);
	aion_panelable_start_editing(priv->current_panel);
}

static void
_validate_editing_button_clicked(GtkButton* button, gpointer data)
{
	AionPanelContainer* panel_container = AION_PANEL_CONTAINER(data);
	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);
	_set_edit_mode(panel_container, FALSE);
	g_assert(priv->current_panel != NULL);
	aion_panelable_end_editing(priv->current_panel, TRUE);
}

static void
_cancel_editing_button_clicked(GtkButton* button, gpointer data)
{
	AionPanelContainer* panel_container = AION_PANEL_CONTAINER(data);
	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);
	gtk_widget_show(priv->start_edit_button);
	gtk_widget_hide(priv->editing_validate_button);
	gtk_widget_hide(priv->editing_cancel_button);
	g_assert(priv->current_panel != NULL);
	aion_panelable_end_editing(priv->current_panel, FALSE);
}

void
aion_panel_container_activate_panel(AionPanelContainer* panel_container, AionPanelable* panel, gboolean edit_mode)
{
	g_return_if_fail(AION_IS_PANEL_CONTAINER(panel_container));
	g_return_if_fail(AION_IS_PANELABLE(panel));
	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);
	GtkWidget* panel_widget =  _get_panel_parent(panel);
	gtk_stack_set_visible_child(GTK_STACK(priv->panel_stack), panel_widget);
	if (edit_mode) {
		_set_edit_mode(panel_container, TRUE);
		aion_panelable_start_editing(panel);
	}
}

static void 
_panel_editing_started(AionPanelable* panel, AionTask* task, gpointer data)
{
	g_assert(AION_IS_PANELABLE(panel));
	g_assert(AION_IS_TASK(task));
	AionPanelContainer* panel_container = AION_PANEL_CONTAINER(data);
	g_debug("Editing started in panel [%s] with task [%s]", 
		aion_panelable_get_name(panel), aion_task_get_label(task));
	g_signal_emit (panel_container, aion_panel_container_signals[SIGNAL_EDITING_STARTED], 0, panel, task);
}

static void 
_panel_editing_finished(AionPanelable* panel, AionTask* task, gboolean modification, gpointer data)
{
	g_assert(AION_IS_PANELABLE(panel));
	g_assert(AION_IS_TASK(task));
	AionPanelContainer* panel_container = AION_PANEL_CONTAINER(data);
	g_debug("Editing finished in panel [%s] with task [%s] modification=%s", 
		aion_panelable_get_name(panel), aion_task_get_label(task), modification ? "yes" : "no");
	g_signal_emit (panel_container, aion_panel_container_signals[SIGNAL_EDITING_FINISHED], 0, panel, task, modification);
}

static void
_detect_panel_changes(AionPanelContainer* panel_container)
{
	g_assert(AION_IS_PANEL_CONTAINER(panel_container));
	AionPanelContainerPrivate* priv = aion_panel_container_get_instance_private(panel_container);

	AionPanelable* panel = priv->current_panel;
	if (panel == NULL)
		return;
	if (! aion_panelable_needs_save(panel))
		return;

	GValue* params = g_new0 (GValue, 3);

	g_value_init (&params[0], G_TYPE_OBJECT);
	g_value_set_object (&params[0], G_OBJECT(panel_container));

	g_value_init (&params[1], G_TYPE_OBJECT);
	g_value_set_object (&params[1], G_OBJECT(priv->current_panel));

	g_value_init (&params[2], G_TYPE_OBJECT);
	g_value_set_object (&params[2], G_OBJECT(priv->task));

	GValue return_val = G_VALUE_INIT;
	g_value_init (&return_val, G_TYPE_BOOLEAN);

	g_signal_emitv (params, aion_panel_container_signals[SIGNAL_NEEDS_SAVE], 0, &return_val);

	gboolean save_asked = g_value_get_boolean (&return_val);
	g_value_unset (&return_val);

	g_value_unset (&params[0]);
	g_value_unset (&params[1]);
	g_value_unset (&params[2]);
  	g_free (params);

	if (save_asked)
		aion_panelable_end_editing(panel, TRUE);
}
