/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-Util"
#include <glib/gi18n.h>

#include "aion-task.h"
#include "aion-sub-task.h"
#include "aion-application.h"
#include "aion-main-window.h"
#include "aion-task-validation-dialog.h"
#include "aion-task-postpone-dialog.h"
#include "aion-notification.h"
#include "aion-task-list-row.h"
#include "aion-task-manager.h"
#include "aion-task-storage.h"

void check_memory_freed()
{
	gint n = 0;
	n+= aion_task_check_zero_instance();
	n+= aion_sub_task_check_zero_instance();
	n+= aion_application_check_zero_instance();
	n+= aion_main_window_check_zero_instance();
	n+= aion_task_validation_dialog_check_zero_instance();
	n+= aion_task_postpone_dialog_check_zero_instance();
	n+= aion_notification_check_zero_instance();
	n+= aion_task_list_row_check_zero_instance();
	n+= aion_task_manager_check_zero_instance();
	n+= aion_task_storage_check_zero_instance();
	if (n != 0)
		g_error("Non-freed object counter : %i", n);
}

GDate* aion_util_get_today_date()
{
	GDateTime* today_date_time = g_date_time_new_now_local();
	guint year  = g_date_time_get_year(today_date_time);
	guint month = g_date_time_get_month(today_date_time);
	guint day   = g_date_time_get_day_of_month(today_date_time);
	g_date_time_unref(today_date_time);
	return g_date_new_dmy(day, month, year);
}


GDate* aion_util_calendar_get_date(GtkCalendar* calendar)
{
	g_return_val_if_fail(GTK_IS_CALENDAR(calendar), NULL);
	guint year, month, day;
	gtk_calendar_get_date (GTK_CALENDAR(calendar), &year, &month, &day);
	month++; // pass range from 0-30 to 1-31 
	return g_date_new_dmy(day, month, year);
}

gchar*
aion_util_get_duration_human_representation(gint minutes)
{
	gint hours = minutes / 60;
	if (hours == 0)
		return g_strdup_printf(_("%i m"), minutes);
	minutes -= hours * 60;
	if (minutes == 0)
		return g_strdup_printf(_("%i h"), hours);
	return g_strdup_printf(_("%i h %i m"), hours, minutes);
}

gchar*
aion_util_get_date_local_representation(const GDate* date)
{
	g_return_val_if_fail(g_date_valid(date), NULL);
	gchar* date_str = g_malloc(32 + 1);
	g_date_strftime (date_str, 32, "%Ex", date);
	return  date_str;
}

static gboolean
_grab_focus_iddle_callback(gpointer data)
{
	GtkWidget* widget = GTK_WIDGET(data);
	gtk_widget_grab_focus(widget);
	return FALSE;
}

void
aion_util_grab_focus(GtkWidget* widget)
{
	g_return_if_fail(GTK_IS_WIDGET(widget));
	g_idle_add(_grab_focus_iddle_callback, widget);
}
