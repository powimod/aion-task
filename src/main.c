/* Aion-Task
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-Main"
#include <glib/gi18n.h>
#include <locale.h>

#include <gtk/gtk.h>

#include "aion-application.h"
#include "util.h"
#include "aion-task-config.h"


/* TODO cleanup
void test() {
	AionTask* task = aion_task_new("toto");

	GDate* dt = g_date_new_dmy(14,7,2023);
	aion_task_set_next_execution_date(task, dt);
	aion_task_set_preview_delay(task, 2);
	aion_task_set_postponement_delay(task, 2);
	g_date_free(dt);

	//aion_task_set_repetition_type(task, AION_TASK_REPETITION_EVERY_MONTH);
	//aion_task_set_interval(task, 2);
	//aion_task_set_postpone_from_when(task, AION_TASK_POSTPONE_FROM_DUE_DATE);
	//aion_task_validate(task);
	g_print("State : %s\n", aion_task_get_state_str(task));
	g_print("end\n");
	exit(1);
}
*/

int
main (int argc, char **argv)
{
	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
        bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
        textdomain(GETTEXT_PACKAGE);

	gchar* data_dir = g_build_filename(g_get_user_data_dir(), "aion-task", NULL); 
	g_debug("Aion-Task data directory : %s", data_dir);
	AionApplication* aion_application = aion_application_new(data_dir); 
	int status = g_application_run (G_APPLICATION (aion_application), argc, argv);
	g_object_unref(aion_application);

	check_memory_freed();
	return status;
}
