/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_NOTIFICATION__
#define __AION_NOTIFICATION__

#include "aion-task.h"
#include <glib-object.h>
#include <gtk/gtk.h>

#define AION_TYPE_NOTIFICATION aion_notification_get_type()

G_DECLARE_FINAL_TYPE (AionNotification, aion_notification, AION, NOTIFICATION, GtkBox);

AionNotification* aion_notification_new_with_message(gchar*);
AionNotification* aion_notification_new_with_action(gchar*, gchar*);

void aion_notification_set_message(AionNotification*, const gchar*);
void aion_notification_set_action(AionNotification*, const gchar*, GAction*);

gint aion_notification_get_instance_count();
gint aion_notification_check_zero_instance();

#endif /*__AION_NOTIFICATION__*/

