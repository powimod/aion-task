/* AionTaskManager
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_TASK_MANAGER__
#define __AION_TASK_MANAGER__

#include "aion-task.h"
#include "aion-task-manager.h"
#include <glib-object.h>

#define AION_TYPE_TASK_MANAGER aion_task_manager_get_type()

G_DECLARE_FINAL_TYPE (AionTaskManager, aion_task_manager, AION, TASK_MANAGER, GObject);

AionTaskManager* aion_task_manager_new();
AionTask* aion_task_manager_create_task(AionTaskManager* task_manager, gchar* label);

typedef void (*AionTaskCallback) (AionTask*, gpointer);
gint aion_task_manager_task_count(AionTaskManager*);
void aion_task_manager_for_each_task(AionTaskManager*, AionTaskCallback, gpointer);
gboolean aion_task_manager_contains_task(AionTaskManager*, AionTask*);
gboolean aion_task_manager_append_task(AionTaskManager*, AionTask*);
gboolean aion_task_manager_remove_task(AionTaskManager*, AionTask*);

void aion_task_manager_initialize_task_manager(AionTaskManager*, const gchar*);
gboolean aion_task_manager_save_data(AionTaskManager*, GError**);
gboolean aion_task_manager_load_data(AionTaskManager*, GError**);

gint aion_task_manager_get_instance_count();
gint aion_task_manager_check_zero_instance();

#endif /*__AION_TASK_MANAGER__*/

