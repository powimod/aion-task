/* AionSubTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-SubTask"
#include "aion-sub-task.h"
#include "util.h"

#define DEFAULT_LOG_ENTRIES_LIMIT 5

struct _AionSubTask {
	GObject parent;
};

typedef struct {
	gint position;
	gchar* label;
	gboolean done;
} AionSubTaskPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(AionSubTask, aion_sub_task, G_TYPE_OBJECT);

static gint instance_counter = 0;


static void
aion_sub_task_init(AionSubTask* sub_task)
{
	AionSubTaskPrivate* priv = aion_sub_task_get_instance_private(sub_task);
	priv->position = -1; // not yet in the subtask list of the task
	priv->label = NULL;
	priv->done = FALSE;
	instance_counter++;
}


static void
aion_sub_task_dispose(GObject* object)
{
	//AionSubTask* sub_task = AION_SUB_TASK(object);
	//AionSubTaskPrivate *priv = aion_sub_task_get_instance_private(sub_task);
	G_OBJECT_CLASS(aion_sub_task_parent_class)->dispose(object);
}


static void
aion_sub_task_finalize(GObject* object)
{
	AionSubTask* sub_task = AION_SUB_TASK(object);
	AionSubTaskPrivate *priv = aion_sub_task_get_instance_private(sub_task);
	if (priv->label != NULL)
		g_free(priv->label);
	G_OBJECT_CLASS(aion_sub_task_parent_class)->finalize(object);
	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_SUB_TASK));
}

 
static void
aion_sub_task_class_init(AionSubTaskClass* sub_task_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(sub_task_class);
	object_class->dispose= aion_sub_task_dispose;
	object_class->finalize = aion_sub_task_finalize;
}


AionSubTask*
aion_sub_task_new(const gchar* label)
{
	g_return_val_if_fail(label != NULL, NULL);
	AionSubTask* sub_task = AION_SUB_TASK(g_object_new(AION_TYPE_SUB_TASK, NULL));
	AionSubTaskPrivate *priv = aion_sub_task_get_instance_private(sub_task);
	priv->label = g_strdup(label);
	return sub_task;
}

const gchar*
aion_sub_task_get_label(AionSubTask* aion_sub_task)
{
	g_return_val_if_fail(AION_IS_SUB_TASK(aion_sub_task), NULL);
	AionSubTaskPrivate *priv = aion_sub_task_get_instance_private(aion_sub_task);
	return priv->label;
}

gboolean
aion_sub_task_set_label(AionSubTask* aion_sub_task, const gchar* new_label)
{
	g_return_val_if_fail(AION_IS_SUB_TASK(aion_sub_task), FALSE);
	g_return_val_if_fail(new_label != NULL, FALSE);
	AionSubTaskPrivate *priv = aion_sub_task_get_instance_private(aion_sub_task);
	gboolean changed = FALSE;

	// label length can't be zero
	gchar* stripped_label = g_strdup(new_label);
	g_strstrip((gchar*)stripped_label);
	if (stripped_label[0] == '\0')
		goto done;

	// do nothing if new label is the same
	if (priv->label != NULL && g_strcmp0(priv->label, stripped_label) == 0) 
		goto done;

	if (priv->label)
		g_free(priv->label);
	priv->label = g_strdup(stripped_label);
	changed = TRUE;
done:
	g_free(stripped_label);
	return changed;
}


gint
aion_sub_task_get_position(AionSubTask* aion_sub_task)
{
	g_return_val_if_fail(AION_IS_SUB_TASK(aion_sub_task), -1);
	AionSubTaskPrivate *priv = aion_sub_task_get_instance_private(aion_sub_task);
	return priv->position;
}

gboolean
aion_sub_task_private_set_position(AionSubTask* aion_sub_task, gint position)
{
	g_return_val_if_fail(AION_IS_SUB_TASK(aion_sub_task), FALSE);
	g_return_val_if_fail(position >= 0,  FALSE);
	AionSubTaskPrivate *priv = aion_sub_task_get_instance_private(aion_sub_task);
	if (priv->position == position)
		return FALSE;
	priv->position = position;
	return TRUE;
}


gboolean 
aion_sub_task_is_done(AionSubTask* aion_sub_task)
{
	g_return_val_if_fail(AION_IS_SUB_TASK(aion_sub_task), FALSE);
	AionSubTaskPrivate *priv = aion_sub_task_get_instance_private(aion_sub_task);
	return priv->done;
}

gboolean 
aion_sub_task_mark_as_done(AionSubTask* aion_sub_task, gboolean done)
{
	g_return_val_if_fail(AION_IS_SUB_TASK(aion_sub_task), FALSE);
	AionSubTaskPrivate *priv = aion_sub_task_get_instance_private(aion_sub_task);
	if (priv->done == done)
		return FALSE;
	priv->done = done;
	return TRUE;
}


gint
aion_sub_task_get_instance_count()
{
	return instance_counter;
}

gint
aion_sub_task_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_SUB_TASK));
	return instance_counter;;
}
