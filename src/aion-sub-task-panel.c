/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-SubTaskPanel"
#include <glib/gi18n.h>

#include "aion-sub-task-panel.h"
#include "aion-panelable.h"
#include "aion-panelable-private.h"
#include "aion-task-private.h"
#include "util.h"

struct _AionSubTaskPanel 
{
	GtkBox parent;
};

typedef struct {
        AionTask* task;
	GtkWidget* sub_task_list; // GtkTreeView
	GtkListStore* sub_task_store;
	GtkWidget* toolbar;
	GtkCellRendererText* label_column_renderer;
	GtkTreeViewColumn *label_column;
	gboolean data_changed;
} AionSubTaskPanelPrivate;

enum {
	MARK_AS_DONE_COLUMN,
	LABEL_COLUMN,
	SUB_TASK_OBJECT_COLUMN,
	NUMBER_OF_COLUMNS
};

// local prototypes
static void aion_sub_task_panel_panelable_interface_init(AionPanelableInterface*);
static void _aion_sub_task_panel_refresh(AionSubTaskPanel*);
void _mark_as_done_check_box_toggled (GtkCellRendererToggle*, char*, gpointer);
static void _add_sub_task_button_clicked (GtkButton*, gpointer);
static void _remove_sub_task_button_clicked (GtkButton*, gpointer);
static void _move_sub_task_up_button_clicked (GtkButton*, gpointer);
static void _move_sub_task_down_button_clicked (GtkButton*, gpointer);
void _subtask_label_edited(GtkCellRendererText*, char*, char*, gpointer);

G_DEFINE_TYPE_WITH_CODE (AionSubTaskPanel, aion_sub_task_panel, GTK_TYPE_BOX,
	G_ADD_PRIVATE(AionSubTaskPanel)
	G_IMPLEMENT_INTERFACE(AION_TYPE_PANELABLE, aion_sub_task_panel_panelable_interface_init)
);

static void
aion_sub_task_panel_init (AionSubTaskPanel* sub_task_panel)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkWidget* button;

	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(sub_task_panel);
	priv->task = NULL;
	priv->data_changed = FALSE;

	//==== edition toolbar

	GtkWidget* toolbar = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_widget_hide(toolbar);
	gtk_box_pack_start(GTK_BOX(sub_task_panel), toolbar, FALSE, FALSE, 5);
	priv->toolbar = toolbar;

	// add subtask button
	button = gtk_button_new_from_icon_name("list-add-symbolic", GTK_ICON_SIZE_SMALL_TOOLBAR); 
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(toolbar), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(_add_sub_task_button_clicked), sub_task_panel);

	// remove subtask button
	button = gtk_button_new_from_icon_name("list-remove-symbolic", GTK_ICON_SIZE_SMALL_TOOLBAR); 
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(toolbar), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(_remove_sub_task_button_clicked), sub_task_panel);

	// move subtask up button
	button = gtk_button_new_from_icon_name("go-up-symbolic", GTK_ICON_SIZE_SMALL_TOOLBAR); 
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(toolbar), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(_move_sub_task_up_button_clicked), sub_task_panel);

	// move subtask down button
	button = gtk_button_new_from_icon_name("go-down-symbolic", GTK_ICON_SIZE_SMALL_TOOLBAR); 
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(toolbar), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(_move_sub_task_down_button_clicked), sub_task_panel);


	//==== sub-task list

	GtkWidget *scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_show(scrolled_window);
	gtk_box_pack_start(GTK_BOX(sub_task_panel), scrolled_window, TRUE, TRUE, 5);

	priv->sub_task_store = gtk_list_store_new(
		NUMBER_OF_COLUMNS,
		G_TYPE_BOOLEAN,    // MARK_AS_DONE_COLUMN
		G_TYPE_STRING,     // LABEL_COLUMN
		G_TYPE_POINTER     // SUB_TASK_OBJECT_COLUMN
	);
	GtkWidget* tree_view = gtk_tree_view_new_with_model(GTK_TREE_MODEL(priv->sub_task_store));
	gtk_widget_show(tree_view);
	gtk_container_add(GTK_CONTAINER(scrolled_window), tree_view);
	priv->sub_task_list = tree_view;

	// column MARK_AS_DONE_COLUMN
	renderer = gtk_cell_renderer_toggle_new();
	column = gtk_tree_view_column_new_with_attributes (
					_("Done"),
					renderer,
					"active", MARK_AS_DONE_COLUMN,
					NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree_view), column);
	g_signal_connect(renderer, "toggled", G_CALLBACK(_mark_as_done_check_box_toggled), sub_task_panel);


	// column LABEL_COLUMN
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes (
					_("Subtask"),
					renderer,
					"text", LABEL_COLUMN,
					NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree_view), column);
	g_signal_connect(renderer, "edited", G_CALLBACK(_subtask_label_edited), sub_task_panel);
	priv->label_column_renderer = GTK_CELL_RENDERER_TEXT(renderer);
	priv->label_column = column;

	// no renderer for column SUB_TASK_OBJECT_COLUMN

	gtk_orientable_set_orientation(GTK_ORIENTABLE(sub_task_panel), GTK_ORIENTATION_VERTICAL);
	gtk_widget_show(GTK_WIDGET(sub_task_panel));
}

static void
aion_sub_task_panel_dispose(GObject* object)
{
	AionSubTaskPanel* panel = AION_SUB_TASK_PANEL(object);
	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(panel);
	if (priv->task) {
		g_object_unref(priv->task);
		priv->task = NULL;
	}
	G_OBJECT_CLASS(aion_sub_task_panel_parent_class)->dispose(object);
}
 
static void
aion_sub_task_panel_finalize(GObject* object)
{
	G_OBJECT_CLASS(aion_sub_task_panel_parent_class)->finalize(object);
}
 
static void
aion_sub_task_panel_class_init(AionSubTaskPanelClass* sub_task_panel_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(sub_task_panel_class);
	object_class->finalize = aion_sub_task_panel_finalize;
	object_class->dispose = aion_sub_task_panel_dispose;
}


static const gchar*
_aion_panelable_interface_get_name(AionPanelable* panel)
{
	return "sub-task-panel";
}


static const gchar*
_aion_panelable_interface_get_title(AionPanelable* panel)
{
	return _("Subtask list");
}

static const gchar*
_aion_panelable_interface_get_icon_name(AionPanelable* panel)
{
	return "view-list-symbolic"; // TODO use specific icon
}

static void
_aion_panelable_interface_set_task(AionPanelable* panel, AionTask* task)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	g_return_if_fail(task == NULL || AION_IS_TASK(task));
	AionSubTaskPanel* sub_task_panel = AION_SUB_TASK_PANEL(panel);
	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(sub_task_panel);
	if (priv->task)
		g_object_unref(priv->task);
	priv->task = task;
	if (priv->task)
		g_object_ref(priv->task);
	_aion_sub_task_panel_refresh(sub_task_panel);
}

static void
_aion_panelable_interface_refresh(AionPanelable* panel)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	_aion_sub_task_panel_refresh(AION_SUB_TASK_PANEL(panel));
}

static gboolean
_aion_panelable_interface_is_editable(AionPanelable* panel)
{
	return TRUE;
}

static gboolean
_aion_panelable_interface_needs_save(AionPanelable* panel)
{
	g_assert(AION_IS_PANELABLE(panel));
	AionSubTaskPanel* sub_task_panel = AION_SUB_TASK_PANEL(panel);
	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(sub_task_panel);
	return priv->data_changed;
}

static void
_aion_panelable_interface_start_editing(AionPanelable* panel)
{
	AionSubTaskPanel* sub_task_panel = AION_SUB_TASK_PANEL(panel);
	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(sub_task_panel);
	gtk_widget_show(priv->toolbar);
	g_object_set(G_OBJECT(priv->label_column_renderer), "editable", TRUE, NULL);
	_aion_panelable_private_emit_editing_started_signal(panel, priv->task);
}

static void
_aion_panelable_interface_end_editing(AionPanelable* panel, gboolean modification)
{
	AionSubTaskPanel* sub_task_panel = AION_SUB_TASK_PANEL(panel);
	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(sub_task_panel);

	gtk_widget_hide(priv->toolbar);
	g_object_set(G_OBJECT(priv->label_column_renderer), "editable", FALSE, NULL);

	GList* sub_task_list =  aion_task_get_sub_task_list(priv->task);
	GtkTreeIter iter;

	if (modification) {
		GList* remaining_sub_task_list =  g_list_copy(sub_task_list);
		gint index = 0;
		if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL(priv->sub_task_store), &iter)) {
			while (TRUE) {
				AionSubTask* sub_task = NULL;
				gtk_tree_model_get(GTK_TREE_MODEL(priv->sub_task_store), &iter, SUB_TASK_OBJECT_COLUMN, &sub_task, -1);
				g_assert(sub_task != NULL);
				g_assert(AION_IS_SUB_TASK(sub_task));

				// update subtask label
				const gchar* new_label = NULL;
				gtk_tree_model_get(GTK_TREE_MODEL(priv->sub_task_store), &iter, LABEL_COLUMN, &new_label, -1);
				g_assert(new_label != NULL);
				const gchar* current_label = aion_sub_task_get_label(sub_task);
				if (g_strcmp0(new_label, current_label)  != 0) 
					aion_sub_task_set_label(sub_task, new_label);

				// test if subtask is new (not in the subtask list of the task)
				GList* sub_task_item = g_list_find(remaining_sub_task_list, sub_task);
				if (sub_task_item == NULL) {
					aion_task_private_append_sub_task(priv->task, sub_task, index);
					g_object_unref(sub_task);
				}
				else {
					remaining_sub_task_list = g_list_delete_link(remaining_sub_task_list, sub_task_item);
					// update subtask position
					aion_task_set_sub_task_position(priv->task, sub_task, index);
				}

				if (!gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->sub_task_store), &iter))
					break;
				index++;
			}
		}
		// delete remaining subtasks (that where not found in the list)
		GList* sub_task_item = remaining_sub_task_list;
		while (sub_task_item) {
			AionSubTask* sub_task = AION_SUB_TASK(sub_task_item->data);
			aion_task_delete_sub_task(priv->task, sub_task);
			sub_task_item = sub_task_item->next;
		}
		g_list_free(remaining_sub_task_list);
	}
	else {
		// if cancel button was pressed, simply unref newly created subtasks to free memory
		if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL(priv->sub_task_store), &iter)) {
			while (TRUE) {
				AionSubTask* sub_task = NULL;
				gtk_tree_model_get(GTK_TREE_MODEL(priv->sub_task_store), &iter, SUB_TASK_OBJECT_COLUMN, &sub_task, -1);
				g_assert(sub_task != NULL);
				g_assert(AION_IS_SUB_TASK(sub_task));
				if (! g_list_find(sub_task_list, sub_task))
					g_object_unref(sub_task);
				if (!gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->sub_task_store), &iter))
					break;
			}
		}
	}
	g_list_free(sub_task_list);

	priv->data_changed = FALSE;
	_aion_sub_task_panel_refresh(sub_task_panel);
	_aion_panelable_private_emit_editing_finished_signal(panel, priv->task, modification);
}


static void
aion_sub_task_panel_panelable_interface_init(AionPanelableInterface* iface)
{
	iface->get_name = _aion_panelable_interface_get_name;
	iface->get_title = _aion_panelable_interface_get_title;
	iface->get_icon_name = _aion_panelable_interface_get_icon_name;
	iface->set_task = _aion_panelable_interface_set_task;
	iface->refresh = _aion_panelable_interface_refresh;
	iface->is_editable = _aion_panelable_interface_is_editable;
	iface->needs_save = _aion_panelable_interface_needs_save;
	iface->start_editing = _aion_panelable_interface_start_editing;
	iface->end_editing = _aion_panelable_interface_end_editing;
}

AionSubTaskPanel*
aion_sub_task_panel_new()
{
	return g_object_new(AION_TYPE_SUB_TASK_PANEL, NULL);
}

static void
_aion_sub_task_panel_refresh(AionSubTaskPanel* sub_task_panel)
{
	g_assert(AION_IS_SUB_TASK_PANEL(sub_task_panel));
	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(sub_task_panel);

	gtk_list_store_clear(priv->sub_task_store);
	if (priv->task == NULL)
		return;

	// TODO add a fake entry to show when sub-task list is empty
	GtkTreeIter iter; 
	AionSubTask* sub_task;
	GList* sub_task_list = aion_task_get_sub_task_list(priv->task);
	GList* sub_task_item = sub_task_list;
	while (sub_task_item) {
		sub_task = AION_SUB_TASK(sub_task_item->data);
		gtk_list_store_append (priv->sub_task_store, &iter);
		gtk_list_store_set (priv->sub_task_store, &iter,
			MARK_AS_DONE_COLUMN, aion_sub_task_is_done(sub_task),
			LABEL_COLUMN, aion_sub_task_get_label(sub_task),
			SUB_TASK_OBJECT_COLUMN, sub_task,
			-1);
		sub_task_item = sub_task_item->next;
	}
	g_list_free(sub_task_list);
	priv->data_changed = FALSE;
}

void
_mark_as_done_check_box_toggled (GtkCellRendererToggle *cell_renderer, char *path, gpointer data)
{
	gboolean done;
	AionSubTaskPanel* sub_task_panel = AION_SUB_TASK_PANEL(data);
	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(sub_task_panel);
	GtkListStore *model = GTK_LIST_STORE(priv->sub_task_store);
	GtkTreeIter iter;
	if (! gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(model), &iter, path))
		g_error("Can't find row");
	// toogle checkbox
	gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, MARK_AS_DONE_COLUMN, &done, -1);
	done = !done;
	gtk_list_store_set(model, &iter, MARK_AS_DONE_COLUMN, done, -1);    
	AionSubTask* sub_task = NULL;
	gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, SUB_TASK_OBJECT_COLUMN, &sub_task, -1);
	g_assert(sub_task != NULL);
	g_assert(AION_IS_SUB_TASK(sub_task));
	aion_sub_task_mark_as_done(sub_task, done);
}

void
_subtask_label_edited(GtkCellRendererText* renderer, char* path, char* new_label, gpointer data)
{
	AionSubTaskPanel* sub_task_panel = AION_SUB_TASK_PANEL(data);
	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(sub_task_panel);
	GtkListStore *model = GTK_LIST_STORE(priv->sub_task_store);
	GtkTreeIter iter;
	if (! gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(model), &iter, path))
		g_error("Can't find row");

	gtk_list_store_set(model, &iter, LABEL_COLUMN, new_label, -1);    
	priv->data_changed = TRUE;
}

static void
_add_sub_task_button_clicked (GtkButton* button, gpointer data)
{
	AionSubTaskPanel* sub_task_panel = AION_SUB_TASK_PANEL(data);
	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(sub_task_panel);

	GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(priv->sub_task_list));
	GtkTreeIter iter;
	GtkTreeIter iter_new;
	gint line_count = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(priv->sub_task_store), NULL);
	if (line_count == 0) {
		gtk_list_store_append (priv->sub_task_store, &iter_new);
	}
	else {
		if (gtk_tree_selection_get_selected(selection, NULL, &iter)) {
			gtk_list_store_insert_after(priv->sub_task_store, &iter_new, &iter);
		}
		else {
			gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(priv->sub_task_store), &iter, NULL, line_count - 1);
			gtk_list_store_insert_after(priv->sub_task_store, &iter_new, &iter);
		}
	}

	AionSubTask* new_sub_task = aion_sub_task_new(_("New subtask"));
	gtk_list_store_set (priv->sub_task_store, &iter_new,
			MARK_AS_DONE_COLUMN, aion_sub_task_is_done(new_sub_task),
			LABEL_COLUMN, aion_sub_task_get_label(new_sub_task),
			SUB_TASK_OBJECT_COLUMN, new_sub_task,
			-1);

	// select new row and make it visible
	gtk_tree_selection_select_iter(selection, &iter_new);
	GtkTreePath *path = gtk_tree_model_get_path (GTK_TREE_MODEL(priv->sub_task_store), &iter_new);
	gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW(priv->sub_task_list), path, NULL, TRUE, 0.5, 0.5);
	gtk_tree_path_free(path);
	priv->data_changed = TRUE;
}

static void
_remove_sub_task_button_clicked (GtkButton* button, gpointer data)
{
	AionSubTaskPanel* sub_task_panel = AION_SUB_TASK_PANEL(data);
	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(sub_task_panel);

	gint line_count = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(priv->sub_task_store), NULL);
	if (line_count == 0) {
		g_warning("Subtask list is empty"); // TODO display a notification
		return;
	}
	GtkTreeIter iter;
	GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(priv->sub_task_list));
	if (! gtk_tree_selection_get_selected(selection, NULL, &iter)) {
		g_warning("No subtask selected"); // TODO display a notification
		return;
	}

	gtk_list_store_remove(priv->sub_task_store, &iter);
	priv->data_changed = TRUE;
}


static void
_move_sub_task_in_list(AionSubTaskPanel* sub_task_panel, gint delta)
{
	AionSubTaskPanelPrivate* priv = aion_sub_task_panel_get_instance_private(sub_task_panel);

	gint line_count = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(priv->sub_task_store), NULL);
	if (line_count == 0) {
		g_warning("Subtask list is empty"); // TODO display a notification
		return;
	}
	GtkTreeIter iter;
	GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(priv->sub_task_list));
	if (! gtk_tree_selection_get_selected(selection, NULL, &iter)) {
		g_warning("No subtask selected"); // TODO display a notification
		return;
	}

	GtkTreeIter iter_before = iter;
	if (delta < 0) {
		if (! gtk_tree_model_iter_previous(GTK_TREE_MODEL(priv->sub_task_store), &iter_before)) {
			g_warning("Already at the start of the list"); // TODO display a notification
			return;
		}
	}
	else {
		if (! gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->sub_task_store), &iter_before)) {
			g_warning("Already at the end of the list"); // TODO display a notification
			return;
		}
	}
	gtk_list_store_swap(priv->sub_task_store, &iter, &iter_before);
	priv->data_changed = TRUE;
}

static void
_move_sub_task_up_button_clicked (GtkButton* button, gpointer data)
{
	AionSubTaskPanel* sub_task_panel = AION_SUB_TASK_PANEL(data);
	_move_sub_task_in_list(sub_task_panel, -1);
}

static void
_move_sub_task_down_button_clicked (GtkButton* button, gpointer data)
{
	AionSubTaskPanel* sub_task_panel = AION_SUB_TASK_PANEL(data);
	_move_sub_task_in_list(sub_task_panel, 1);
}

// TODO add instance_counter
