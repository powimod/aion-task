/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_TASK_PRIVATE__
#define __AION_TASK_PRIVATE__

G_BEGIN_DECLS

AionTaskLogEntry* aion_task_private_get_log_entry(AionTask*, gint);
void aion_task_private_set_total_duration(AionTask*, gint);
void aion_task_private_set_execution_count(AionTask*, gint);
void aion_task_private_resize_log_entries(AionTask*, gint);
void aion_task_private_add_log_entry(AionTask*, const GDate*, gint);
void aion_task_private_set_log_entry_duration(AionTask*, gint, gint);
void aion_task_private_append_sub_task(AionTask* task, AionSubTask* aion_sub_task, gint);

G_END_DECLS

#endif /*__AION_TASK_PRIVATE__*/
