/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __AION_TASK_CREATION_ASSISTANT__
#define __AION_TASK_CREATION_ASSISTANT__

#include "aion-task.h"
#include "aion-main-window.h"
#include <glib-object.h>
#include <gtk/gtk.h>

#define AION_TYPE_TASK_CREATION_ASSISTANT aion_task_creation_assistant_get_type()

G_DECLARE_FINAL_TYPE (AionTaskCreationAssistant, aion_task_creation_assistant, AION, TASK_CREATION_ASSISTANT, GtkAssistant);

AionTaskCreationAssistant* aion_task_creation_assistant_new(AionMainWindow*);

gint aion_task_creation_assistant_get_instance_count();
gint aion_task_creation_assistant_check_zero_instance();

#endif /*__AION_TASK_CREATION_ASSISTANT__*/

