/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-MainWindow"
#include <glib/gi18n.h>

#include "aion-task-config.h"
#include "aion-main-window.h"
#include "aion-task-creation-assistant.h"
#include "aion-task-validation-dialog.h"
#include "aion-task-postpone-dialog.h"
#include "aion-notification.h"
#include "aion-application.h"
#include "aion-task-list-row.h"
#include "aion-panel-container.h"
#include "aion-task-general-panel.h"
#include "aion-task-properties-panel.h"
#include "aion-task-note-panel.h"
#include "aion-task-log-panel.h"
#include "aion-sub-task-panel.h"
#include "aion_task-resources.h"
#include "util.h"


#define PROG_NAME "Aion Task Manager"
#define AION_MAIN_WINDOW_NAME "org.gnome.gitlab.powimod.aion-task"

// Librem5 : 
// - screen resolution : 360 x 720 px
// - toolbars height : top=26 px;  bottom=40px
// - user size landscape : 720 x 288 px
// - user size portrait  : 360 x 648 px

#define LANDSCAPE_MODE
//#define PORTRAIT_MODE

#ifdef LANDSCAPE_MODE
	#define DEFAULT_WINDOW_WIDTH 640
	#define DEFAULT_WINDOW_HEIGHT 480 
#else
	#ifdef PORTRAIT_MODE
		#define DEFAULT_WINDOW_WIDTH 360
		#define DEFAULT_WINDOW_HEIGHT 648 
	#else
		#define DEFAULT_WINDOW_WIDTH 640
		#define DEFAULT_WINDOW_HEIGHT 480 
	#endif
#endif

// TODO used ?
#define TASK_LIST_MIN_WIDTH 100
#define TASK_LIST_MAX_WIDTH 350
#define TASK_PANEL_MIN_WIDTH 330
#define TASK_PANEL_MAX_WIDTH 400

#define ACTION_NAME_CREATE_NEW_TASK "create-new-task"
#define ACTION_NAME_VALIDATE_CURRENT_TASK "validate-current-task"
#define ACTION_NAME_POSTPONE_CURRENT_TASK "postpone-current-task"
#define ACTION_NAME_DELETE_CURRENT_TASK "delete-current-task"
#define ACTION_NAME_FILTER_PENDING_TASKS "filter-pending-tasks"
#define ACTION_NAME_SHOW_SEARCH_BAR "show-search-bar"
#define ACTION_NAME_HIDE_INFO_PANE "hide-task-info-pane"
#define ACTION_NAME_QUIT "quit"
#define ACTION_NAME_ABOUT "about"
#ifdef USER_HELP
#define ACTION_NAME_HELP "help"
#endif
#define ACTION_NAME_CANCEL_TASK_DELETE "cancel-delete"

struct _AionMainWindow {
	GtkApplicationWindow parent_instance;
};

typedef struct {
	// two leafs in leaflet [task_main_page]
	GtkWidget* main_leaflet;
	GtkWidget* task_list_leaf;
	GtkWidget* task_details_leaf; // TODO rename [task_pannels_leaf]

	AionPanelContainer* panel_container_leaf;
	AionPanelable* task_general_panel;
	AionPanelable* task_properties_panel;
	AionPanelable* sub_task_panel;
	AionPanelable* task_log_panel;

	GtkListBox* task_list_box;
	GtkWidget* notification_stack;
	GtkWidget* search_bar;
	GtkWidget* hide_details_button;

	AionTask* selected_task;
	AionTask* deleted_task; // to undo task deletion

	gboolean filter_due_date_tasks;
	gint filter_limit_days_count;
	GDate* filter_limit_date;
	guint filter_timer_id;
	gchar* text_searched;
} AionMainWindowPrivate;


G_DEFINE_TYPE_WITH_PRIVATE (AionMainWindow, aion_main_window, HDY_TYPE_APPLICATION_WINDOW)

static gint instance_counter = 0;

// local prototypes
static void aion_main_window_dispose(GObject*);
static void aion_main_window_finalize(GObject*);
static GtkListBoxRow* _task_list_add_row (AionTask*, gpointer);
static void _task_list_row_destroyed (GtkButton*, gpointer);
static void _task_list_row_selected (GtkListBox*, GtkListBoxRow*, gpointer);
static void _task_list_row_activated(GtkListBox*, GtkListBoxRow* , gpointer);
static void _search_entry_changed (GtkSearchEntry*, gpointer);
static void _search_entry_stop (GtkSearchEntry*, gpointer);
static void _select_last_row(GtkListBox*);
static void update_header_bar (AionMainWindow* main_window);
static void leaflet_folded_changed (HdyLeaflet*, GParamSpec*, gpointer);
static void window_show_event(GtkWidget*, gpointer);
static void _action_create_new_task (GSimpleAction*, GVariant*, gpointer);
static void _action_validate_current_task (GSimpleAction*, GVariant*, gpointer);
static void _action_postpone_current_task (GSimpleAction*, GVariant*, gpointer);
static void _action_delete_current_task (GSimpleAction*, GVariant*, gpointer);
static void _action_cancel_task_delete (GSimpleAction*, GVariant*, gpointer);
static void _action_filter_pending_tasks (GSimpleAction*, GVariant*, gpointer);
static void _action_hide_info_pane (GSimpleAction*, GVariant*, gpointer);
static void _action_show_search_bar (GSimpleAction*, GVariant*, gpointer);
static void _action_about (GSimpleAction*, GVariant*, gpointer);
static void _action_quit (GSimpleAction*, GVariant*, gpointer);
#ifdef USER_HELP
static void _action_help (GSimpleAction*, GVariant*, gpointer);
#endif
static void _schedule_message_display_when_list_empty(AionMainWindow*);
static void _refresh_selected_row(AionMainWindow*);
static void _panel_editing_finished(AionPanelContainer*, AionPanelable*, AionTask*, gboolean, gpointer);
static gboolean _panel_save_needed(AionPanelContainer*, AionPanelable*, AionTask*, gpointer);
static void _filter_day_count_changed(GtkSpinButton*, gpointer);
static void _task_validation_dialog_closed(AionTaskValidationDialog*, gboolean, gpointer);
static void _task_creation_assistant_closed(AionTaskValidationDialog*, AionTask*, gpointer);
static void _task_postpone_dialog_closed(AionTaskValidationDialog*, gboolean, gpointer);
typedef struct {
	AionMainWindow* main_window;
	AionTaskListRow* task_list_row;
	AionTask* task; // FIXME used ? 
	GtkListBoxRow* list_box_row; 
}_TaskListRowData;


static void
_action_create_new_task (GSimpleAction* action, GVariant* parameter, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	g_assert(AION_IS_MAIN_WINDOW(main_window));
	AionTaskCreationAssistant* assistant = aion_task_creation_assistant_new(main_window);
	g_signal_connect(assistant, "closed", G_CALLBACK(_task_creation_assistant_closed), main_window);

}

static void
_task_creation_assistant_closed (AionTaskValidationDialog* task_validation_dialog, AionTask* new_task, gpointer data)
{
	if (new_task == NULL) {
		g_debug("Task creation canceled\n");
		return;
	}
	g_debug("New task : %s", aion_task_get_label(new_task));
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	AionApplication* aion_application = aion_main_window_get_application(main_window);
	AionTaskManager* task_manager = aion_application_get_task_manager(aion_application);
	aion_task_manager_append_task(task_manager, new_task);
	GtkListBoxRow* new_row =_task_list_add_row (new_task, main_window);
	gtk_list_box_select_row(priv->task_list_box, new_row);
	hdy_leaflet_set_visible_child(HDY_LEAFLET(priv->main_leaflet), priv->task_list_leaf);
}

static void
_task_validation_dialog_closed(AionTaskValidationDialog* task_validation_dialog, gboolean validation, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	g_assert(AION_IS_MAIN_WINDOW(main_window));
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);

	if (! validation) 
		return;

	// refresh task list and task's panels
	GtkListBoxRow* selected_row =  gtk_list_box_get_selected_row (priv->task_list_box);
	AionTaskListRow* task_list_row = AION_TASK_LIST_ROW(g_object_get_data(G_OBJECT(selected_row), "task-list-row"));
	aion_task_list_row_refresh(task_list_row);
	gtk_list_box_invalidate_filter(priv->task_list_box);
	aion_panel_container_refresh(priv->panel_container_leaf);

	// display a notification with next execution date	
	gchar* message = NULL;
	if (aion_task_is_enabled(priv->selected_task)) {
		const GDate* next_execution_date = aion_task_get_next_execution_date(priv->selected_task);
		g_assert(g_date_valid(next_execution_date));
		gchar* next_execution_str = aion_util_get_date_local_representation(next_execution_date);
		message = g_strdup_printf("Next execution date is %s", next_execution_str);
		g_free(next_execution_str);
	}
	else {
		message = g_strdup_printf(_("Task is now disabled (repeat once)"));
	}
	aion_main_window_display_notification(main_window, message);
	g_free(message);
}

static void
_action_validate_current_task (GSimpleAction* action, GVariant* parameter, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	g_assert(AION_IS_MAIN_WINDOW(main_window));
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	g_assert(priv->selected_task != NULL);

	AionTask* selected_task = priv->selected_task;
	if (selected_task == NULL){
		g_warning("No task selected for validation");
		aion_main_window_display_notification(main_window, _("No task selected"));
		return;
	}

	// when task is disabled, inform user it can't be postponed
	if (aion_task_get_state(selected_task) == AION_TASK_STATE_DISABLED) {
		aion_main_window_display_notification(main_window, _("Can't validate a task which is disabled")); 
		return;
	}

	AionTaskValidationDialog* task_validation_dialog = aion_task_validation_dialog_new(priv->selected_task, main_window);
	g_signal_connect(task_validation_dialog, "closed", G_CALLBACK(_task_validation_dialog_closed), main_window);
}

static void
_task_postpone_dialog_closed (AionTaskValidationDialog* task_validation_dialog, gboolean validation, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	g_assert(AION_IS_MAIN_WINDOW(main_window));
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);

	if (! validation) 
		return;

	GtkListBoxRow* selected_row =  gtk_list_box_get_selected_row (priv->task_list_box);
	AionTaskListRow* task_list_row = AION_TASK_LIST_ROW(g_object_get_data(G_OBJECT(selected_row), "task-list-row"));
	aion_task_list_row_refresh(task_list_row);
	gtk_list_box_invalidate_filter(priv->task_list_box);
	aion_panel_container_refresh(priv->panel_container_leaf);

	gchar* message = g_strdup_printf(_("New postponement delay : %i day(s)"), 
				aion_task_get_postponement_delay(priv->selected_task));
	aion_main_window_display_notification(main_window, message);
	g_free(message);

}


static void
_action_postpone_current_task (GSimpleAction* action, GVariant* parameter, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	g_assert(AION_IS_MAIN_WINDOW(main_window));
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	g_assert(priv->selected_task != NULL);

	AionTask* selected_task = priv->selected_task;
	if (selected_task == NULL){
		g_warning("No task selected for postponement");
		aion_main_window_display_notification(main_window, _("No task selected"));
		return;
	}

	// when task is disabled, inform user it can't be postponed
	if (aion_task_get_state(selected_task) == AION_TASK_STATE_DISABLED) {
		aion_main_window_display_notification(main_window, _("Can't postpone a task which is disabled"));
		return;
	}

	AionTaskPostponeDialog* task_postpone_dialog = aion_task_postpone_dialog_new(priv->selected_task, main_window);
	g_signal_connect(task_postpone_dialog, "closed", G_CALLBACK(_task_postpone_dialog_closed), main_window);
}


static void
_action_delete_current_task (GSimpleAction* action, GVariant* parameter, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	g_assert(AION_IS_MAIN_WINDOW(main_window));
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	g_assert(priv->selected_task != NULL);

	AionTask* selected_task = priv->selected_task;
	if (selected_task == NULL){
		g_warning("No task selected for deletion");
		aion_main_window_display_notification(main_window, _("No task selected"));
		return;
	}

	if (priv->deleted_task)
		g_object_unref(priv->deleted_task);

	// keep a handle on the deleted task to restore it if Cancel button is pressed
	priv->deleted_task = selected_task;
	g_object_ref(priv->deleted_task);

	// TODO use an Action
	AionApplication* aion_application = aion_main_window_get_application(main_window);
	AionTaskManager* task_manager = aion_application_get_task_manager(aion_application);
	aion_task_manager_remove_task(task_manager , selected_task);

	GtkListBoxRow* selected_row =  gtk_list_box_get_selected_row (priv->task_list_box);
	gtk_container_remove(GTK_CONTAINER(priv->task_list_box), GTK_WIDGET(selected_row));
	gtk_list_box_invalidate_filter(priv->task_list_box);
	// task panels are automatically refreshed by «row-selected» event (with row set to NULL)
	// hide task details leaf
	hdy_leaflet_set_visible_child(HDY_LEAFLET(priv->main_leaflet), priv->task_list_leaf);
	aion_panel_container_set_task(priv->panel_container_leaf, NULL);

	AionNotification* notif = aion_main_window_display_notification(main_window, _("Task deleted"));
	GAction *cancel_action = g_action_map_lookup_action(G_ACTION_MAP(main_window), ACTION_NAME_CANCEL_TASK_DELETE);
	g_assert(G_IS_ACTION(cancel_action));
	aion_notification_set_action(notif, "Cancel", cancel_action);
}

static gboolean
_task_filter_function (GtkListBoxRow* row, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	_schedule_message_display_when_list_empty(main_window);
	gpointer task_list_row_pointer =  g_object_get_data(G_OBJECT(row), "task-list-row");
	if (task_list_row_pointer == NULL) 
		return TRUE;
	AionTaskListRow* task_list_row = AION_TASK_LIST_ROW(task_list_row_pointer);
	AionTask* task = (AionTask*)aion_task_list_row_get_task(task_list_row);
	AionTaskState state;
	if (priv->filter_limit_days_count == 0)
		state = aion_task_get_state(task);
	else
		state = aion_task_evaluate_state_on_date_of(task, priv->filter_limit_date);
	gboolean row_visible = TRUE;
	if (priv->filter_due_date_tasks)
		row_visible = (state == AION_TASK_STATE_PREVIEW || state == AION_TASK_STATE_ON_TIME || state == AION_TASK_STATE_OVERDUE);
	if (row_visible && priv->text_searched[0] != '\0') {
		gchar* str = g_utf8_casefold(aion_task_get_label(task), -1);
		row_visible = (g_strrstr(str, priv->text_searched) != NULL);
		g_free(str);
	}
	return row_visible;
}

static gint _task_sort_function (GtkListBoxRow *row1, GtkListBoxRow *row2, gpointer user_data)
{
	gpointer task_list_row1_pointer =  g_object_get_data(G_OBJECT(row1), "task-list-row");
	if (task_list_row1_pointer == NULL) 
		return 0;
	gpointer task_list_row2_pointer =  g_object_get_data(G_OBJECT(row2), "task-list-row");
	if (task_list_row2_pointer == NULL) 
		return 0;
	AionTaskListRow* task_list_row1 = AION_TASK_LIST_ROW(task_list_row1_pointer);
	AionTask* task1 = (AionTask*)aion_task_list_row_get_task(task_list_row1);
	AionTaskListRow* task_list_row2 = AION_TASK_LIST_ROW(task_list_row2_pointer);
	AionTask* task2 = (AionTask*)aion_task_list_row_get_task(task_list_row2);
	return aion_task_get_priority(task1) < aion_task_get_priority(task2);
}

static void
_action_filter_pending_tasks (GSimpleAction* action, GVariant* parameter, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	gboolean filtering = g_variant_get_boolean(g_action_get_state(G_ACTION(action))); 
	filtering = !filtering; // inverse state
	g_action_change_state(G_ACTION(action), g_variant_new_boolean(filtering));
	priv->filter_due_date_tasks = filtering;
	gtk_list_box_invalidate_filter(GTK_LIST_BOX(priv->task_list_box));
}

static void
_action_hide_info_pane (GSimpleAction* action, GVariant* parameter, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	hdy_leaflet_set_visible_child(HDY_LEAFLET(priv->main_leaflet), priv->task_list_leaf);
}


static void
_action_show_search_bar (GSimpleAction* action, GVariant* parameter, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	gboolean show_bar = g_variant_get_boolean(g_action_get_state(G_ACTION(action))); 
	show_bar = !show_bar; // inverse state
	g_action_change_state(G_ACTION(action), g_variant_new_boolean(show_bar));
	gtk_search_bar_set_search_mode(GTK_SEARCH_BAR(priv->search_bar), show_bar);
	// TODO clean previous texst searched when search bar is shown
}

static void
_action_about (GSimpleAction* action, GVariant* parameter, gpointer main_window)
{
	// TODO use structure instead of function calls
	GtkWidget* dialog = gtk_about_dialog_new();
	gtk_window_set_transient_for (GTK_WINDOW(dialog), GTK_WINDOW(main_window));
	gtk_window_set_modal (GTK_WINDOW(dialog), TRUE);
	gtk_window_set_destroy_with_parent (GTK_WINDOW(dialog), TRUE);
	gtk_about_dialog_set_program_name (GTK_ABOUT_DIALOG(dialog), PROG_NAME);
	gtk_about_dialog_set_version (GTK_ABOUT_DIALOG(dialog), VERSION);
	gtk_about_dialog_set_comments (GTK_ABOUT_DIALOG(dialog), "A simple cyclic task manager");
	const gchar *authors[2] = { "Dominique Parisot", NULL };
	gtk_about_dialog_set_authors (GTK_ABOUT_DIALOG(dialog), authors);
	gtk_about_dialog_set_logo_icon_name (GTK_ABOUT_DIALOG(dialog), "aion-task-icon");
	gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(dialog), "https://gitlab.gnome.org/powimod/aion-task/-/wikis/Aion-Task-Manager-home-page");
	gtk_about_dialog_set_translator_credits (GTK_ABOUT_DIALOG(dialog), _("translator-credits"));
	// FIXME g_signal_connect(dialog, "response", G_CALLBACK(gtk_widget_destroy),  NULL);
	gtk_widget_show (GTK_WIDGET(dialog));
}
 
static void
_action_quit (GSimpleAction* action, GVariant* parameter, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	gtk_window_close(GTK_WINDOW(main_window));
}

#ifdef USER_HELP
static void
_action_help (GSimpleAction* action, GVariant* parameter, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	GError* error = NULL;
	const gchar* name_of_user_manual = "aion-task";
	gchar* help_uri = g_strdup_printf ("help:%s", name_of_user_manual);
	gtk_show_uri_on_window (GTK_WINDOW(main_window), help_uri, GDK_CURRENT_TIME, &error);
	if (error != NULL) {
		gchar* message = g_strdup_printf("Can't show help : %s", error->message);
		g_error_free(error);
		aion_main_window_display_notification(main_window, message);
		g_warning(message);
		g_free(message);
	}
	g_free(help_uri);
}
#endif

void
aion_main_window_declare_actions(AionMainWindow* main_window)
{
	GSimpleAction* simple_action;
	GtkApplication* application = gtk_window_get_application(GTK_WINDOW(main_window));
	g_assert(application != NULL); // FIXME application is null when called from aion_main_window_init

	//=== action [create-new-task]
	simple_action = g_simple_action_new(ACTION_NAME_CREATE_NEW_TASK, NULL);
	g_signal_connect(G_OBJECT(simple_action), "activate", G_CALLBACK(_action_create_new_task), main_window);
	g_action_map_add_action (G_ACTION_MAP(main_window), G_ACTION(simple_action));
	const gchar *accels_1[2] = { "<Ctrl>N", NULL };
	gtk_application_set_accels_for_action(application, "win." ACTION_NAME_CREATE_NEW_TASK, accels_1);

	//=== action [validate-current-task]
	simple_action = g_simple_action_new(ACTION_NAME_VALIDATE_CURRENT_TASK, NULL);
	g_signal_connect(G_OBJECT(simple_action), "activate", G_CALLBACK(_action_validate_current_task), main_window);
	g_action_map_add_action (G_ACTION_MAP(main_window), G_ACTION(simple_action));

	//=== action [validate-current-task]
	simple_action = g_simple_action_new(ACTION_NAME_POSTPONE_CURRENT_TASK, NULL);
	g_signal_connect(G_OBJECT(simple_action), "activate", G_CALLBACK(_action_postpone_current_task), main_window);
	g_action_map_add_action (G_ACTION_MAP(main_window), G_ACTION(simple_action));
	
	//=== action [delete-current-task]
	simple_action = g_simple_action_new(ACTION_NAME_DELETE_CURRENT_TASK, NULL);
	g_signal_connect(G_OBJECT(simple_action), "activate", G_CALLBACK(_action_delete_current_task), main_window);
	g_action_map_add_action (G_ACTION_MAP(main_window), G_ACTION(simple_action));
	
	//=== action [filter-pending-tasks]
	GVariant* state = g_variant_new_boolean(TRUE);
	simple_action = g_simple_action_new_stateful(ACTION_NAME_FILTER_PENDING_TASKS , NULL, state);
	g_signal_connect(G_OBJECT(simple_action), "activate", G_CALLBACK(_action_filter_pending_tasks), main_window);
	g_action_map_add_action (G_ACTION_MAP(main_window), G_ACTION(simple_action));
	const gchar *accels_2[2] = { "F2", NULL };
	gtk_application_set_accels_for_action(application, "win." ACTION_NAME_FILTER_PENDING_TASKS, accels_2);

	//=== action [show_search_bar]
	state = g_variant_new_boolean(FALSE);
	simple_action = g_simple_action_new_stateful(ACTION_NAME_SHOW_SEARCH_BAR, NULL, state);
	g_signal_connect(G_OBJECT(simple_action), "activate", G_CALLBACK(_action_show_search_bar), main_window);
	g_action_map_add_action (G_ACTION_MAP(main_window), G_ACTION(simple_action));
	const gchar *accels_3[2] = { "<Ctrl>F", NULL };
	gtk_application_set_accels_for_action(application, "win." ACTION_NAME_SHOW_SEARCH_BAR, accels_3);

	//=== action [hide-task-info-pane]
	simple_action = g_simple_action_new(ACTION_NAME_HIDE_INFO_PANE, NULL);
	g_signal_connect(G_OBJECT(simple_action), "activate", G_CALLBACK(_action_hide_info_pane), main_window);
	g_action_map_add_action (G_ACTION_MAP(main_window), G_ACTION(simple_action));
	const gchar *accels_4[2] = { "Escape", NULL };
	gtk_application_set_accels_for_action(application, "win." ACTION_NAME_HIDE_INFO_PANE, accels_4);

	// action [Quit]
	simple_action = g_simple_action_new(ACTION_NAME_QUIT, NULL);
	g_signal_connect(G_OBJECT(simple_action), "activate", G_CALLBACK(_action_quit), main_window);
	g_action_map_add_action (G_ACTION_MAP(main_window), G_ACTION(simple_action));
	const gchar *accels_5[2] = { "<Ctrl>Q", NULL };
	gtk_application_set_accels_for_action(application, "win." ACTION_NAME_QUIT, accels_5);

	// action [about]
	simple_action = g_simple_action_new(ACTION_NAME_ABOUT, NULL);
	g_signal_connect(G_OBJECT(simple_action), "activate", G_CALLBACK(_action_about), main_window);
	g_action_map_add_action (G_ACTION_MAP(main_window), G_ACTION(simple_action));

#ifdef USER_HELP
	// action [help]
	simple_action = g_simple_action_new(ACTION_NAME_HELP, NULL);
	g_signal_connect(G_OBJECT(simple_action), "activate", G_CALLBACK(_action_help), main_window);
	g_action_map_add_action (G_ACTION_MAP(main_window), G_ACTION(simple_action));
#endif

	// action [cancel-delete]
	simple_action = g_simple_action_new(ACTION_NAME_CANCEL_TASK_DELETE, NULL);
	g_signal_connect(G_OBJECT(simple_action), "activate", G_CALLBACK(_action_cancel_task_delete), main_window);
	g_action_map_add_action (G_ACTION_MAP(main_window), G_ACTION(simple_action));
}


static void
aion_main_window_init(AionMainWindow* main_window)
{
	g_assert(AION_IS_MAIN_WINDOW(main_window));
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	priv->selected_task = NULL;
	priv->deleted_task = NULL;
	priv->filter_due_date_tasks = TRUE;
	priv->filter_limit_date = aion_util_get_today_date();
	priv->filter_timer_id = 0;
	priv->text_searched = g_strdup("");

	gtk_window_set_default_size(GTK_WINDOW(main_window), DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);

	/* How to use icon resources
	   #define AION_APPLICATION_ID "org.gnome.aion-task"

	   if GApplication ID = [org.gnome.aion-task] 
	   then already defined : [org/gnome/aion-task/icons]

	   Add at begining of the file :
		#include "aion_task-resources.h"

	   How to use :
		GtkWidget* img = gtk_image_new_from_icon_name("aion-task-icon.svg", GTK_ICON_SIZE_INVALID);
		GtkWidget* img = gtk_image_new_from_resource("/org/gnome/aion-task/icons/aion-task-icon.svg");

	   See : src/aion_task.gresources.xml
	*/


	g_resources_register (aion_task_get_resource());
	GtkIconTheme* icon_theme = gtk_icon_theme_get_default();
	gtk_icon_theme_add_resource_path (icon_theme, "/org/gnome/aion-task/icons");

	// notification stack
	GtkWidget* overlay = gtk_overlay_new(); // to display notifications on top
	gtk_widget_show(overlay);
	gtk_container_add(GTK_CONTAINER(main_window), overlay);
	GtkWidget* v_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	gtk_widget_show(v_box);
	gtk_widget_set_margin_top(v_box, 50);// FIXME hard coded (margin to display notification below title bar)
	gtk_overlay_add_overlay(GTK_OVERLAY(overlay), v_box);
	priv->notification_stack = v_box;
	gtk_overlay_reorder_overlay(GTK_OVERLAY(overlay), priv->notification_stack, 1);
	gtk_overlay_set_overlay_pass_through(GTK_OVERLAY(overlay), priv->notification_stack, TRUE);

	// main leaflet (with task list leaf and panel container leaf)
	GtkWidget* leaflet = hdy_leaflet_new();
	gtk_widget_show(leaflet);
	gtk_orientable_set_orientation(GTK_ORIENTABLE(leaflet), GTK_ORIENTATION_HORIZONTAL);
	g_signal_connect(G_OBJECT(leaflet), "notify::folded", G_CALLBACK(leaflet_folded_changed), main_window);
	priv->main_leaflet = leaflet;

	gtk_overlay_add_overlay(GTK_OVERLAY(overlay), GTK_WIDGET(priv->main_leaflet));
	gtk_overlay_reorder_overlay(GTK_OVERLAY(overlay), GTK_WIDGET(priv->main_leaflet), 0);

	//== Leaf with task list

	GtkBuilder* builder = gtk_builder_new_from_resource("/org/gnome/aion-task/ui/task-list-leaf");
	g_assert(builder != NULL);

	priv->task_list_leaf = GTK_WIDGET(gtk_builder_get_object(builder, "task-list-leaf"));
	//gtk_widget_set_size_request(GTK_WIDGET(priv->task_list_leaf), TASK_LIST_MIN_WIDTH, -1); 
	g_assert(priv->task_list_leaf);
	gtk_container_add (GTK_CONTAINER(priv->main_leaflet), priv->task_list_leaf);

	priv->search_bar = GTK_WIDGET(gtk_builder_get_object(builder, "task-search-bar"));
	g_assert(priv->search_bar);

	GtkWidget* search_entry = GTK_WIDGET(gtk_builder_get_object(builder, "task-search-entry"));
	g_assert(search_entry);
	g_signal_connect(G_OBJECT(search_entry), "search-changed", G_CALLBACK(_search_entry_changed), main_window);
	g_signal_connect(G_OBJECT(search_entry), "stop-search", G_CALLBACK(_search_entry_stop), main_window);

	priv->task_list_box = GTK_LIST_BOX(gtk_builder_get_object(builder, "task-list-box"));
	g_assert(priv->task_list_box);
	gtk_list_box_set_filter_func (GTK_LIST_BOX(priv->task_list_box), _task_filter_function, main_window, NULL);
	gtk_list_box_set_sort_func (GTK_LIST_BOX(priv->task_list_box), _task_sort_function, main_window, NULL);
	g_signal_connect(priv->task_list_box, "row-selected", G_CALLBACK(_task_list_row_selected), main_window);
	g_signal_connect(priv->task_list_box, "row-activated", G_CALLBACK(_task_list_row_activated), main_window);

	GtkWidget* button = GTK_WIDGET(gtk_builder_get_object(builder, "filter-day-count"));
	g_assert(button != NULL);
	g_signal_connect(button, "value-changed", G_CALLBACK(_filter_day_count_changed), main_window);

	g_object_unref(builder);


	//== Leaf with task panel container

	builder = gtk_builder_new_from_resource("/org/gnome/aion-task/ui/task-details-leaf");
	g_assert(builder != NULL);

	priv->task_details_leaf = GTK_WIDGET(gtk_builder_get_object(builder, "task-details-leaf"));
	g_assert(priv->task_details_leaf);
	gtk_container_add (GTK_CONTAINER(priv->main_leaflet), priv->task_details_leaf);

	GtkWidget* panel_container_place = GTK_WIDGET(gtk_builder_get_object(builder, "panel-container-place"));
	hdy_clamp_set_maximum_size(HDY_CLAMP(panel_container_place), TASK_PANEL_MAX_WIDTH);
	g_assert(panel_container_place);

	priv->hide_details_button = GTK_WIDGET(gtk_builder_get_object(builder, "hide_details_button"));

	g_object_unref(builder);

	priv->panel_container_leaf = aion_panel_container_new();
	gtk_widget_show(GTK_WIDGET(priv->panel_container_leaf));
	gtk_container_add (GTK_CONTAINER(panel_container_place), GTK_WIDGET(priv->panel_container_leaf));
	gtk_widget_set_size_request(GTK_WIDGET(priv->panel_container_leaf), TASK_PANEL_MIN_WIDTH, -1); 
	gtk_widget_set_hexpand(GTK_WIDGET(priv->panel_container_leaf), TRUE);
	g_signal_connect(priv->panel_container_leaf, "panel-editing-finished", G_CALLBACK(_panel_editing_finished), main_window); 
	g_signal_connect(priv->panel_container_leaf, "save-needed", G_CALLBACK(_panel_save_needed), main_window); 

	// Task general panel
	AionTaskGeneralPanel* task_general_panel = aion_task_general_panel_new();
	aion_panel_container_add_panel(priv->panel_container_leaf, AION_PANELABLE(task_general_panel), TRUE, TRUE);
	priv->task_general_panel = AION_PANELABLE(task_general_panel);

	// Task properties panel
	AionTaskPropertiesPanel* task_properties_panel = aion_task_properties_panel_new();
	aion_panel_container_add_panel(priv->panel_container_leaf, AION_PANELABLE(task_properties_panel), TRUE, TRUE);
	priv->task_properties_panel = AION_PANELABLE(task_properties_panel);

	// Task note panel
	AionTaskNotePanel* task_note_panel = aion_task_note_panel_new();
	aion_panel_container_add_panel(priv->panel_container_leaf, AION_PANELABLE(task_note_panel), TRUE, TRUE);

	// Subtask panel
	AionSubTaskPanel* sub_task_panel = aion_sub_task_panel_new();
	aion_panel_container_add_panel(priv->panel_container_leaf, AION_PANELABLE(sub_task_panel), FALSE, FALSE);
	priv->sub_task_panel = AION_PANELABLE(sub_task_panel);

	// Task log panel
	AionTaskLogPanel* task_log_panel = aion_task_log_panel_new();
	aion_panel_container_add_panel(priv->panel_container_leaf, AION_PANELABLE(task_log_panel), TRUE, TRUE);
	priv->task_log_panel = AION_PANELABLE(task_log_panel);

	g_signal_connect(G_OBJECT(main_window), "show", G_CALLBACK(window_show_event), NULL);

	g_assert(main_window);
	g_assert(GTK_IS_WIDGET(main_window));
	gtk_widget_show(GTK_WIDGET(main_window));

	instance_counter++;
}

static void
aion_main_window_dispose(GObject* object)
{
	g_return_if_fail(AION_IS_MAIN_WINDOW(object));
	AionMainWindow* main_window = AION_MAIN_WINDOW(object);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	if (priv->deleted_task){
		g_object_unref(priv->deleted_task);
		priv->deleted_task = NULL;
	}
	G_OBJECT_CLASS(aion_main_window_parent_class)->dispose(object);
}

static void
aion_main_window_finalize(GObject* object)
{
	g_return_if_fail(AION_IS_MAIN_WINDOW(object));
	G_OBJECT_CLASS(aion_main_window_parent_class)->finalize(object);

	AionMainWindow* main_window = AION_MAIN_WINDOW(object);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	g_free(priv->text_searched);
	g_date_free(priv->filter_limit_date);

	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_MAIN_WINDOW));
}


static void
aion_main_window_class_init(AionMainWindowClass* aion_main_window_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(aion_main_window_class);
	object_class->dispose= aion_main_window_dispose;
	object_class->finalize = aion_main_window_finalize;
}


AionMainWindow* 
aion_main_window_new(AionApplication* aion_application)
{
	g_return_val_if_fail (AION_IS_APPLICATION (aion_application), NULL);
	AionMainWindow* self = AION_MAIN_WINDOW(g_object_new(AION_TYPE_MAIN_WINDOW, 
		"application", GTK_APPLICATION(aion_application),
		NULL));
	return self;
}

AionApplication* aion_main_window_get_application(AionMainWindow* main_window)
{
	g_return_val_if_fail (AION_IS_MAIN_WINDOW(main_window), NULL);
	AionApplication* aion_application = (AionApplication*) gtk_window_get_application(GTK_WINDOW(main_window));
	g_assert(AION_IS_APPLICATION(aion_application));
	return aion_application;
}


static void
_task_list_remove_row(GtkWidget* widget, gpointer data)
{
	GtkListBox* list_box = GTK_LIST_BOX(data);
	gtk_container_remove(GTK_CONTAINER(list_box), widget);
}

static GtkListBoxRow*
_task_list_add_row (AionTask* aion_task, gpointer p_main_window)
{
	g_assert(AION_IS_TASK(aion_task));
	AionMainWindow* main_window = AION_MAIN_WINDOW(p_main_window);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);

	_TaskListRowData* task_list_row_data = g_malloc0(sizeof(_TaskListRowData));
	task_list_row_data->main_window = main_window;
	task_list_row_data->task = aion_task;

	GtkWidget* list_box_row = gtk_list_box_row_new();
	task_list_row_data->list_box_row = GTK_LIST_BOX_ROW(list_box_row);
	g_signal_connect(list_box_row, "destroy", G_CALLBACK(_task_list_row_destroyed), task_list_row_data);

	GtkWidget* h_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5); // FIXME hard coded spacing
	gtk_container_add(GTK_CONTAINER(list_box_row), h_box);
	gtk_container_set_border_width(GTK_CONTAINER(h_box), 2); // FIXME hard coded spacing

	// task list row
	AionTaskListRow* task_list_row = aion_task_list_row_new(NULL); // task will be set later
	gtk_box_pack_start(GTK_BOX(h_box), GTK_WIDGET(task_list_row), TRUE, TRUE, 0);  // FIXME hard coded spacing
	task_list_row_data->task_list_row = task_list_row;

	gtk_widget_show_all(GTK_WIDGET(list_box_row));
	// task of task list row is set after gtk_widget_show_all because it's field may be invisble
	aion_task_list_row_set_task(task_list_row, aion_task);
	g_object_set_data(G_OBJECT(list_box_row), "task-list-row", task_list_row);

	gtk_container_add(GTK_CONTAINER(priv->task_list_box), GTK_WIDGET(list_box_row));
	return GTK_LIST_BOX_ROW(list_box_row);
}

void
aion_main_window_display_data(AionMainWindow* main_window)
{
	g_return_if_fail (AION_IS_MAIN_WINDOW(main_window));
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	AionApplication* aion_application =  aion_main_window_get_application(main_window);
	AionTaskManager* task_manager = aion_application_get_task_manager(aion_application);
	// clear list
	gtk_container_foreach(GTK_CONTAINER(priv->task_list_box), _task_list_remove_row, priv->task_list_box);

	// add a special row to explain why list is empty 
	GtkWidget* message_row = gtk_label_new("");
	gtk_widget_hide(message_row);
	gtk_widget_set_margin_top(message_row, 30);
	gtk_label_set_line_wrap(GTK_LABEL(message_row), TRUE);
	gtk_label_set_justify(GTK_LABEL(message_row), GTK_JUSTIFY_CENTER);

	GtkWidget* row = gtk_list_box_row_new();
	gtk_widget_show(row);
	gtk_container_add(GTK_CONTAINER(row), message_row);
	gtk_list_box_prepend(GTK_LIST_BOX(priv->task_list_box), row);
	gtk_list_box_row_set_activatable(GTK_LIST_BOX_ROW(row), FALSE);
	gtk_list_box_row_set_selectable(GTK_LIST_BOX_ROW(row), FALSE);

	// add a row for each task
	aion_task_manager_for_each_task(task_manager, (AionTaskCallback) _task_list_add_row, main_window);
}


static void
_action_cancel_task_delete (GSimpleAction* action, GVariant* parameter, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	g_return_if_fail(priv->deleted_task != NULL);
	AionApplication* aion_application = aion_main_window_get_application(main_window);
	AionTaskManager* task_manager = aion_application_get_task_manager(aion_application);

	g_info("Restore task [%s]", aion_task_get_label(priv->deleted_task));

	aion_task_manager_append_task(task_manager, priv->deleted_task);
	GtkListBoxRow* new_row =_task_list_add_row (priv->deleted_task, main_window);

	gtk_list_box_select_row(priv->task_list_box, new_row);
	_select_last_row(priv->task_list_box);
	aion_panel_container_set_task(priv->panel_container_leaf, priv->deleted_task);

	g_object_unref(priv->deleted_task);
	priv->deleted_task = NULL;
}


static void
_task_list_row_destroyed (GtkButton* button, gpointer data)
{
	_TaskListRowData* task_list_row_data = data;
	g_free(task_list_row_data);
}

static void
_task_list_row_selected (GtkListBox *task_list_box, GtkListBoxRow *selected_row, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);

	AionTask* selected_task = NULL;
	if (selected_row == NULL) {
		// no row is selected
		selected_task = NULL;
	}
	else {
		// a row is selected
		gpointer task_list_row_pointer = g_object_get_data(G_OBJECT(selected_row), "task-list-row");
		if (! task_list_row_pointer) 
			g_error("Selected row does not contain a task");
		AionTaskListRow* task_list_row = AION_TASK_LIST_ROW(task_list_row_pointer);
		selected_task = (AionTask*)aion_task_list_row_get_task(task_list_row);
	}
	priv->selected_task = selected_task;
}

// FIXME test to use gtk_list_box_set_activate_on_single_click

static void
_task_list_row_activated(GtkListBox* task_list_box, GtkListBoxRow *activated_row, gpointer data)
{
	gpointer task_list_row_pointer = g_object_get_data(G_OBJECT(activated_row), "task-list-row");
	if (! task_list_row_pointer) 
		g_error("Activated row does not contain a task");
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	hdy_leaflet_set_visible_child(HDY_LEAFLET(priv->main_leaflet), priv->task_details_leaf);
	aion_panel_container_set_task(AION_PANEL_CONTAINER(priv->panel_container_leaf), priv->selected_task);
}


static gboolean
_select_last_row_callback(gpointer data)
{
	GtkWidget* list_box = GTK_WIDGET(data);
	g_assert(GTK_IS_LIST_BOX(list_box));

	GtkWidget* parent_viewport = gtk_widget_get_parent(GTK_WIDGET(list_box));
	g_assert(GTK_IS_VIEWPORT(parent_viewport));
	GtkWidget* scrolled_window = gtk_widget_get_parent(GTK_WIDGET(parent_viewport));
	g_assert(GTK_IS_SCROLLED_WINDOW(scrolled_window));

	GList* children = gtk_container_get_children(GTK_CONTAINER(list_box));
	GList* last_child_item = g_list_last(children);
	GtkWidget* child = last_child_item->data;
	g_list_free(children);

	g_assert(child != NULL);
	g_assert(GTK_IS_WIDGET(child));
	g_assert(GTK_IS_LIST_BOX_ROW(child));

	gint x, y;
	if (! gtk_widget_translate_coordinates (child, scrolled_window,  0, 0, &x, &y))
		g_warning("Can't translate coordinates");

	GtkAdjustment *vadj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(scrolled_window));
	gtk_adjustment_set_value(vadj, y);

	return FALSE;
}

static void 
_select_last_row(GtkListBox* list_box)
{
	g_assert(GTK_IS_LIST_BOX(list_box));
	g_idle_add(_select_last_row_callback, list_box);
}

static gboolean
_message_display_when_list_empty_cb(gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	priv->filter_timer_id = 0;

	GtkWidget* message_row = NULL;
	GList *row_list = gtk_container_get_children(GTK_CONTAINER(priv->task_list_box));
	GList* row_item = row_list;
	guint n_row = 0;
	guint visible_row_count = 0;
	while (row_item != NULL) {
		GtkWidget* row = row_item->data;
		row_item = row_item->next;
		GtkWidget* row_child = gtk_bin_get_child(GTK_BIN(row));
		// ignore message row
		if (GTK_IS_LABEL(row_child)) {
			message_row = row_child;
			continue;
		}
		// FYI :calling gtk_widget_is_visible(row_child) does not work
		if (gtk_widget_get_child_visible(row))
			visible_row_count++;
		n_row++;
	}
	g_assert(message_row != NULL);
	if (visible_row_count > 0){
		gtk_widget_hide(message_row);
	}
	else {
		gtk_widget_show(message_row);
		gchar* msg = NULL;
		if (n_row == 0)
			msg = _("<b>No task currently exists...</b>\n\nClick on the new task button to create your first task.");
		else
			msg = _("<b>There is no pending task...</b>\n\nClick on the filter button in toolbar to show all tasks.");
		gtk_label_set_markup(GTK_LABEL(message_row), msg);
	}

	g_list_free(row_list);
	return FALSE; // do not call again
}


static void _schedule_message_display_when_list_empty(AionMainWindow* main_window)
{
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	if (priv->filter_timer_id > 0) 
		g_source_remove(priv->filter_timer_id);
	priv->filter_timer_id = g_timeout_add(100, _message_display_when_list_empty_cb, main_window);
}

static void
_search_entry_changed (GtkSearchEntry* search_entry, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	g_free(priv->text_searched);
	// TODO strip text searched 
	priv->text_searched = g_utf8_casefold(gtk_entry_get_text(GTK_ENTRY(search_entry)), -1);
	g_strstrip((gchar*)priv->text_searched);
	gtk_list_box_invalidate_filter(GTK_LIST_BOX(priv->task_list_box));
	gtk_list_box_invalidate_sort(GTK_LIST_BOX(priv->task_list_box));
}

static void
_search_entry_stop (GtkSearchEntry* search_entry, gpointer data)
{
	// when user press the ESCAPE keyboard key, toggle the search button in header bar
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	GAction* search_action = g_action_map_lookup_action(G_ACTION_MAP(main_window), ACTION_NAME_SHOW_SEARCH_BAR);
	g_assert(search_action != NULL);
	g_action_change_state(G_ACTION(search_action), g_variant_new_boolean(FALSE));
}

//TODO implement the [screen-changed] signal to detect when the window is moved from smartphone screen to real screen
static void
update_header_bar (AionMainWindow* main_window)
{
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	gboolean folded = hdy_leaflet_get_folded(HDY_LEAFLET(priv->main_leaflet));
	gtk_widget_set_visible(priv->hide_details_button, folded);
}

static void
leaflet_folded_changed (HdyLeaflet* leaflet, GParamSpec *pspec, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	update_header_bar(main_window);
}

static void
window_show_event(GtkWidget *main_window, gpointer user_data)
{
	update_header_bar(AION_MAIN_WINDOW(main_window));
}


AionNotification*
aion_main_window_display_notification(AionMainWindow* window, gchar* message)
{
	g_return_val_if_fail(AION_IS_MAIN_WINDOW(window), NULL);
	g_return_val_if_fail(message != NULL, NULL);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(window);
	AionNotification* notification = aion_notification_new_with_message(message);
	gtk_box_pack_start(GTK_BOX(priv->notification_stack), GTK_WIDGET(notification), FALSE, FALSE, 5);
	g_object_set(G_OBJECT(notification), "halign", GTK_ALIGN_CENTER, "valign", GTK_ALIGN_START, NULL);
	gtk_widget_show(GTK_WIDGET(notification));
	return notification;
}


gint
aion_main_window_get_instance_count()
{
	return instance_counter;
}

gint
aion_main_window_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_MAIN_WINDOW));
	return instance_counter;
}

static void
_refresh_selected_row(AionMainWindow* main_window)
{
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	AionTask* selected_task = priv->selected_task;
	g_assert(selected_task != NULL);

	GtkListBoxRow* selected_row =  gtk_list_box_get_selected_row (priv->task_list_box);
	AionTaskListRow* task_list_row = AION_TASK_LIST_ROW(g_object_get_data(G_OBJECT(selected_row), "task-list-row"));
	aion_task_list_row_refresh(task_list_row);
	gtk_list_box_invalidate_filter(priv->task_list_box);
	gtk_list_box_invalidate_sort(GTK_LIST_BOX(priv->task_list_box));
}

static void
_panel_editing_finished(AionPanelContainer* panel_container, AionPanelable* panel, AionTask* task, gboolean modification, gpointer data)
{
	g_assert(AION_IS_PANEL_CONTAINER(panel_container));
	g_assert(AION_IS_PANELABLE(panel));
	g_assert(AION_IS_TASK(task));
	g_assert(data != NULL && AION_IS_MAIN_WINDOW(data));

	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	AionApplication* aion_application = aion_main_window_get_application(main_window);
	AionTaskManager* task_manager = aion_application_get_task_manager(aion_application);

	if (panel == priv->task_properties_panel) {
		// detect if edited task is new by checking if already in task list
		gboolean is_a_new_task = ! aion_task_manager_contains_task(task_manager, task);
		if (! modification) {
			if (is_a_new_task) {
				aion_panel_container_set_task(priv->panel_container_leaf, NULL);
				aion_main_window_display_notification(main_window, _("Task creation canceled"));
				hdy_leaflet_set_visible_child(HDY_LEAFLET(priv->main_leaflet), priv->task_list_leaf);
				// FIXME add a button in notification to restore newly created task
			}
		}
		else {
			if (is_a_new_task) {
				aion_task_manager_append_task(task_manager, task);
				GtkListBoxRow* new_row = _task_list_add_row (task, main_window);
				gtk_list_box_select_row(priv->task_list_box, new_row);
				_select_last_row(priv->task_list_box);
				aion_main_window_display_notification(main_window, _("New task created"));
			}
			_refresh_selected_row(main_window);
			aion_panelable_refresh(priv->task_general_panel);
			aion_panelable_refresh(priv->task_log_panel); // refresh log in case log limit is reduced
		}
	}
}

static gboolean
_panel_save_needed(AionPanelContainer* panel_container, AionPanelable* panel, AionTask* task, gpointer data)
{
	g_assert(AION_IS_PANEL_CONTAINER(panel_container));
	g_assert(AION_IS_PANELABLE(panel));
	g_assert(AION_IS_TASK(task));
	g_assert(data != NULL && AION_IS_MAIN_WINDOW(data));

	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	gchar* message = g_strdup_printf("Changes in task [%s] have been save automatically...", aion_task_get_label(task));
	aion_main_window_display_notification(main_window, message);
	g_free(message);
	return TRUE; // ask to save data */
}

static void
_filter_day_count_changed(GtkSpinButton* spin_button, gpointer data)
{
	AionMainWindow* main_window = AION_MAIN_WINDOW(data);
	AionMainWindowPrivate* priv = aion_main_window_get_instance_private(main_window);
	priv->filter_limit_days_count = gtk_spin_button_get_value(spin_button);
	g_date_free(priv->filter_limit_date);
	priv->filter_limit_date = aion_util_get_today_date();
	g_date_add_days(priv->filter_limit_date, priv->filter_limit_days_count);
	gtk_list_box_invalidate_filter(GTK_LIST_BOX(priv->task_list_box));
	gtk_list_box_invalidate_sort(GTK_LIST_BOX(priv->task_list_box));
}


