/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <gtk/gtk.h>

void check_memory_freed();

GDate* aion_util_get_today_date();
GDate* aion_util_calendar_get_date(GtkCalendar*);
gchar* aion_util_get_duration_human_representation(gint);
gchar* aion_util_get_date_local_representation(const GDate*);
void aion_util_grab_focus(GtkWidget*);
