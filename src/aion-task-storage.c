/* AionTaskStorage
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-TaskStorage"
#include "aion-task-storage.h"
#include "aion-task-private.h"

#define XML_FORMAT_VERSION 9

struct _AionTaskStorage {
	GObject parent;
};

typedef struct {
	GFile* xml_file;
} AionTaskStoragePrivate;

G_DEFINE_TYPE_WITH_PRIVATE(AionTaskStorage, aion_task_storage, G_TYPE_OBJECT);

static gint instance_counter = 0;

GQuark
aion_storage_error_quark(void)
{
        return g_quark_from_static_string("aion-type-storage-error-quark");
}

static void
aion_task_storage_init(AionTaskStorage* task_storage)
{
	AionTaskStoragePrivate* priv = aion_task_storage_get_instance_private(task_storage);
	priv->xml_file = NULL;
	instance_counter++;
}


static void
aion_task_storage_dispose(GObject* object)
{
	//AionTaskStorage* task_storage = AION_TASK_STORAGE(object);
	//AionTaskStoragePrivate *priv = aion_task_storage_get_instance_private(task_storage);
	G_OBJECT_CLASS(aion_task_storage_parent_class)->dispose(object);
}


static void
aion_task_storage_finalize(GObject* object)
{
	AionTaskStorage* task_storage = AION_TASK_STORAGE(object);
	AionTaskStoragePrivate *priv = aion_task_storage_get_instance_private(task_storage);
	if (priv->xml_file != NULL) 
		g_object_unref(priv->xml_file);
	G_OBJECT_CLASS(aion_task_storage_parent_class)->finalize(object);
	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_TASK_STORAGE));
}

 
static void
aion_task_storage_class_init(AionTaskStorageClass* task_storage_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(task_storage_class);
	object_class->dispose= aion_task_storage_dispose;
	object_class->finalize = aion_task_storage_finalize;
}


AionTaskStorage*
aion_task_storage_new(GFile* xml_file)
{
	g_return_val_if_fail(G_IS_FILE(xml_file), NULL);
	AionTaskStorage* task_storage = AION_TASK_STORAGE(g_object_new(AION_TYPE_TASK_STORAGE, NULL));
	AionTaskStoragePrivate *priv = aion_task_storage_get_instance_private(task_storage);
	g_object_ref(xml_file);
	priv->xml_file = xml_file;
	return task_storage;
}



gboolean aion_task_storage_save(AionTaskStorage* task_storage, GSList* aion_task_list, GError** error)
{
	gchar* line = NULL;
	gboolean success;
	gboolean result = FALSE;
	GFileOutputStream* file_output_stream = NULL;
	GDataOutputStream* data_output_stream = NULL;

	AionTaskStoragePrivate *priv = aion_task_storage_get_instance_private(task_storage);
	g_return_val_if_fail(AION_IS_TASK_STORAGE(task_storage), FALSE);
	g_return_val_if_fail(AION_IS_TASK_STORAGE(task_storage), FALSE);

	// open XML file for writing
	file_output_stream = g_file_replace(priv->xml_file, NULL, FALSE, G_FILE_CREATE_PRIVATE, NULL, error);
	if (file_output_stream == NULL)
		goto done;
	data_output_stream = g_data_output_stream_new(G_OUTPUT_STREAM(file_output_stream));

	// write opening root tag
	line = g_strdup_printf("<aion-task format=\"%i\">\n", XML_FORMAT_VERSION);
	success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
	if (! success)
		goto done;
	g_free(line); line = NULL;

	// write <task-list> start tag
	line = g_strdup_printf("\t<task-list>\n");
	success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
	if (! success)
		goto done;
	g_free(line); line = NULL;

	// write each task
	GSList* aion_task_item = aion_task_list;
	while (aion_task_item) {
		AionTask* task = (AionTask*) aion_task_item->data;
		g_assert(AION_IS_TASK(task));
		aion_task_item = aion_task_item->next;
		g_debug("Saving task [%s]", aion_task_get_label(task));

		// write <task> start tag

		gchar* enabled_attr = g_strdup_printf("enabled=\"%s\" ",
			aion_task_is_enabled(task) ? "true" : "false");

		gchar* repetition_type_attr = g_strdup_printf("repetition_type=\"%s\" ",
			aion_task_get_repetition_type_to_string(task));

		const GDate *dt = aion_task_get_next_execution_date(task);
		gchar* next_execution_date_attr = NULL;
		// FIXME no need to test if next execution date is valid
		if (g_date_valid(dt)){
			next_execution_date_attr = g_strdup_printf("next_execution=\"%04i-%02i-%02i\" ",
					g_date_get_year(dt), 
					g_date_get_month(dt), 
					g_date_get_day(dt)
				);
		}
		else {
			next_execution_date_attr = g_strdup(""); 
		}

		gint preview_delay = aion_task_get_preview_delay(task);
		gchar* preview_delay_attr = NULL;
		if (preview_delay > 0) 
			preview_delay_attr = g_strdup_printf("preview_delay=\"%i\" ", preview_delay);
		else
			preview_delay_attr = g_strdup("");


		gint postponement_delay = aion_task_get_postponement_delay(task);
		gchar* postponement_delay_attr = NULL;
		if (postponement_delay > 0) 
			postponement_delay_attr = g_strdup_printf("postponement_delay=\"%i\" ", postponement_delay);
		else
			postponement_delay_attr = g_strdup("");

		gchar* postpone_attr = NULL;
		gchar* interval_attr = NULL;
		if (aion_task_get_repetition_type(task) != AION_TASK_REPETITION_ONCE) {
			interval_attr = g_strdup_printf("interval=\"%i\" ", aion_task_get_interval(task));
			gchar* postpone_from_when_str = NULL;
			switch (aion_task_get_postpone_from_when(task)) {
				case AION_TASK_POSTPONE_FROM_EXECUTION_DATE :
					postpone_from_when_str = "execution";
					break;
				case AION_TASK_POSTPONE_FROM_DUE_DATE:
					postpone_from_when_str = "due-date";
					break;
				default:
					g_assert_not_reached();
			}
			postpone_attr = g_strdup_printf("postpone_from=\"%s\" ", postpone_from_when_str);
		}
		else {
			interval_attr = g_strdup("");
			postpone_attr = g_strdup("");
		}

		gchar* priority_attr = g_strdup_printf("priority=\"%i\" ", aion_task_get_priority(task));

		line = g_strdup_printf("\t\t<task %s%s%s%s%s%s%s%s>\n",
				enabled_attr,
				repetition_type_attr,
				postpone_attr,
				interval_attr,
				priority_attr,
				next_execution_date_attr,
				preview_delay_attr,
				postponement_delay_attr
			);
		g_free(enabled_attr);
		g_free(repetition_type_attr);
		g_free(next_execution_date_attr);
		g_free(interval_attr);
		g_free(priority_attr);
		g_free(postpone_attr);
		g_free(postponement_delay_attr);
		g_free(preview_delay_attr);

		success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
		if (! success)
			goto done;
		g_free(line); line = NULL;

		// write <label> tag
		line = g_markup_printf_escaped("\t\t\t<label>%s</label>\n", aion_task_get_label(task));
		success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
		if (! success)
			goto done;
		g_free(line); line = NULL;

		// write <note> tag
		const gchar* note = aion_task_get_note(task);
		if (note) {
			line = g_markup_printf_escaped("\t\t\t<note>%s</note>\n", note);
			success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
			if (! success)
				goto done;
			g_free(line); line = NULL;
		}


		// write <task-log> start tag

		gchar* default_duration_attr = NULL;
		gint default_duration = aion_task_get_default_duration(task);
		if (default_duration > 0)
			default_duration_attr = g_strdup_printf("default_duration=\"%i\" ",  default_duration);
		else
			default_duration_attr = g_strdup("");

		gchar* total_duration_attr = NULL;
		gint total_duration = aion_task_get_total_duration(task);
		if (total_duration > 0)
			total_duration_attr = g_strdup_printf("total_duration=\"%i\" ",  total_duration);
		else
			total_duration_attr = g_strdup("");

		gint execution_count = aion_task_get_execution_count(task);
		gint log_entries_count = aion_task_get_log_entries_count(task);
		gint log_entries_limit = aion_task_get_log_entries_limit(task);
		g_assert(log_entries_limit >= log_entries_count);
		line = g_strdup_printf("\t\t\t<task-log total=\"%i\" count=\"%i\" limit=\"%i\" %s%s>\n",
				execution_count,
				log_entries_count,
				log_entries_limit,
				default_duration_attr,
				total_duration_attr
			);
		g_free(default_duration_attr);
		g_free(total_duration_attr);

		success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
		if (! success)
			goto done;
		g_free(line); line = NULL;

		for (gint entry_index = 0; entry_index < log_entries_count; entry_index++) {
			AionTaskLogEntry* log_entry = aion_task_private_get_log_entry(task, entry_index);

			// write <log> tag

			gchar* duration_attr = NULL;
			//gint duration = aion_task_get_log_entry_duration(task, log_entry_index);
			if (log_entry->duration > 0)
				duration_attr = g_strdup_printf(" duration=\"%i\" ",  log_entry->duration);
			else
				duration_attr = g_strdup("");

			line = g_strdup_printf("\t\t\t\t<log date=\"%04i-%02i-%02i\"%s/>\n",
					log_entry->year,
					log_entry->month,
					log_entry->day,
					duration_attr
				);

			g_free(duration_attr);

			success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
			if (! success)
				goto done;
			g_free(line); line = NULL;
		}

		// write <task-log> end tag
		line = g_strdup_printf("\t\t\t</task-log>\n");
		success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
		if (! success)
			goto done;
		g_free(line); line = NULL;


		GList* sub_task_list = aion_task_get_sub_task_list(task);
		if (sub_task_list != NULL) {

			// write <sub-task-list> start tag
			line = g_strdup_printf("\t\t\t<sub-task-list>\n");
			success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
			if (! success)
				goto done;
			g_free(line); line = NULL;

			// write each sub-task
			GList* sub_task_item = sub_task_list;
			while (sub_task_item) {
				AionSubTask* sub_task = AION_SUB_TASK(sub_task_item->data);
				line = g_strdup_printf("\t\t\t\t<sub-task done=\"%s\">%s</sub-task>\n",
						aion_sub_task_is_done(sub_task) ? "true" : "false",
						aion_sub_task_get_label(sub_task)
					);
				success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
				if (! success)
					goto done;
				g_free(line); line = NULL;
				sub_task_item = sub_task_item->next;
			}

			// write <sub-task-list> end tag
			line = g_strdup_printf("\t\t\t</sub-task-list>\n");
			success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
			if (! success)
				goto done;
			g_free(line); line = NULL;

			g_list_free(sub_task_list);
		}

		// write <task> end tag
		line = g_strdup_printf("\t\t</task>\n");
		success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
		if (! success)
			goto done;
		g_free(line); line = NULL;


	}

	// write <task-list> end tag
	line = g_strdup_printf("\t</task-list>\n");
	success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
	if (! success)
		goto done;
	g_free(line); line = NULL;

	// write closing root tag
	line = g_strdup_printf("</aion-task>\n");
	success = g_data_output_stream_put_string(data_output_stream, line, NULL, error);
	if (! success)
		goto done;
	g_free(line); line = NULL;

	success = g_output_stream_close(G_OUTPUT_STREAM(file_output_stream), NULL, error);
	if (! success)
		goto done;

	g_info("Data save success");
	result = TRUE;

done:
	if (data_output_stream) g_object_unref(data_output_stream); 
	if (file_output_stream) g_object_unref(file_output_stream); 
	if (line)               g_free(line);
	return result;
}



enum {
	_AION_XML_CONTEXT_ROOT,
	_AION_XML_CONTEXT_HEADER,
	_AION_XML_CONTEXT_TASK_LIST,
	_AION_XML_CONTEXT_TASK_ITEM,
	_AION_XML_CONTEXT_TASK_LABEL,
	_AION_XML_CONTEXT_TASK_NOTE,
	_AION_XML_CONTEXT_TASK_LOG,
	_AION_XML_CONTEXT_TASK_LOG_ENTRY,
	_AION_XML_CONTEXT_SUB_TASK_LIST,
	_AION_XML_CONTEXT_SUB_TASK_ITEM
};

struct _xml_parse_data {
	gint context;
	gboolean load_success;
	GSList* task_list;
	AionTask* current_task;
	gint log_entry_index;
	AionSubTask* current_sub_task;
};


static void
_xml_start_element_func(GMarkupParseContext* context, const gchar* element_name, const gchar** attribute_names, const gchar** attribute_values, gpointer data, GError** error)
{
	struct _xml_parse_data* xml_data = (struct _xml_parse_data*) data;
	g_debug("\t- XML start <%s>", element_name);

	// <aion-task> root markup
	if (g_strcmp0("aion-task", element_name) == 0) {
		if (xml_data->context != _AION_XML_CONTEXT_ROOT) {
			g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
						"<aion-task> is not in root context");
			return;
		}
		xml_data->context = _AION_XML_CONTEXT_HEADER;
		gint i_attr = 0;
		while (TRUE){
			const gchar* attribute_name = attribute_names[i_attr];
			const gchar* attribute_value = attribute_values[i_attr];
			if (attribute_name == NULL)
				break;
			// attribute [format]
			if (g_strcmp0("format", attribute_name) == 0) {
				gint format_version = g_ascii_strtoll(attribute_value, NULL, 0);
				if (format_version == 0) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid XML format version \"%s\"", attribute_value);
					return;
				}
				if (format_version > XML_FORMAT_VERSION) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"XML format version %i not supported (higher supported version is %i)", format_version, XML_FORMAT_VERSION);
					return;
				}
			}
			i_attr++;
		}
		return;
	}

	// <task-list> markup
	if (g_strcmp0("task-list", element_name) == 0) {
		if (xml_data->context != _AION_XML_CONTEXT_HEADER) {
			g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
						"<task-list> is not in <aion-task>");
			return;
		}
		xml_data->context = _AION_XML_CONTEXT_TASK_LIST;
		return;
	}


	// <task> markup
	if (g_strcmp0("task", element_name) == 0) {
		if (xml_data->context != _AION_XML_CONTEXT_TASK_LIST) {
			g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
						"<task> is not in <task-list>");
			return;
		}
		g_assert(xml_data->current_task == NULL);
		xml_data->context = _AION_XML_CONTEXT_TASK_ITEM;
		xml_data->current_task = aion_task_new("new task");
		xml_data->task_list = g_slist_append(xml_data->task_list, xml_data->current_task);
		xml_data->log_entry_index = 0;
		g_assert(xml_data->current_sub_task == NULL);
		xml_data->current_sub_task = NULL;
		gint i_attr = 0;
		while (TRUE){
			const gchar* attribute_name = attribute_names[i_attr];
			const gchar* attribute_value = attribute_values[i_attr];
			if (attribute_name == NULL)
				break;
			g_assert(attribute_value != NULL);
			g_debug("\t\t* %s = %s", attribute_name, attribute_value);

			if (g_strcmp0("enabled", attribute_name) == 0) {
				gboolean enabled = FALSE;
				if (g_strcmp0(attribute_value, "true") == 0)
					enabled = TRUE;
				else if (g_strcmp0(attribute_value, "false") == 0)
					enabled = FALSE;
				else {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid boolean value of an [enabled] attribute");
					return;
				}
				aion_task_set_enabled(xml_data->current_task, enabled);
			}

			if (g_strcmp0("repetition_type", attribute_name) == 0) {
				gint repetition_type = aion_task_repetition_type_from_string(attribute_value);
				aion_task_set_repetition_type(xml_data->current_task, repetition_type);
			}

			if (g_strcmp0("postpone_from", attribute_name) == 0) { // due-date or execution
				AionTaskPostponeFromWhen postpone_from_when;
				if (g_strcmp0(attribute_value, "execution") == 0)
					postpone_from_when = AION_TASK_POSTPONE_FROM_EXECUTION_DATE;
				else if (g_strcmp0(attribute_value, "due-date") == 0)
					postpone_from_when = AION_TASK_POSTPONE_FROM_DUE_DATE;
				else {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid value [%s) for a [postpone_from] attribute",
								attribute_value);
					return;
				}
				aion_task_set_postpone_from_when(xml_data->current_task, postpone_from_when);
			}

			if (g_strcmp0("interval", attribute_name) == 0) {
				gint interval = g_ascii_strtoll(attribute_value, NULL, 0);
				if (interval == 0) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid task interval");
					return;
				}
				aion_task_set_interval(xml_data->current_task, interval);
			}

			if (g_strcmp0("priority", attribute_name) == 0) {
				gint priority = g_ascii_strtoll(attribute_value, NULL, 0);
				if (priority < AION_TASK_MINIMUM_PRIORITY || priority > AION_TASK_MAXIMUM_PRIORITY) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid task priority");
					return;
				}
				aion_task_set_priority(xml_data->current_task, priority);
			}


			if (g_strcmp0("next_execution", attribute_name) == 0)  {
				GDate* dt = g_date_new();
				g_date_set_parse(dt, attribute_value);
				if (! g_date_valid(dt)){
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid next execution date");
					return;
				}
				aion_task_set_next_execution_date(xml_data->current_task, dt);
				g_date_free(dt);
			}

			if (g_strcmp0("postponement_delay", attribute_name) == 0) {
				gint postponement_delay = g_ascii_strtoll(attribute_value, NULL, 0);
				if (postponement_delay == 0) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid task postponement delay");
					return;
				}
				aion_task_set_postponement_delay(xml_data->current_task, postponement_delay);
			}
	
			if (g_strcmp0("preview_delay", attribute_name) == 0) {
				gint preview_delay = g_ascii_strtoll(attribute_value, NULL, 0);
				if (preview_delay == 0) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid task preview delay");
					return;
				}
				aion_task_set_preview_delay(xml_data->current_task, preview_delay);
			}

			i_attr++;
		}
		return;
	}

	// <label> markup
	if (g_strcmp0("label", element_name) == 0) {
		if (xml_data->context != _AION_XML_CONTEXT_TASK_ITEM) {
			g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
						"<label> is not in <task>");
			return;
		}
		xml_data->context = _AION_XML_CONTEXT_TASK_LABEL;
		g_assert(xml_data->current_task != NULL);
		return;
	}

	// <note> markup
	if (g_strcmp0("note", element_name) == 0) {
		if (xml_data->context != _AION_XML_CONTEXT_TASK_ITEM) {
			g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
						"<note> is not in <task>");
			return;
		}
		xml_data->context = _AION_XML_CONTEXT_TASK_NOTE;
		g_assert(xml_data->current_task != NULL);
		return;
	}

	// <task-log> markup
	if (g_strcmp0("task-log", element_name) == 0) {
		if (xml_data->context != _AION_XML_CONTEXT_TASK_ITEM) {
			g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
						"<task-log> is not in <task>");
			return;
		}
		xml_data->context = _AION_XML_CONTEXT_TASK_LOG;
		g_assert(xml_data->current_task != NULL);
		xml_data->log_entry_index = 0;
		gint i_attr = 0;
		while (TRUE){
			const gchar* attribute_name = attribute_names[i_attr];
			const gchar* attribute_value = attribute_values[i_attr];
			if (attribute_name == NULL)
				break;
			g_assert(attribute_value != NULL);
			//g_print("\t\t* %s = %s\n", attribute_name, attribute_value);

			// attribute [total]
			if (g_strcmp0("total", attribute_name) == 0)  {
				gint execution_count = g_ascii_strtoll(attribute_value, NULL, 0);
				if (execution_count < 0) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid task execution count");
					return;
				}
				aion_task_private_set_execution_count(xml_data->current_task, execution_count);
			}

			// attribute [count]
			if (g_strcmp0("count", attribute_name) == 0)  {
				gint log_entries_count = g_ascii_strtoll(attribute_value, NULL, 0);
				if (log_entries_count < 0) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid task log entries counter");
					return;
				}
				aion_task_private_resize_log_entries(xml_data->current_task, log_entries_count);
			}
			// attribute [limit]
			if (g_strcmp0("limit", attribute_name) == 0)  {
				gint log_entries_limit = g_ascii_strtoll(attribute_value, NULL, 0);
				if (log_entries_limit < 0) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid task log entries limit");
					return;
				}
				aion_task_set_log_entries_limit(xml_data->current_task, log_entries_limit);
			}
			
			// attribute [default_duration]
			if (g_strcmp0("default_duration", attribute_name) == 0) {
				gint default_duration = g_ascii_strtoll(attribute_value, NULL, 0);
				if (default_duration == 0) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid task default duration");
					return;
				}
				aion_task_set_default_duration(xml_data->current_task, default_duration);
			}

			// attribute [total_duration]
			if (g_strcmp0("total_duration", attribute_name) == 0) {
				gint total_duration = g_ascii_strtoll(attribute_value, NULL, 0);
				if (total_duration == 0) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid task default duration");
					return;
				}
				aion_task_private_set_total_duration(xml_data->current_task, total_duration);
			}

			i_attr++;
		}
		return;
	}

	// <log> markup
	if (g_strcmp0("log", element_name) == 0) {
		if (xml_data->context != _AION_XML_CONTEXT_TASK_LOG) {
			g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
						"<log> is not in <task-log>");
			return;
		}
		xml_data->context = _AION_XML_CONTEXT_TASK_LOG_ENTRY;
		g_assert(xml_data->current_task != NULL);
		gint i_attr = 0;
		while (TRUE){
			const gchar* attribute_name = attribute_names[i_attr];
			const gchar* attribute_value = attribute_values[i_attr];
			if (attribute_name == NULL)
				break;
			g_assert(attribute_value != NULL);
			//g_print("\t\t* %s = %s\n", attribute_name, attribute_value);

			// attribute [date]
			if (g_strcmp0("date", attribute_name) == 0)  {
				GDate* date = g_date_new();
				g_date_set_parse(date, attribute_value);
				if (! g_date_valid(date)){
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid log entry date [%s]", attribute_value);
					return;
				}
				gint entry_index = xml_data->log_entry_index;
				AionTaskLogEntry* log_entry = aion_task_private_get_log_entry(xml_data->current_task, entry_index);
				log_entry->year  = g_date_get_year(date);
				log_entry->month = g_date_get_month(date);
				log_entry->day   = g_date_get_day(date);
				g_date_free(date);
			}

			// read  [duration] attribute
			if (g_strcmp0("duration", attribute_name) == 0)  {
				gint duration = g_ascii_strtoll(attribute_value, NULL, 0);
				if (duration == 0) {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid task-log duration");
					return;
				}
				gint entry_index = xml_data->log_entry_index;
				aion_task_private_set_log_entry_duration(xml_data->current_task, entry_index, duration);
			}
			i_attr++;
		}
		return;
	}

	// <sub-task-list> markup
	if (g_strcmp0("sub-task-list", element_name) == 0) {
		if (xml_data->context != _AION_XML_CONTEXT_TASK_ITEM) {
			g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
						"<sub-task-list> is not in <task>");
			return;
		}
		xml_data->context = _AION_XML_CONTEXT_SUB_TASK_LIST;
		return;
	}


	// <sub-task> markup
	if (g_strcmp0("sub-task", element_name) == 0) {
		if (xml_data->context != _AION_XML_CONTEXT_SUB_TASK_LIST) {
			g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
						"<sub-task> is not in <sub-task-list>");
			return;
		}
		xml_data->context = _AION_XML_CONTEXT_SUB_TASK_ITEM;
		g_assert(xml_data->current_sub_task == NULL);
		xml_data->current_sub_task = aion_task_create_sub_task(xml_data->current_task, "anonymous");

		gint i_attr = 0;
		while (TRUE){
			const gchar* attribute_name = attribute_names[i_attr];
			const gchar* attribute_value = attribute_values[i_attr];
			if (attribute_name == NULL)
				break;
			g_assert(attribute_value != NULL);
			//g_print("\t\t* %s = %s\n", attribute_name, attribute_value);

			// attribute [enabled]
			if (g_strcmp0("done", attribute_name) == 0) {
				gboolean is_done = FALSE;
				if (g_strcmp0(attribute_value, "true") == 0)
					is_done = TRUE;
				else if (g_strcmp0(attribute_value, "false") == 0)
					is_done = FALSE;
				else {
					g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_XML_ERROR,
								"Invalid boolean value of an [done] attribute in subtask");
					return;
				}
				aion_sub_task_mark_as_done(xml_data->current_sub_task, is_done);
			}
			i_attr++;
		}
		return;
	}

	// TODO set a returnable error
	g_error("Unknown XML opening tag <%s>", element_name);
}


static void _xml_end_element_func(GMarkupParseContext* context, const gchar* element_name, gpointer data, GError** error)
{
	struct _xml_parse_data* xml_data = (struct _xml_parse_data*) data;
	g_debug("\t- XML end <%s>", element_name);
	if (g_strcmp0("aion-task", element_name) == 0) {
		xml_data->context = _AION_XML_CONTEXT_ROOT;
		xml_data->load_success = TRUE;
		return;
	}

	if (g_strcmp0("task-list", element_name) == 0) {
		xml_data->context = _AION_XML_CONTEXT_HEADER;
		return;
	}

	if (g_strcmp0("task", element_name) == 0) {
		xml_data->context = _AION_XML_CONTEXT_TASK_LIST;
		xml_data->current_task = NULL;
		return;
	}

	if (g_strcmp0("label", element_name) == 0) {
		xml_data->context = _AION_XML_CONTEXT_TASK_ITEM;
		return;
	}

	if (g_strcmp0("note", element_name) == 0) {
		xml_data->context = _AION_XML_CONTEXT_TASK_ITEM;
		return;
	}

	if (g_strcmp0("task-log", element_name) == 0) {
		xml_data->context = _AION_XML_CONTEXT_TASK_ITEM;
		return;
	}

	if (g_strcmp0("log", element_name) == 0) {
		xml_data->log_entry_index++;
		xml_data->context = _AION_XML_CONTEXT_TASK_LOG;
		return;
	}

	if (g_strcmp0("sub-task-list", element_name) == 0) {
		xml_data->context = _AION_XML_CONTEXT_TASK_ITEM;
		return;
	}

	if (g_strcmp0("sub-task", element_name) == 0) {
		xml_data->context = _AION_XML_CONTEXT_SUB_TASK_LIST;
		xml_data->current_sub_task = NULL;
		return;
	}

	// TODO set a returnable error
	g_error("Unknown XML closing tag <%s>", element_name);
}


static void _xml_text_func(GMarkupParseContext* context, const gchar* text, gsize text_len, gpointer data, GError** error)
{
	struct _xml_parse_data* xml_data = (struct _xml_parse_data*) data;
	// task label
	if (xml_data->context == _AION_XML_CONTEXT_TASK_LABEL){
		g_debug("\t\t- task label %s", text);
		g_assert(xml_data->current_task != NULL);
		g_assert(AION_IS_TASK(xml_data->current_task));
		aion_task_set_label(xml_data->current_task, text);
	}
	// task note
	if (xml_data->context == _AION_XML_CONTEXT_TASK_NOTE){
		g_debug("\t\t- task note [%s]", text);
		g_assert(xml_data->current_task != NULL);
		g_assert(AION_IS_TASK(xml_data->current_task));
		aion_task_set_note(xml_data->current_task, text);
	}
	// sub-task label
	if (xml_data->context == _AION_XML_CONTEXT_SUB_TASK_ITEM){
		g_debug("\t\t- sub-task label [%s]", text);
		g_assert(xml_data->current_sub_task != NULL);
		aion_sub_task_set_label(xml_data->current_sub_task, text);
	}
}

static void _xml_parser_error_func(GMarkupParseContext* context, GError* error, gpointer data)
{
	g_warning("XML parse error : %s", error->message);
}


static const GMarkupParser _aion_storage_xml_parser = {
	_xml_start_element_func, 
	_xml_end_element_func, 
	_xml_text_func, 
	NULL, 
	_xml_parser_error_func
};


GString*
_read_input_stream_content(GInputStream* input_stream, GError** error)
{
	g_return_val_if_fail(G_IS_INPUT_STREAM(input_stream), NULL);	

	GDataInputStream* data_input_stream = NULL;
	GString* file_content = NULL;
	gchar* line = NULL;

	data_input_stream = g_data_input_stream_new(G_INPUT_STREAM(input_stream));
	gsize line_length = 0;
	file_content = g_string_new(NULL);
	while (TRUE){
		line = g_data_input_stream_read_line(data_input_stream, &line_length, NULL, error);
		// FIXME read error 
		if (line == NULL) 
			break;
		g_string_append(file_content, line);
		g_free(line);
		g_string_append(file_content, "\n");
	}
	gboolean success = g_input_stream_close(G_INPUT_STREAM(input_stream), NULL, error);
	if (! success) 
		goto done;

done:
	if (data_input_stream) g_object_unref(data_input_stream);
	return file_content;
}

GString*
_read_file_content(GFile* file, GError** error)
{
	g_return_val_if_fail(G_IS_FILE(file), NULL);	

	GFileInputStream* file_input_stream = NULL;
	GString* file_content = NULL;

	file_input_stream = g_file_read(file, NULL, error);
	if (file_input_stream == NULL) 
		goto done; 

	file_content = _read_input_stream_content(G_INPUT_STREAM(file_input_stream), error);

done:
	if (file_input_stream) g_object_unref(file_input_stream);
	return file_content;
}

static void
_free_task(gpointer data)
{
	AionTask* task = (AionTask*) data;
	g_object_unref(task);
}


gboolean
aion_task_storage_load(AionTaskStorage* task_storage, GSList** p_task_list, GError** error)
{
	gboolean load_success = FALSE;
	GError *internal_error = NULL;
	GMarkupParseContext* xml_parse_context = NULL;
	struct _xml_parse_data* xml_data = NULL;

	g_return_val_if_fail(AION_IS_TASK_STORAGE(task_storage), FALSE);
	g_return_val_if_fail(p_task_list != NULL, FALSE);

	g_assert(error != NULL);
	AionTaskStoragePrivate *priv = aion_task_storage_get_instance_private(task_storage);

	gchar* file_path = g_file_get_path(priv->xml_file);
	g_info("Loading file \"%s\"", file_path);
	g_free(file_path);

	if (! g_file_query_exists(priv->xml_file, NULL)) {
		g_info("File does not yet exist");
		load_success = TRUE; // start with a new file
		goto done;
	}

	GString* file_content = _read_file_content(priv->xml_file, &internal_error);
	if (! file_content)
		goto done;

	xml_data = g_malloc0(sizeof(struct _xml_parse_data));
	xml_data->context = _AION_XML_CONTEXT_ROOT;
	xml_data->task_list = NULL;
	xml_data->current_task = NULL;
	xml_data->load_success = FALSE;
	xml_data->log_entry_index = 0;
	xml_data->current_sub_task = NULL;

	xml_parse_context = g_markup_parse_context_new (
		&_aion_storage_xml_parser,
		0, // flags
		xml_data,
		NULL //user_data_dnotify
	);
	if (! g_markup_parse_context_parse (xml_parse_context, file_content->str, file_content->len, &internal_error))
		goto done;

	*p_task_list = xml_data->task_list;
	load_success= TRUE;

done:
	if (xml_parse_context)
		g_markup_parse_context_free(xml_parse_context);
	if (xml_data)
		g_free(xml_data);
	if (! load_success) {
		if (! internal_error) 
			g_set_error(&internal_error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_FILE_READ_ERROR,
					"Unknown error"); // TODO translation
		gchar* file_path = g_file_get_path(priv->xml_file);
		g_set_error(error, AION_TYPE_STORAGE_ERROR, AION_STORAGE_ERROR_FILE_READ_ERROR,
					"Can't read file \"%s\" : %s",  // TODO translation
					file_path,
					internal_error->message
			);
		g_free(file_path);
		g_clear_error(&internal_error);
		g_slist_free_full(*p_task_list, _free_task);
		*p_task_list = NULL;
	}
	return load_success;
}


gint
aion_task_storage_get_instance_count()
{
	return instance_counter;
}

gint
aion_task_storage_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_TASK_STORAGE));
	return instance_counter;
}
