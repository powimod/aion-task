/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-Panelable"
#include "aion-panelable.h"
#include "aion-panelable-private.h"
#include "aion-task.h"
#include <glib-object.h>

G_DEFINE_INTERFACE(AionPanelable, aion_panelable, G_TYPE_OBJECT)

enum {
	SIGNAL_EDITING_STARTED,
	SIGNAL_EDITING_FINISHED,
	LAST_SIGNAL
};
static guint aion_panelable_signals[LAST_SIGNAL] = { 0 };

static void
aion_panelable_default_init(AionPanelableInterface *interface)
{
	aion_panelable_signals[SIGNAL_EDITING_STARTED] = 
		g_signal_new ("editing-started",
			G_TYPE_FROM_INTERFACE(interface),
			G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
			0,               // class offset
			NULL,            // accumulator 
			NULL,            // accumulator data
			NULL,            // C marshaller
			G_TYPE_NONE,     // return_type
			1,               // n_params 
			AION_TYPE_TASK);      // first parameter
	aion_panelable_signals[SIGNAL_EDITING_FINISHED] = 
		g_signal_new ("editing-finished",
			G_TYPE_FROM_INTERFACE(interface),
			G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
			0,               // class offset
			NULL,            // accumulator 
			NULL,            // accumulator data
			NULL,            // C marshaller
			G_TYPE_NONE,     // return_type
			2,               // n_params 
			AION_TYPE_TASK,  // first parameter
			G_TYPE_BOOLEAN); // second parameter
}

const gchar*
aion_panelable_get_name(AionPanelable* panel)
{
	g_return_val_if_fail(AION_IS_PANELABLE(panel), NULL);
	AionPanelableInterface* interface = AION_PANELABLE_GET_IFACE(panel);
	g_return_val_if_fail(interface->get_name != NULL, NULL);
	return interface->get_name(panel);
}


const gchar*
aion_panelable_get_title(AionPanelable* panel)
{
	g_return_val_if_fail(AION_IS_PANELABLE(panel), NULL);
	AionPanelableInterface* interface = AION_PANELABLE_GET_IFACE(panel);
	g_return_val_if_fail(interface->get_title != NULL, NULL);
	return interface->get_title(panel);
}

const gchar*
aion_panelable_get_icon_name(AionPanelable* panel)
{
	g_return_val_if_fail(AION_IS_PANELABLE(panel), NULL);
	AionPanelableInterface* interface = AION_PANELABLE_GET_IFACE(panel);
	g_return_val_if_fail(interface->get_icon_name != NULL, NULL);
	return interface->get_icon_name(panel);
}


void
aion_panelable_set_task(AionPanelable* panel, AionTask* task)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	g_return_if_fail(task == NULL || AION_IS_TASK(task));
	AionPanelableInterface* interface = AION_PANELABLE_GET_IFACE(panel);
	g_return_if_fail(interface->set_task != NULL);
	interface->set_task(panel, task);
}

void
aion_panelable_refresh(AionPanelable* panel)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	AionPanelableInterface* interface = AION_PANELABLE_GET_IFACE(panel);
	g_return_if_fail(interface->refresh != NULL);
	interface->refresh(panel);
}

gboolean
aion_panelable_is_editable(AionPanelable* panel)
{
	g_return_val_if_fail(AION_IS_PANELABLE(panel), FALSE);
	AionPanelableInterface* interface = AION_PANELABLE_GET_IFACE(panel);
	g_return_val_if_fail(interface->is_editable != NULL, FALSE);
	return interface->is_editable(panel);
}

gboolean
aion_panelable_needs_save(AionPanelable* panel)
{
	g_return_val_if_fail(AION_IS_PANELABLE(panel), FALSE);
	AionPanelableInterface* interface = AION_PANELABLE_GET_IFACE(panel);
	g_return_val_if_fail(interface->needs_save != NULL, FALSE);
	return interface->needs_save(panel);
}

void aion_panelable_start_editing(AionPanelable* panel)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	AionPanelableInterface* interface = AION_PANELABLE_GET_IFACE(panel);
	g_return_if_fail(interface->start_editing != NULL);
	return interface->start_editing(panel);
}

void aion_panelable_end_editing(AionPanelable* panel, gboolean validate)
{
	g_return_if_fail(AION_IS_PANELABLE(panel));
	AionPanelableInterface* interface = AION_PANELABLE_GET_IFACE(panel);
	g_return_if_fail(interface->end_editing != NULL);
	return interface->end_editing(panel, validate);
}

void
_aion_panelable_private_emit_editing_started_signal(AionPanelable* panelable, AionTask* task)
{
  g_return_if_fail (AION_IS_PANELABLE(panelable));
  g_signal_emit (panelable, aion_panelable_signals[SIGNAL_EDITING_STARTED], 0, task);
}

void
_aion_panelable_private_emit_editing_finished_signal(AionPanelable* panelable, AionTask* task, gboolean validation)
{
  g_return_if_fail (AION_IS_PANELABLE(panelable));
  g_signal_emit (panelable, aion_panelable_signals[SIGNAL_EDITING_FINISHED], 0, task, validation);
}

