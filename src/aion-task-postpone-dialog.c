/* AionTask
 * Copyright (C) 2023 Dominique Parisot
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Aion-TaskPostponeDialog"
#include <glib/gi18n.h>

#include "aion-task-postpone-dialog.h"
#include "util.h"

struct _AionTaskPostponeDialog {
	GtkDialog parent;
};

typedef struct {
	AionTask* task;
	AionMainWindow* main_window;
	GtkWidget* day_count_field;
	GtkWidget* notification_overlay;
	GtkWidget* calendar;
} AionTaskPostponeDialogPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(AionTaskPostponeDialog, aion_task_postpone_dialog, GTK_TYPE_DIALOG);

static gint instance_counter = 0;

enum {
	SIGNAL_DIALOG_CLOSED,
	LAST_SIGNAL
};
static guint aion_task_postpone_dialog_signals[LAST_SIGNAL] = { 0 };

// local prototypes
static void _dialog_response(GtkDialog*, gint, gpointer);
static void _refresh(AionTaskPostponeDialog*);
static void _calendar_day_selected(GtkCalendar*, gpointer);
static void _days_field_changed(GtkSpinButton*, gpointer);

static void
aion_task_postpone_dialog_init(AionTaskPostponeDialog* task_postpone_dialog)
{
	g_return_if_fail(AION_IS_TASK_POSTPONE_DIALOG(task_postpone_dialog));
	AionTaskPostponeDialogPrivate* priv = aion_task_postpone_dialog_get_instance_private(task_postpone_dialog);
	priv->task = NULL;
	priv->main_window = NULL;

	gtk_window_set_modal(GTK_WINDOW(task_postpone_dialog), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(task_postpone_dialog), TRUE);

	gtk_dialog_add_buttons(GTK_DIALOG(task_postpone_dialog),
				_("OK"), GTK_RESPONSE_ACCEPT,
				_("Cancel"), GTK_RESPONSE_REJECT,
				NULL);

	GtkWidget* content_area = gtk_dialog_get_content_area(GTK_DIALOG(task_postpone_dialog));
	gtk_container_set_border_width(GTK_CONTAINER(content_area), 15); // FIXME hard coded spacing

	GtkBuilder* builder = gtk_builder_new_from_resource("/org/gnome/aion-task/ui/task-postpone-dialog");
	g_assert(builder != NULL);
	GtkWidget* container = GTK_WIDGET(gtk_builder_get_object(builder, "task-postpone-dialog-content"));
	gtk_box_pack_start(GTK_BOX(content_area), container, TRUE, TRUE, 5);

	priv->day_count_field = GTK_WIDGET(gtk_builder_get_object(builder, "day-count"));
	g_assert(priv->day_count_field);
	g_signal_connect(priv->day_count_field, "changed", G_CALLBACK(_days_field_changed), task_postpone_dialog);

	priv->notification_overlay = GTK_WIDGET(gtk_builder_get_object(builder, "notification-overlay"));
	g_assert(priv->notification_overlay);

	priv->calendar = GTK_WIDGET(gtk_builder_get_object(builder, "calendar"));
	g_assert(priv->calendar);
	g_signal_connect(priv->calendar, "day-selected", G_CALLBACK(_calendar_day_selected), task_postpone_dialog);

	g_object_unref(builder);

	g_signal_connect(task_postpone_dialog, "response", G_CALLBACK(_dialog_response), task_postpone_dialog);
	gtk_widget_show_all(GTK_WIDGET(task_postpone_dialog));

	instance_counter++;
}

static void
aion_task_postpone_dialog_dispose(GObject* object)
{
	AionTaskPostponeDialog* task_postpone_dialog = AION_TASK_POSTPONE_DIALOG(object);
	AionTaskPostponeDialogPrivate *priv = aion_task_postpone_dialog_get_instance_private(task_postpone_dialog);
	if (priv->task) {
		g_object_unref(priv->task);
		priv->task = NULL;
	}
	if (priv->main_window) {
		g_object_unref(priv->main_window);
		priv->main_window= NULL;
	}
	G_OBJECT_CLASS(aion_task_postpone_dialog_parent_class)->dispose(object);
}


static void
aion_task_postpone_dialog_finalize(GObject* object)
{
	//AionTaskPostponeDialog* task_postpone_dialog = AION_TASK_POSTPONE_DIALOG(object);
	//AionTaskPostponeDialogPrivate *priv = aion_task_postpone_dialog_get_instance_private(task_postpone_dialog);
	G_OBJECT_CLASS(aion_task_postpone_dialog_parent_class)->finalize(object);
	instance_counter--;
	if (instance_counter < 0)
		g_warning("Instance counter of [%s] class is negative", g_type_name(AION_TYPE_TASK_POSTPONE_DIALOG));
}

 
static void
aion_task_postpone_dialog_class_init(AionTaskPostponeDialogClass* task_postpone_dialog_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(task_postpone_dialog_class);
	object_class->dispose= aion_task_postpone_dialog_dispose;
	object_class->finalize = aion_task_postpone_dialog_finalize;

	aion_task_postpone_dialog_signals[SIGNAL_DIALOG_CLOSED] = 
		g_signal_new ("closed",
			G_TYPE_FROM_CLASS(task_postpone_dialog_class),
			G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
			0,                   // class offset
			NULL,                // accumulator 
			NULL,                // accumulator data
			NULL,                // C marshaller
			G_TYPE_NONE,         // return_type
			1,                   // n_params 
			G_TYPE_BOOLEAN);     // first parameter
}


AionTaskPostponeDialog*
aion_task_postpone_dialog_new(AionTask* task, AionMainWindow* main_window)
{
	g_return_val_if_fail(AION_IS_TASK(task), NULL);
	g_return_val_if_fail(AION_IS_MAIN_WINDOW(main_window), NULL);
	AionTaskPostponeDialog* task_postpone_dialog = 
		AION_TASK_POSTPONE_DIALOG(g_object_new(AION_TYPE_TASK_POSTPONE_DIALOG, 
			"use-header-bar", FALSE,
			NULL));
	AionTaskPostponeDialogPrivate* priv = aion_task_postpone_dialog_get_instance_private(task_postpone_dialog);

	priv->task = task; g_object_ref(priv->task);
	priv->main_window = main_window; g_object_ref(priv->main_window);
	gtk_window_set_transient_for(GTK_WINDOW(task_postpone_dialog), GTK_WINDOW(main_window));

	_refresh(task_postpone_dialog);
	return task_postpone_dialog;
}

static void
_refresh(AionTaskPostponeDialog* task_postpone_dialog)
{
	g_assert(AION_IS_TASK_POSTPONE_DIALOG(task_postpone_dialog));
	AionTaskPostponeDialogPrivate* priv = aion_task_postpone_dialog_get_instance_private(task_postpone_dialog);

	gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->day_count_field), 1);

	// select tomorrow in calendar
	GDate* postponement_date = aion_util_get_today_date();
	g_date_add_days(postponement_date , 1);
	guint year  = g_date_get_year(postponement_date);
	guint month = g_date_get_month(postponement_date);
	guint day   = g_date_get_day(postponement_date);
	g_date_free(postponement_date);
	gtk_calendar_select_month(GTK_CALENDAR(priv->calendar), month-1, year);
	gtk_calendar_select_day(GTK_CALENDAR(priv->calendar), day);

}

static void
_dialog_response(GtkDialog* dialog, gint response_id, gpointer data)
{
	AionTaskPostponeDialog* task_postpone_dialog = AION_TASK_POSTPONE_DIALOG(data);
	AionTaskPostponeDialogPrivate* priv = aion_task_postpone_dialog_get_instance_private(task_postpone_dialog);
	gboolean postpone = FALSE;
	if (response_id == GTK_RESPONSE_ACCEPT) {
		gint days = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->day_count_field));
		g_print("Number of days to postpone : %i\n", days); 
		postpone = aion_task_postpone(priv->task, days);
	}
	g_print("Postpone : %s\n", postpone ? "yes" : "no");
	g_signal_emit(task_postpone_dialog, aion_task_postpone_dialog_signals[SIGNAL_DIALOG_CLOSED], 0, postpone);
	gtk_widget_destroy(GTK_WIDGET(dialog));
}

gint
aion_task_postpone_dialog_get_instance_count()
{
	return instance_counter;
}

gint
aion_task_postpone_dialog_check_zero_instance()
{
	if (instance_counter > 0)
		g_warning("%i instance(s) of [%s] class still in memory", instance_counter, g_type_name(AION_TYPE_TASK_POSTPONE_DIALOG));
	return instance_counter;
}

static void
_calendar_day_selected(GtkCalendar *calendar, gpointer data)
{
	AionTaskPostponeDialog* task_postpone_dialog = AION_TASK_POSTPONE_DIALOG(data);
	AionTaskPostponeDialogPrivate* priv = aion_task_postpone_dialog_get_instance_private(task_postpone_dialog);

	GDate* postpone_date= aion_util_calendar_get_date(GTK_CALENDAR(calendar));
	GDate* today_date = aion_util_get_today_date();
	gint days = g_date_days_between(today_date, postpone_date);
	g_date_free(today_date);
	g_date_free(postpone_date);

	if (days <= 0) {
		// FIXME the notification covers all the widget height
		AionNotification* notification = aion_notification_new_with_message("Postpone date must be in the future");
		gtk_widget_show(GTK_WIDGET(notification));
		gtk_overlay_add_overlay(GTK_OVERLAY(priv->notification_overlay), GTK_WIDGET(notification));
		return;
	}
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->day_count_field), days);
}

static void
_days_field_changed(GtkSpinButton *day_count_field, gpointer data)
{
	AionTaskPostponeDialog* task_postpone_dialog = AION_TASK_POSTPONE_DIALOG(data);
	AionTaskPostponeDialogPrivate* priv = aion_task_postpone_dialog_get_instance_private(task_postpone_dialog);

	gint days = gtk_spin_button_get_value(day_count_field);
	if (days < 1) {
		g_warning("Days must be greather than zero");
		return;
	}
	GDate* postponement_date = aion_util_get_today_date();
	g_date_add_days(postponement_date , days);
	guint year  = g_date_get_year(postponement_date);
	guint month = g_date_get_month(postponement_date);
	guint day   = g_date_get_day(postponement_date);
	g_date_free(postponement_date);

	gtk_calendar_select_month(GTK_CALENDAR(priv->calendar), month-1, year);
	gtk_calendar_select_day(GTK_CALENDAR(priv->calendar), day);
}
